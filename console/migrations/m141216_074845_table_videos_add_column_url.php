<?php

use yii\db\Schema;
use yii\db\Migration;

class m141216_074845_table_videos_add_column_url extends Migration
{
    public function up()
    {
        $this->addColumn('{{%videos}}', 'url', Schema::TYPE_STRING);
    }

    public function down()
    {
        echo "m141216_074845_table_videos_add_column_url cannot be reverted.\n";

        return false;
    }
}
