<?php

use yii\db\Schema;
use yii\db\Migration;

class m141121_125605_create_table_labels extends Migration
{
    public function up()
    {
        $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB';

        $this->createTable(
            '{{%labels}}',
            [
                'id' => Schema::TYPE_PK,
                'name' => Schema::TYPE_STRING . ' NOT NULL',
                'full_name' => Schema::TYPE_STRING . ' NOT NULL',
                'label_id' => Schema::TYPE_STRING . ' NOT NULL',
                'parent_id' => Schema::TYPE_STRING,
                'url_alias' => Schema::TYPE_STRING,
            ],
            $tableOptions
        );
    }

    public function down()
    {
        //echo "m141121_125605_create_table_labels cannot be reverted.\n";

        $this->dropTable('{{%labels}}');
        return false;
    }
}
