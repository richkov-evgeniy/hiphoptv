<?php

use yii\db\Schema;
use yii\db\Migration;

class m141121_130221_create_table_labels_videos extends Migration
{
    public function up()
    {
        $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB';
        $this->createTable(
            '{{%labels_videos}}',
            [
                'id' => Schema::TYPE_PK,
                'label_id' => Schema::TYPE_STRING . ' NOT NULL',
                'video_embed_code' => Schema::TYPE_STRING . ' NOT NULL',
            ],
            $tableOptions
        );
    }

    public function down()
    {
        //echo "m141121_130221_create_table_labels_videos cannot be reverted.\n";
        $this->dropTable('{{%labels_videos}}');

        return false;
    }
}
