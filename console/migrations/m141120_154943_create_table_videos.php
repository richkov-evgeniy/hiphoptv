<?php

use yii\db\Schema;
use yii\db\Migration;

class m141120_154943_create_table_videos extends Migration
{
    public function up()
    {
        $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB';
        $this->createTable(
            '{{%videos}}',
            [
                'id' => Schema::TYPE_PK,
                'asset_type' => Schema::TYPE_STRING . '(30) NOT NULL',
                'created_at' => Schema::TYPE_INTEGER . ' NOT NULL',
                'description' => Schema::TYPE_TEXT . ' NOT NULL',
                'duration' => Schema::TYPE_INTEGER . ' NOT NULL',
                'embed_code' => Schema::TYPE_STRING . ' NOT NULL',
                'name' => Schema::TYPE_STRING . ' NOT NULL',
                'preview_image_url' => Schema::TYPE_STRING . ' NOT NULL',
                'status' => Schema::TYPE_STRING . ' NOT NULL',
                'updated_at' => Schema::TYPE_INTEGER . ' NOT NULL',
                'player_id' => Schema::TYPE_STRING . ' NOT NULL',
                'url_alias' => Schema::TYPE_STRING,
            ],
            $tableOptions
        );
    }

    public function down()
    {
        //echo "m141120_154943_create_table_videos cannot be reverted.\n";
        $this->dropTable('{{%videos}}');
        return false;
    }
}
