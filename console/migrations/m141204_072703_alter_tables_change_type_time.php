<?php

use yii\db\Schema;
use yii\db\Migration;

class m141204_072703_alter_tables_change_type_time extends Migration
{
    public function up()
    {
        $this->alterColumn('{{%videos}}', 'created_at', Schema::TYPE_STRING . " NOT NULL");
        $this->alterColumn('{{%videos}}', 'updated_at', Schema::TYPE_STRING . " NOT NULL");

        $this->alterColumn('{{%shows}}', 'created_at', Schema::TYPE_STRING . " NOT NULL");
        $this->alterColumn('{{%shows}}', 'update_at', Schema::TYPE_STRING . " NOT NULL");
    }

    public function down()
    {
        echo "m141204_072703_alter_tables_change_type_time cannot be reverted.\n";

        return false;
    }
}
