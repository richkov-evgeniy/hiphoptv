<?php

use yii\db\Schema;
use yii\db\Migration;

class m141201_133743_table_profiles_add_column_date_birth extends Migration
{
    public function up()
    {
        $this->addColumn('{{%profiles}}', 'date_of_birth', Schema::TYPE_STRING . ' NOT NULL');
    }

    public function down()
    {
        echo "m141201_133743_table_profiles_add_column_date_birth cannot be reverted.\n";

        return false;
    }
}
