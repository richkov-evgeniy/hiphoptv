<?php

use yii\db\Schema;
use yii\db\Migration;

class m150625_181824_tbl_events_text_nullable extends Migration
{
    public function up()
    {
        $this->alterColumn('{{%events}}', 'text', Schema::TYPE_STRING);
    }

    public function down()
    {
        echo "m150625_181824_tbl_events_text_nullable cannot be reverted.\n";

        return false;
    }
    
    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }
    
    public function safeDown()
    {
    }
    */
}
