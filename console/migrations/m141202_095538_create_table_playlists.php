<?php

use yii\db\Schema;
use yii\db\Migration;

class m141202_095538_create_table_playlists extends Migration
{
    public function up()
    {
        $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB';

        // Blogs table
        $this->createTable('{{%playlists}}', [
            'id' => Schema::TYPE_PK,
            'user_id' => Schema::TYPE_INTEGER . ' NOT NULL',
            'playlist_name' => Schema::TYPE_STRING . ' NOT NULL',
            'videos' => 'longtext',
        ], $tableOptions);
    }

    public function down()
    {
        echo "m141202_095538_create_table_playlists cannot be reverted.\n";
        $this->dropTable('{{$playlists}}');
        return false;
    }
}
