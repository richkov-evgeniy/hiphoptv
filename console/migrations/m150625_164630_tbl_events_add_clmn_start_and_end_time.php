<?php

use yii\db\Schema;
use yii\db\Migration;

class m150625_164630_tbl_events_add_clmn_start_and_end_time extends Migration
{
    public function up()
    {
        $this->addColumn('{{%events}}', 'start_time', Schema::TYPE_INTEGER . ' AFTER `time` ');
        $this->addColumn('{{%events}}', 'end_time', Schema::TYPE_INTEGER . ' AFTER `start_time` ');
        $this->dropColumn('{{%events}}', 'time');
    }

    public function down()
    {
        echo "m150625_164630_tbl_events_add_clmn_start_and_end_time cannot be reverted.\n";

        return false;
    }
    
    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }
    
    public function safeDown()
    {
    }
    */
}
