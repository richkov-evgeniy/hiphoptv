<?php

use yii\db\Schema;
use yii\db\Migration;

class m141201_112624_alter_table_events extends Migration
{
    public function up()
    {
        $this->alterColumn('{{%events}}','time', Schema::TYPE_INTEGER);
    }

    public function down()
    {
        echo "m141201_112624_alter_table_events cannot be reverted.\n";

        return false;
    }
}
