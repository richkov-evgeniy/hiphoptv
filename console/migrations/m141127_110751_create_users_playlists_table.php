<?php

use yii\db\Schema;
use yii\db\Migration;

class m141127_110751_create_users_playlists_table extends Migration
{
    public function up()
    {
        $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB';

        $this->createTable(
            '{{%users_playlists}}',
            [
                'id' => Schema::TYPE_PK,
                'user_id' => Schema::TYPE_INTEGER . ' NOT NULL',
                'playlist_name' => Schema::TYPE_STRING . ' NOT NULL',
                'playlist_embed_code' => Schema::TYPE_STRING . ' NOT NULL',
                'url_alias' => Schema::TYPE_STRING,
            ],
            $tableOptions
        );
    }

    public function down()
    {
        echo "m141127_110751_create_users_playlists_table cannot be reverted.\n";
        $this->dropTable('{{%users_playlists}}');
        return false;
    }
}
