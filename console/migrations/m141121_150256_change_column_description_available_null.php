<?php

use yii\db\Schema;
use yii\db\Migration;

class m141121_150256_change_column_description_available_null extends Migration
{
    public function up()
    {
        $this->alterColumn('{{%videos}}','description', Schema::TYPE_TEXT);
        $this->alterColumn('{{%shows}}','description', Schema::TYPE_TEXT);
    }

    public function down()
    {
        echo "m141121_150256_change_column_description_available_null cannot be reverted.\n";

        return false;
    }
}
