<?php

use yii\db\Schema;
use yii\db\Migration;

class m141205_125120_table_user_add_access_token extends Migration
{
    public function up()
    {
        $this->addColumn('{{%users}}', 'access_token', Schema::TYPE_STRING);
    }

    public function down()
    {
        echo "m141205_125120_table_user_add_access_token cannot be reverted.\n";

        return false;
    }
}
