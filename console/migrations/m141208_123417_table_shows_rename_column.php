<?php

use yii\db\Schema;
use yii\db\Migration;

class m141208_123417_table_shows_rename_column extends Migration
{
    public function up()
    {
        $this->renameColumn('{{%shows}}', 'update_at', 'updated_at');
    }

    public function down()
    {
        echo "m141208_123417_table_shows_rename_column cannot be reverted.\n";

        return false;
    }
}
