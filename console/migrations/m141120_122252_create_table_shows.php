<?php

use yii\db\Schema;
use yii\db\Migration;

class m141120_122252_create_table_shows extends Migration
{
    public function up()
    {
        $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB';
        $this->createTable(
            '{{%shows}}',
            [
                'id' => Schema::TYPE_PK,
                'asset_type' => Schema::TYPE_STRING . '(30) NOT NULL',
                'created_at' => Schema::TYPE_INTEGER . ' NOT NULL',
                'description' => Schema::TYPE_TEXT . ' NOT NULL',
                'duration' => Schema::TYPE_INTEGER . ' NOT NULL',
                'embed_code' => Schema::TYPE_STRING . ' NOT NULL',
                'name' => Schema::TYPE_STRING . ' NOT NULL',
                'preview_image_url' => Schema::TYPE_STRING . ' NOT NULL',
                'status' => Schema::TYPE_STRING . ' NOT NULL',
                'update_at' => Schema::TYPE_INTEGER . ' NOT NULL',
                'player_id' => Schema::TYPE_STRING . ' NOT NULL',
                'url_alias' => Schema::TYPE_STRING,
                'main_photo' => Schema::TYPE_STRING,
                'main_text' => Schema::TYPE_TEXT
            ],
            $tableOptions
        );
    }

    public function down()
    {
        //echo "m141120_122252_create_table_shows cannot be reverted.\n";
        $this->dropTable('{{%shows}}');
        return false;
    }
}
