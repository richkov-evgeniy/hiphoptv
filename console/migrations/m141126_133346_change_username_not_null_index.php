<?php

use yii\db\Schema;
use yii\db\Migration;

class m141126_133346_change_username_not_null_index extends Migration
{
    public function up()
    {
        $this->dropIndex('username', '{{%users}}');

        $this->alterColumn('{{%users}}', 'username', Schema::TYPE_STRING . '(30)' );
    }

    public function down()
    {
        echo "m141126_133346_change_username_not_null_index cannot be reverted.\n";

        $this->createIndex('username', '{{%users}}', 'username', true);

        $this->alterColumn('{{%users}}', 'username', Schema::TYPE_STRING . '(30) NOT NULL' );

        return false;
    }
}
