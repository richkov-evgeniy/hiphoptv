<?php

use yii\db\Schema;
use yii\db\Migration;

class m141128_171530_create_table_events extends Migration
{
    public function up()
    {
        $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB';

        $this->createTable(
            '{{%events}}',
            [
                'id' => Schema::TYPE_PK,
                'title' => Schema::TYPE_STRING . ' NOT NULL',
                'time' => Schema::TYPE_STRING . ' NOT NULL',
                'text' => Schema::TYPE_STRING . ' NOT NULL',
                'image' => Schema::TYPE_STRING,
            ],
            $tableOptions
        );
    }

    public function down()
    {
        echo "m141128_171530_create_table_events cannot be reverted.\n";
        $this->dropTable('{{%events}}');
        return false;
    }
}
