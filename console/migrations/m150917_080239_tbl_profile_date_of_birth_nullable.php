<?php

use yii\db\Schema;
use yii\db\Migration;

class m150917_080239_tbl_profile_date_of_birth_nullable extends Migration
{
    public function up()
    {
        $this->alterColumn('{{%profiles}}', 'date_of_birth', Schema::TYPE_STRING . ' DEFAULT NULL');
    }

    public function down()
    {
        echo "m150917_080239_tbl_profile_date_of_birth_nullable cannot be reverted.\n";

        return false;
    }
    
    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }
    
    public function safeDown()
    {
    }
    */
}
