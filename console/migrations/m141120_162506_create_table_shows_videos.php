<?php

use yii\db\Schema;
use yii\db\Migration;

class m141120_162506_create_table_shows_videos extends Migration
{
    public function up()
    {
        $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB';
        $this->createTable(
            '{{%shows_videos}}',
            [
                'id' => Schema::TYPE_PK,
                'show_embed_code' => Schema::TYPE_STRING . ' NOT NULL',
                'video_embed_code' => Schema::TYPE_STRING . ' NOT NULL',
            ],
            $tableOptions
        );
    }

    public function down()
    {
        //echo "m141120_162506_create_table_shows_videos cannot be reverted.\n";
        $this->dropTable('{{%shows_videos}}');

        return false;
    }
}
