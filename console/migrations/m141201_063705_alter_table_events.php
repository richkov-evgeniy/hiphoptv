<?php

use yii\db\Schema;
use yii\db\Migration;

class m141201_063705_alter_table_events extends Migration
{
    public function up()
    {
        $this->addColumn('{{%events}}', 'weight', Schema::TYPE_INTEGER . ' NOT NULL');
    }

    public function down()
    {
        echo "m141201_063705_alter_table_events cannot be reverted.\n";
        $this->dropColumn('{{%events}}', 'weight');
        return false;
    }
}
