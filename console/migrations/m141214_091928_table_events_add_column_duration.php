<?php

use yii\db\Schema;
use yii\db\Migration;

class m141214_091928_table_events_add_column_duration extends Migration
{
    public function up()
    {
        $this->addColumn('{{%events}}', 'duration', Schema::TYPE_INTEGER);
    }

    public function down()
    {
        echo "m141214_091928_table_events_add_column_duration cannot be reverted.\n";

        return false;
    }
}
