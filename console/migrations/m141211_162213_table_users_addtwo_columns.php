<?php

use yii\db\Schema;
use yii\db\Migration;

class m141211_162213_table_users_addtwo_columns extends Migration
{
    public function up()
    {
        $this->addColumn('{{%users}}', 'service', Schema::TYPE_STRING);
        $this->addColumn('{{%users}}', 'service_id', Schema::TYPE_STRING);
    }

    public function down()
    {
        echo "m141211_162213_table_users_addtwo_columns cannot be reverted.\n";

        return false;
    }
}
