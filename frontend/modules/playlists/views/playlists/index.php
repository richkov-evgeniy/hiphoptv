<?php
use yii\helpers\Html;
use yii\helpers\Url;
?>

<div class="playlists-wrapper container-fluid">
    <div class="container">

        <div class="row">

            <div class="col-lg-12 playlists-header">
                <h1 class="head-style" >Playlists</h1>
            </div>

            <div class="create-playlist-form col-lg-12">
                <div class="row">
                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                        <?= Html::input('text', 'playlist-name', null, ['id' => 'playlist-name' , 'placeholder' =>'Enter playlist name']) ?>
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                        <?= Html::input('submit', 'create', 'Create', ['id' => 'create-playlist-submit']) ?>
                    </div>
                </div>
            </div>

            <div class="playlists-list col-lg-12">
                <?php foreach($playlists as $playlist) : ?>
                    <div class="playlist-item">
                        <div class="col-lg-3 col-md-3 col-sm-4 col-xs-4">
                            <a  href="<?= Url::to(['/playlists/playlists/watch', 'playlist_id' => $playlist['id']]); ?>"><?= $playlist['playlist_name'] ?></a>
                        </div>
                        <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
                            <a  href="<?= Url::to(['/playlists/playlists/watch', 'playlist_id' => $playlist['id']]); ?>">Watch</a>
                        </div>
                        <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
                            <a href="<?= Url::to(['/playlists/playlists/edit', 'playlist_id' => $playlist['id']]) ?>">Edit</a>
                        </div>
                        <div class="col-lg-3 col-md-3 col-sm-2 col-xs-2">
                            <a class="pl-delete" data-id="<?= $playlist['id'] ?>" href="#">X</a>
                        </div>
                     </div>
                <?php endforeach; ?>
            </div>

        </div>

    </div>
</div>
