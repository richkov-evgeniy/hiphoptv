<?php
use yii\helpers\Url;
use yii\helpers\Html;

$playlist_url = 'http://' . $_SERVER['HTTP_HOST'] .  Url::current();
$this->registerMetaTag(['name' => 'og:image', 'content' => 'http://hiphoptv.com/images/music_block_2.png']);
$this->registerMetaTag(['name' => 'og:title', 'content' => 'HipHopTV Playlist - ' . $playlist_name]);
$this->registerMetaTag(['name' => 'og:description', 'content' => 'Please see the playlist I created with HIPHOPTV.com']);
$this->registerMetaTag(['name' => 'og:url', 'content' => $playlist_url]);


$this->registerMetaTag(['name' => 'twitter:card', 'content' => 'summary_large_image']);
$this->registerMetaTag(['name' => 'twitter:title', 'content' => 'HipHopTV Playlist - ' . $playlist_name]);
$this->registerMetaTag(['name' => 'twitter:url', 'content' => $playlist_url]);
$this->registerMetaTag(['name' => 'twitter:description', 'content' => 'Please see the playlist I created with HIPHOPTV.com']);
$this->registerMetaTag(['name' => 'twitter:image:src', 'content' => 'http://hiphoptv.com/images/music_block_2.png']);

?>


<div class="container-fluid playlist-wrapper">
    <div class="container">
        <div class="row">

            <div class="col-lg-10 col-md-12 col-sm-12 col-xs-12">
                <div class="wrap-playerContainer">
                    <div id="watch-playlist" class="video-content">
                        <script src='//player.ooyala.com/v3/8716a37dac0c4bcebb4edaed06b01a36'></script>
                        <div id='ooyalaplayer'></div>
                    </div>
                </div>

                <div style="display: none">

                    <script type="text/javascript" src="//yandex.st/share/share.js" charset="utf-8"></script>
                    <div class="yashare-auto-init" data-yashareL10n="ru" data-yashareType="none" data-yashareQuickServices="facebook,twitter"></div>
                </div>

            </div>

            <div class="col-lg-2 col-md-12 col-sm-12 col-xs-12">
                <?php if(count($videos)) : ?>
                    <?php
                        $counter = 0;
                    ?>
                    <div class="row">
                        <!--<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">-->
                        <div class="col-lg-12 col-md-1 col-sm-2 pl-video-top available"><span class="Chevron-bottom-pl"></span></div>

                        <div class="col-lg-12 col-md-1 col-sm-1 col-xs-1 pl-video-left available"><span class="Chevron-right-pl"></span></div>

                        <!--<div class="ul-hidden">-->
                            <ul class="col-lg-12 col-md-10 col-sm-10 col-xs-10 playlist-videos-list">

                                    <?php foreach($videos as $video) : ?>
                                        <li class="pl-video-item visible-item" data-id="<?= $video['embed_code'] ?>" data-name="<?= $video['name'] ?>">
                                            <div class="col-lg-12 col-md-2 col-sm-3 col-xs-4">
                                                <a data-order="<?= $counter ?>" class="pl-image" href="<?= Url::to(['/watch/watch/watch', 'id' => $video['embed_code']]); ?>"><img src="<?= $video['preview_image_url'] ?>" class="label-video-img"></a>
                                            </div>
                                         </li>
                                        <?php
                                        $counter++;
                                        ?>
                                    <?php endforeach; ?>

                            </ul>
                        <!--</div>-->

                        <div class="col-lg-12 col-md-1 col-sm-2 pl-video-bottom available"><span class="Chevron-top-pl"></span></div>

                        <div class="col-lg-12 col-md-1 col-sm-1 col-xs-1 pl-video-right available"><span class="Chevron-left-pl"></span></div>
                        <!--</div>-->
                    </div>

                    <div class="c-clearfix"></div>

                <?php else : ?>
                    <div class="simple-text col-lg-12">
                        Your playlist is empty
                    </div>
                <?php endif; ?>
            </div>

        </div>
    </div>

    <div class="container-player-icons-bar">
        <div class="container">
            <div class="player-icons-bar">

                <div class="player-current-info">
                </div>

            </div>
        </div>
    </div>

</div>
