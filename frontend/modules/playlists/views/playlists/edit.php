<?php
use yii\helpers\Url;
use yii\helpers\Html;
?>

<div class="container-fluid playlist-page">
    <div class="playlist-edit-page-wrapper">
        <div class="playlist-edit-page-container container" data-id='<?= $playlist['id'] ?>' data-name='<?= $playlist['playlist_name'] ?>'>
            <div class="row">
                <div class="playlist-edit-panel col-lg-12">
                    <div class="row">
                        <div class="col-xs-4">
                            <?= Html::input('text', 'playlist-name', $playlist['playlist_name'], ['id' => 'edit-playlist-name'])  ?>
                        </div>
                        <div class="col-xs-4">
                            <?= Html::input('submit', 'playlist-save', 'save', ['id' => 'playlist-save'])  ?>
                        </div>
                        <div class="col-xs-4">
                            <?= Html::input('submit', 'playlist-delete', 'delete', ['id' => 'playlist-delete'])  ?>
                        </div>
                    </div>
                </div>


                <?php if(count($videos)) : ?>
                    <div class="col-lg-12">
                        <ul class="playlist-videos" id="sortable">
                            <?php $counter = 1; ?>
                            <?php foreach($videos as $video) : ?>
                                <li class="row video-available ui-state-default" id="pl-video-id" data-id="<?= $video['embed_code'] ?>">
                                    <div class='pl-video col-lg-12'>
                                        <div class="pl-video-container">
                                            <div class="row">
                                                <div class="position-pl col-lg-1"><?= $counter++ ?></div>
                                                <a class="col-lg-2 pl-image" href="<?= Url::to(['/watch/watch/watch', 'id' => $video['embed_code']]); ?>"><img src="<?= $video['preview_image_url'] ?>" class="label-video-img"></a>
                                                <div class="pl-label-video-info col-lg-8">
                                                    <a href="<?= Url::to(['/watch/watch/watch', 'id' => $video['embed_code']]); ?>" class="pl-video-link"><?= $video['name'] ?></a>
                                                </div>
                                                <div class="pl-video-delete col-lg-1">
                                                    <a class="pl-video-delete-link" data-id="<?= $video['embed_code'] ?>" href="#">x</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </li>
                            <?php endforeach; ?>
                        </ul>
                    </div>

                    <div class="c-clearfix"></div>
                <?php else : ?>
                    <div class="simple-text col-lg-12">
                        Your playlist is empty
                    </div>
                <?php endif; ?>
            </div>
        </div>
    </div>
</div>
