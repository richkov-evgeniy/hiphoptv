<?php

namespace app\modules\playlists;

class Module extends \yii\base\Module
{
    public $controllerNamespace = 'app\modules\playlists\controllers';

    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
