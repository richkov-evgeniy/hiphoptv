<?php

namespace app\modules\playlists\controllers;

use yii\web\Controller;
use Yii;

class DefaultController extends Controller
{
    public function actionIndex()
    {
        if(!Yii::$app->user->getId()) {
            $this->redirect('/home-guest');
        }

        return $this->render('index');
    }
}
