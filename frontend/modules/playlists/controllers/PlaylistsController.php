<?php

namespace app\modules\playlists\controllers;

use yii\web\Controller;
use app\modules\playlists\models\Playlists;
use Yii;
use app\modules\browse\models\Videos;

class PlaylistsController extends Controller
{
    public function actionIndex()
    {
        if(!Yii::$app->user->getId()) {
            $this->redirect('/home-guest');
        }

        $model_playlists = new Playlists();

        $playlists = $model_playlists->find()
            ->where(["user_id" => Yii::$app->user->getId()])
            ->asArray()
            ->all();

        return $this->render('index', [
            'playlists' => $playlists
        ]);
    }


    public function actionCreate()
    {
        if(!Yii::$app->user->getId()) {
            $this->redirect('/home-guest');
        }

        $user_id = Yii::$app->user->getId();
        $model_playlists = new Playlists();
        $model_playlists->user_id = $user_id;
        $model_playlists->playlist_name = $_POST['playlist_name'];
        $videos = array();
        $model_playlists->videos = json_encode($videos);
        $model_playlists->save();

        $data = array();

        echo json_encode($data);
    }


    public function actionAddvideo()
    {
        if(!Yii::$app->user->getId()) {
            $this->redirect('/home-guest');
        }

        $model_playlists = new Playlists();
        $playlist = $model_playlists->find()
            ->andWhere(['id' => $_POST['playlist_id']])
            ->andWhere(['user_id' => Yii::$app->user->getId()])
            ->one();

        $old_videos = json_decode($playlist->videos);
        $old_videos[] = $_POST['video_id'];

        $playlist->videos = json_encode($old_videos);
        $playlist->save();

        $data = array();
        echo json_encode($data);
    }


    public function actionDelete()
    {
        if(!Yii::$app->user->getId()) {
            $this->redirect('/home-guest');
        }

        $playlist = Playlists::findOne($_POST['playlist_id']);
        $playlist->delete();

        $data = array();
        echo json_encode($data);
    }


    public function actionEdit($playlist_id)
    {
        if(!Yii::$app->user->getId()) {
            $this->redirect('/home-guest');
        }

        $model_playlists = new Playlists();
        $playlist = $model_playlists->find()
        ->andWhere(['id' => $playlist_id])
        ->andWhere(['user_id' => Yii::$app->user->getId()])
        ->one();

        $playlist_videos = json_decode($playlist->videos, true);


        $model_videos = new Videos();
        $videos = $model_videos->find()
            ->where(['embed_code' => $playlist_videos])
            ->asArray()
            ->all();

        $videos_sorted = array();

        foreach ($videos as $video) {
            $order_number = array_search($video['embed_code'], $playlist_videos);
            $videos_sorted[$order_number] = $video;
        }

        ksort($videos_sorted);


        return $this->render('edit', [
            'playlist' => $playlist,
            'videos' => $videos_sorted
        ]);
    }


    public function actionUpdate() {

        if(!Yii::$app->user->getId()) {
            $this->redirect('/home-guest');
        }

        $model_playlists = new Playlists();

        $playlist = $model_playlists->find()
            ->andWhere(['id' => $_POST['playlist_id']])
            ->andWhere(['user_id' => Yii::$app->user->getId()])
            ->one();

        if(isset($_POST['videos_sort'])) {
            $videos_sort = $_POST['videos_sort'];
            unset($videos_sort[0]);
            $videos = array();
            foreach ($videos_sort as $video) {
                $videos[] = $video;
            }

            $playlist->videos = json_encode($videos);
        }   else {
            $videos = array();
            $playlist->videos = json_encode($videos);
        }

        $playlist->playlist_name = $_POST['playlist_name'];

        $playlist->save();

        $data = array();
        echo json_encode($data);
    }


    public function actionWatch($playlist_id) {

        /*if(!Yii::$app->user->getId()) {
            $this->redirect('/home-guest');
        }*/

        $model_playlists = new Playlists();
        $playlist = $model_playlists->find()
            ->andWhere(['id' => $playlist_id])
            //->andWhere(['user_id' => Yii::$app->user->getId()])
            ->one();

        $playlist_videos = json_decode($playlist->videos, true);

        $model_videos = new Videos();
        $videos = $model_videos->find()
            ->where(['embed_code' => $playlist_videos])
            ->asArray()
            ->all();

        $videos_sorted = array();

        foreach ($videos as $video) {
            $order_number = array_search($video['embed_code'], $playlist_videos);
            $videos_sorted[$order_number] = $video;
        }

        ksort($videos_sorted);

        return $this->render('watch', [
            'videos' => $videos_sorted,
            'playlist_name' => $playlist->playlist_name
        ]);
    }


}
