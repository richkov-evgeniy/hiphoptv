<?php

namespace app\modules\info;

class Module extends \yii\base\Module
{
    public $controllerNamespace = 'app\modules\info\controllers';

    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
