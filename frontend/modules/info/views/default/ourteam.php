<?php
?>


<div class="container-fluid ourteam-wrapper">
    <div class="ourteam-container container">

        <div class="row">
            <div class="col-lg-12">
                <h1 class="our-team-header">our team</h1>
            </div>
        </div>

        <div class="row">
            <div class="col-lg-12 ourteam-desk-wrapper">
                <div class="ourteam-desk-container">
                    <div class="row">
                        <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                            <div class="team-member">
                                <div class="tm-image-wrapper">
                                    <a class="show-bio-shawn">
                                        <img src="<?= $this->assetManager->publish('@vova07/themes/site/images/ceo.png')[1] ?>">
                                    </a>
                                </div>
                                <div class="member-name">
                                    <h2>Shawn D. Granberry</h2>
                                </div>
                                <div class="member-post">
                                    Founder and CEO
                                </div>
                                <div id="bio-shawn" class="bio-info">
                                    <a class="bio-close">X</a>
                                    <div class="bio-head">
                                        Shawn D. Granberry
                                    </div>
                                    <div class="bio-text">
                                        Shawn D. Granberry is CEO of Watch Now Networks, Inc. and has worked in the entertainment
                                        industry for over 25 years. While still in high school he worked in the A&R department
                                        in his Uncle's 2-Tuff-E-Nuff Productions where his mother was the CEO. 2-Tuff-E-Nuff
                                        productions worked with such artist and companies as Toni, Tone', Tony, En Vogue, Madonna,
                                        Club Nouveau,Con Funk Shun,Timex Social Club, Regina Belle, Alexander O'neal, BET Networks,
                                        ABC and Fox Networks. In high school Mr. Granberry also teamed up with Shakir Stewart to
                                        produce some of the biggest parties Oakland, CA has ever seen for high school students.
                                        While attending UC Berkeley, he continued his entertainment career by creating several
                                        very popular club nights and concerts in the San Francisco Bay Area.

                                        After college, Mr. Granberry worked as a close confidant to Shakir Stewart and also continued
                                        to produce events like, Def Jam's, 'How To Find A Mega Star'. Mr. Granberry also worked very
                                        close with famed movie producer, Robert Watts at Transformer Entertainment. While working
                                        with Stewart and Watts, Mr. Granberry began researching the new broadcast model now known
                                        as Over the Top broadcasting. After returning to UC Berkeley to fully study this model,
                                        Mr. Granberry founded Watch Now Networks, Inc. Mr. Granberry's passion in working with
                                        inner city youth so he also founded The Scholar Athlete Union and Bears Youth Basketball
                                        with help from childhood friend Jason Kidd and works very close with Stanford and UC
                                        Berkeley faculty to help young people achieve a college education and survive the dangers
                                        of the streets.
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                            <div class="team-member">
                                <div class="tm-image-wrapper">
                                    <a class="show-bio-aaronjit">
                                        <img src="<?= $this->assetManager->publish('@vova07/themes/site/images/coo.png')[1] ?>">
                                    </a>
                                </div>
                                <div class="member-name">
                                    <h2>Aaronjit Athwal</h2>
                                </div>
                                <div class="member-post">
                                    COO
                                </div>
                                <div id="bio-aaronjit" class="bio-info">
                                    <a class="bio-close">X</a>
                                    <div class="bio-head">
                                        Aaronjit Athwal
                                    </div>
                                    <div class="bio-text">
                                        Aaronjit Athwal is a San Jose State University graduate with a degree in Business Marketing.
                                        At the young age of 24 Mr. Athwal has over 6 years of experience in the music business with
                                        consulting roles with a RnC records in Lonodn, England, So Timeless records. He is also the
                                        manager of an up coming artist Nave Suave and all the members of Gold Watch Gang. Mr. Athwal
                                        is responsible for all the branding, image control, and marketing for the artist. He is born
                                        in the internet era and is in touch with every changing trends of the music industry online.

                                        As the COO of Watch Networks, Aaronjit provides his expertise in the online music industry
                                        by providing techniques to gain ultimate brand royalty. With all his experience of creating
                                        and consulting on brand development, he is able to provide the competitive advantage needed
                                        for HipHoptv.com to become a leader in the music industry.
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                            <div class="team-member">
                                <div class="tm-image-wrapper">
                                    <a class="show-bio-edlover">
                                        <img src="<?= $this->assetManager->publish('@vova07/themes/site/images/edlover.png')[1] ?>">
                                    </a>
                                </div>
                                <div class="member-name">
                                    <h2>Ed Lover</h2>
                                </div>
                                <div class="member-post">
                                    VP of Artist Relations
                                </div>
                                <div id="bio-edlover" class="bio-info">
                                    <a class="bio-close">X</a>
                                    <div class="bio-head">
                                        Ed Lover
                                    </div>
                                    <div class="bio-text">
                                        Before reaching fame on MTV, Ed Lover was part of an eccentric and deliberately enigmatic hip hop collective
                                        called No Face, primarily with fellow members Kevon Shah and Mark 'Mark Sexx' Skeete, who served as the
                                        main producer. No Face first debuted in 1989 on Island Records Club music imprint Great Jones with its
                                        only known recording for the label, 'Hump Music'—an underground sexually explicit parody of The Jungle
                                        Brothers 1988 hip-house classic 'I'll House You.' No Face would continue recording for another five years,
                                        but it only released one album in 1990, Wake Your Daughter Up on its own No Face label, which was operated
                                        as an imprint of the Rush Associated Labels division of Def Jam Recordings.<br/>

                                        <br/>Yo! MTV Raps<br/>

                                        <br/>Roberts is best known for saying “C’mon, son!” and being the co-host of the weekday version of
                                        MTV's hip hop music specialty program Yo! MTV Raps Today with partner André 'Doctor Dré' Brown. (The
                                        main weekend version was hosted by hip hop pioneer 'Fab Five Freddy' Brathwaite) On Yo! MTV Raps Today,
                                        Ed created his own dance called theEd Lover Dance that became somewhat popular in the 1990s and was performed
                                        to the track 'The 900 Number' by DJ Mark the 45 King.<br/>

                                        <br/>New York radio<br/>

                                        <br/>Ed and Dré—who hosted the high-rated Morning Show with Ed, Lisa, and Dré on New York's Hot 97 FM
                                        from 1993 to 1998—released only one album, 1994's Back Up Off Me! The previous year, they starred as a
                                        pair of hapless barbers turned police officers in the New Line Cinema feature film “Who’s the Man” which
                                        was well received and was hailed as the hip hop whodunit.<br/>

                                        <br/>He was later a radio personality on New York's Hip-Hop Radio Power 105.1 FM and also appeared
                                        on the VH1 program 100 Greatest One-Hit Wonders in 2002. Lover also hosted a show on HBO in 2000–2001
                                        titled KO Nation. Ed hosted the hit TV show Hip Hop Hold ’Em along with the self-produced web show called
                                        C’mon, Son! He was also the co-host of the morning show on WWPR-FM (Power 105.1) in New York City. In 2011,
                                        Lover became the host of his own show called 'Friday Night Flava' on WRKS (Kiss FM) in New York City. Currently
                                        hosts 'TheEd Lover Show' on SiriusXM's Old Skool rap station BackSpin.
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                            <div class="team-member">
                                <div class="tm-image-wrapper">
                                    <a class="show-bio-damani">
                                        <img src="<?= $this->assetManager->publish('@vova07/themes/site/images/damanithompson.png')[1] ?>">
                                    </a>
                                </div>
                                <div class="member-name">
                                    <h2>Damani Thompson</h2>
                                </div>
                                <div class="member-post">
                                    VP of Artist Relations
                                </div>
                                <div id="bio-damani" class="bio-info">
                                    <a class="bio-close">X</a>
                                    <div class="bio-head">
                                        Damani Thompson
                                    </div>
                                    <div class="bio-text">
                                        Since 1993 when Souls of Mischief dropped their freshman album 93 ‘Til Infinity, the name Phesto has been synonymous with lyricism among Hip Hop fans.<br/>

                                        <br/>Growing up in Oakland, Phesto attended the same junior high school as Tajai, who introduced him to A-Plus, Opio and Casual. When Tajai, A-Plus and Opio began rhyming as a group, Phesto was asked to join and the four MCs came to be known as Souls of Mischief.<br/>

                                        <br/>Although Phesto is well known and critically acclaimed for his intricate lyrics and charisma as an MC, he is also a seasoned producer who has been honing his craft for 14 years. His production credits include “Phesto Dee” from Hieroglyphics’ Third Eye Vision album, “This is for My Niggas” from Casual’s album Meanwhile, “Airborne Rangers” from the Souls of Mischief album Trilogy: Conflict, Climax, Resolution, & “Shift the Sands”, “Way too Cold” and “Maximize Third Eyes” (12” B-Side) from the Souls of Mischief album Focus.<br/>

                                        <br/>As with everything the members of Hieroglyphics and Souls of Mischief do, Phesto takes the creation of music seriously. “I’m at the age now where if you ask me what I am I’ll tell you I’m a musician. 15 years ago, I might have said I’m a rapper. I’m actually enrolled in school studying music theory. I wanted to get classical training because if I’m going to be a musician I want to be the best musician I can be. I want to see all sides of the prism.” Musicians Kev Choice (piano/organ/Moog), Uncle Ron (guitar) and Nayo LaFreniere (vocals) are featured on the album.<br/>

                                        <br/>As VP of Artist Relations Phesto brings years experience as an artist, producer and music executive to Hiphoptv.com<br/>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                            <div class="team-member">
                                <div class="tm-image-wrapper">
                                    <a class="show-bio-jineea">
                                        <img src="<?= $this->assetManager->publish('@vova07/themes/site/images/jineeabutler.png')[1] ?>">
                                    </a>
                                </div>
                                <div class="member-name">
                                    <h2>Jineea Butler</h2>
                                </div>
                                <div class="member-post">
                                    VP of Community Outreach & Relations
                                </div>
                                <div id="bio-jineea" class="bio-info">
                                    <a class="bio-close">X</a>
                                    <div class="bio-head">
                                        Jineea Butler
                                    </div>
                                    <div class="bio-text">
                                        Jineea Butler is a Long Island University Brooklyn Psychology Graduate, Hall of Fame Athlete, Founder of the Social Services of Hip Hop and the Hip Hop Union. This quintessential authority of everything Hip Hop has been an instrumental force for the last ten years. She began her career in Hip Hop as one of two females to play on the male dominated HOT 97 HOT SHOTS Celebrity Basketball Team managed by Radio Personalities Ed Lover and Curt Flirt. Based on her experiences she published an article that many Hip Hop Entertainers had Narcissistic Personality Disorder; her theory was later defended, proved and expanded by a New York Times Magazine article stating that entertainers in general suffer from the disorder. She was then inspired to focus her remaining collegiate studies around Hip Hop’s social and psychological issues, ultimately creating a niche market. As a Social Worker, she specialized in servicing the Hip Hop personalities which were mainly the resistant and deviant behaviors of the clients she served in HIV/AIDS Housing, Rikers Island, and Homeless Services.<br/>

                                        <br/>While on Rikers Island she did a study on the correlation between jail and Hip Hop and was urged by her supervisor to create a training for her peers which was entitled ‘Hip Hop From a Clinical Perspective’ now known as Teach Hip Hop NYC. She got an official accommodation by the Commissioner and quickly rose through the ranks. She became known as the Hip Hop Specialist leaving a true mark on her last position as Director of Career Development for one of the largest homeless service agencies in the country, The Doe Fund. Her dual career ended when she finalized her research, and gave birth to the Social Services of Hip Hop. A trailblazer of her generation Jineea has not been scared to challenge the Hip Hop Community to be better in the areas of Education, Politics and Basketball. As a Hip Hop Analyst, she has become an expert at investigating the trends and behaviors of the community and delivering programming that solves the issues that surrounds what she has coined the ‘Hip Hop Dilemma’, the common distasteful physical, emotional and/or mental experience that negatively impacts individuals who are involved in or come in contact with the culture. To her credit she has produced the first Hip Hop Awards show that honored Hip Hop’s pioneers for their contributions, which has been adapted into VH1’s Hip Hop Honors and subsequently relaunched the career of Flavor Flav.<br/>

                                        <br/>She was featured in the historical documentary, The Other Side of Hip Hop, The Sixth Element where one of her mentors Legendary Photographer Ernie Paniccioli’s life was immortalized. She has provided technical assistance for Kenny Graham’s West 4th Street Basketball Classic using the platform to produce positive entertainment productions like Hip Hop Helps the Homeless, which featured a then young P-Star star of the PBS show the Electric Factory. She also produced West 4th Street’s 30th Anniversary titled Drop the Rock and Love Hip Hop, where she led the way in repealing the Rockefeller Drug Laws by getting the most petitions signed in one day. She landed a role in her own movie titled ‘Follow the Leader’ when she caught the attention of film maker, David Ambrose while cultivating youth programming for the worst school in New York State. The documentary has spawned a Hip Hop 4 Education Screening Series, which was launched at the BET Hip Hop Awards Weekend in 2010. She founded the Hip Hop Union in 2009, introduced the I AM A CITIZEN campaign, and initiated a six month road tour.  Most notably, Jineea used her Leaders In Training Program at Wings Academy in the Bronx to coach the girls basketball team to the schools first city title and also became the first girls Bronx team to compete in the state tournament. Lastly, Jineea has spent the past few years coordinating the Hip Hop events for Reverend Jesse Jackson Sr. and the Rainbow Push Coalition with the goal to connect the generations that have been lost in translation to the relevance, guidance and wealth of expertise that can be gained from this historic organization.
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                            <div class="team-member">
                                <div class="tm-image-wrapper">
                                    <a class="show-bio-ernie">
                                        <img src="<?= $this->assetManager->publish('@vova07/themes/site/images/ernieparnniccioli.png')[1] ?>">
                                    </a>
                                </div>
                                <div class="member-name">
                                    <h2>Ernie Panniccioli</h2>
                                </div>
                                <div class="member-post">
                                    Director of Photography
                                </div>
                                <div id="bio-ernie" class="bio-info">
                                    <a class="bio-close">X</a>
                                    <div class="bio-head">
                                        Ernie Panniccioli
                                    </div>
                                    <div class="bio-text">
                                        Nearly thirty years ago, Ernie Paniccioli began photographing the graffiti art throughout New York City as well as the young people creating it. Armed with a 35-millimeter camera, Paniccioli literally recorded the beginning salvos of hip hop, today the most dominant youth culture on the planet. Be it Grandmaster Flash at the Roxy, a summer block party in the Bronx, the fresh faces of Queen Latifah and Will Smith, the cocksure personas of Tupac Shakur, The Notorious B.I.G., and Emimem, or the regal grace of Lauryn Hill, Ernie Paniccioli has been there to showcase hip hop's evolution much in the same way Gordon Parks recorded the Civil Rights Movement, or akin to the manner in which James Van Der Zee, the great photographer of Harlem in the 1920s, met the energy and spirit of his times.
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>