<?php
 use yii\helpers\Html;
?>




<div class="container-fluid about-us-wrapper for-investors">
    <div class="aboutus-container container">

        <div class="row">
            <div class="col-lg-12">
                <h1 class="about-us-header">For Investors</h1>
            </div>
        </div>

        <?php if($access) : ?>
            <div class="row">
                <div class="col-lg-12 about-us-info-wrapper">
                    <div class="about-us-info-container">

                        <div class="au-text-main">
                            <script src='//player.ooyala.com/v3/8716a37dac0c4bcebb4edaed06b01a36'></script><div id='ooyalaplayer'></div><script>OO.ready(function() { OO.Player.create('ooyalaplayer', 'VpM3NqdDr3oL0Nd7Rsf--8_Z93E76DL0', {"autoplay":true}); });</script><noscript><div>Please enable Javascript to watch this video</div></noscript>
                        </div>

                        <div class="for-investors-pdf">
                            <a id="downloadLink" href="/pdf/HipHopTV_-_Deck_Final.pdf" target="_blank" type="application/octet-stream" download="HipHopTV_-_Deck_Final.pdf">Download PDF</a>
                        </div>

                    </div>
                </div>
            </div>
        <?php else: ?>
        <div class="row">
            <div class="col-lg-12 about-us-info-wrapper">
                <div class="about-us-info-container">
                    <p>Please enter a password</p>
                    <div class="investors-pass">
                        <?= Html::beginForm()?>
                        <?= Html::input("text", "password") ?>
                        <?= Html::input("submit", "send", "send")?>
                        <?= Html::endForm()?>
                    </div>
                </div>
            </div>
        </div>
        <?php endif; ?>

    </div>
</div>