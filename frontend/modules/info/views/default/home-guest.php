<?php
use yii\widgets\ActiveForm;
use vova07\fileapi\Widget;
use yii\helpers\Html;
use yii\authclient\widgets\AuthChoice;

$this->title = 'HipHopTV - Welcome';
?>
<section id="welcome">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="welcome-block">
                    <p>Welcome to</p>
                    <a href="#" class="logo"><img src="/images/logo_retina.png"/></a>
                    <button class="play-btn" data-toggle="modal" data-target="#video-popup"></button>
                    <h1>Sign up now for early and exclusive <br/>
                        access before HipHopTV launches!</h1>
                </div>
            </div>
        </div>
    </div>
</section>

<section id="about">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="about-title"><h3>About</h3></div>

                <p>HipHopTV is a 24-hour live streaming online platform that features exclusive original
                    programming, custom playlists, licensed content and music videos. Viewers can watch on demand
                    through our linear site and mobile app. All the best hip-hop on the planet in one place and you
                    don’t even need a cable box.</p>
                <a href="" class="btn btn-red" data-toggle="modal" data-target="#signup-popup">Sign up now</a>
            </div>
        </div>
    </div>
</section>

<section id="options">
    <ul class="opt-items">
        <li>
            <div class="item-img"><img src="/images/playlists.png"/></div>
            <div class="item-info">
                <h4>Playlists</h4>

                <p>Build your own custom video playlists that you can share on social media</p>
                <a href="" class="btn btn-red" data-toggle="modal" data-target="#signup-popup">Sign up now</a>
            </div>
        </li>
        <li>
            <div class="item-img"><img src="/images/live.png"/></div>
            <div class="item-info">
                <h4>Live Streaming</h4>

                <p>Music videos, interviews, documentaries all streaming LIVE on all connected devices</p>
                <a href="" class="btn btn-red" data-toggle="modal" data-target="#signup-popup">Sign up now</a>
            </div>
        </li>
        <li>
            <div class="item-img"><img src="/images/discover.png"/></div>
            <div class="item-info">
                <h4>Discover</h4>

                <p>Search an artist or era and discover videos
                    that are similar </p>
                <a href="" class="btn btn-red" data-toggle="modal" data-target="#signup-popup">Sign up now</a>
            </div>
        </li>
    </ul>
</section>

<section id="categories">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <ul class="nav nav-tabs" role="tablist">
                    <li role="presentation" class="active"><a href="#music" aria-controls="music" role="tab"
                                                              data-toggle="tab">Music Videos</a></li>
                    <li role="presentation"><a href="#original" aria-controls="original" role="tab" data-toggle="tab">Original
                            Content</a></li>
                    <li role="presentation"><a href="#documentaries" aria-controls="documentaries" role="tab"
                                               data-toggle="tab">Documentaries</a></li>
                    <li role="presentation"><a href="#live" aria-controls="live" role="tab" data-toggle="tab">Live
                            Performances</a></li>
                </ul>

                <div class="tab-content">
                    <div role="tabpanel" class="tab-pane active" id="music">
                        <ul class="categories">
                            <li>
                                <div class="category-img">
                                    <img src="/images/music_block_1.png" alt=""/>
                                </div>
                                <div class="category-info">
                                    <p>Hip Hop TV Street Reel</p>
                                </div>
                            </li>
                            <li>
                                <div class="category-img">
                                    <img src="/images/music_block_2.png" alt=""/>
                                </div>
                                <div class="category-info">
                                    <p>Dizzy Wright - The Flavor feat. SwizZz</p>
                                </div>
                            </li>
                            <li>
                                <div class="category-img">
                                    <img src="/images/music_block_3.png" alt=""/>
                                </div>
                                <div class="category-info">
                                    <p>Damani - Everybody C'mon</p>
                                </div>
                            </li>
                            <li>
                                <div class="category-img">
                                    <img src="/images/music_block_4.png" alt=""/>
                                </div>
                                <div class="category-info">
                                    <p>Damani - Now That's Love feat. Musiq Soulchild & Robert Glasper</p>
                                </div>
                            </li>
                        </ul>
                    </div>
                    <div role="tabpanel" class="tab-pane" id="original">
                        <ul class="categories">
                            <li>
                                <div class="category-img">
                                    <img src="/images/original_block_1.png" alt=""/>
                                </div>
                                <div class="category-info">
                                    <p>CertifiedFunny - Ed Lover C’mon Son 86</p>
                                </div>
                            </li>
                            <li>
                                <div class="category-img">
                                    <img src="/images/original_block_2.png" alt=""/>
                                </div>
                                <div class="category-info">
                                    <p>CertifiedFunny - Ed Lover C’mon Son 85</p>
                                </div>
                            </li>
                            <li>
                                <div class="category-img">
                                    <img src="/images/original_block_3.png" alt=""/>
                                </div>
                                <div class="category-info">
                                    <p>CertifiedFunny - Ed Lover C’mon Son 84</p>
                                </div>
                            </li>
                            <li>
                                <div class="category-img">
                                    <img src="/images/original_block_4.png" alt=""/>
                                </div>
                                <div class="category-info">
                                    <p>CertifiedFunny - Ed Lover C’mon Son 83</p>
                                </div>
                            </li>
                        </ul>
                    </div>
                    <div role="tabpanel" class="tab-pane" id="documentaries">
                        <ul class="categories">
                            <li>
                                <div class="category-img">
                                    <img src="/images/doc_block_1.png" alt=""/>
                                </div>
                                <div class="category-info">
                                    <p>The Battle</p>
                                </div>
                            </li>
                            <li>
                                <div class="category-img">
                                    <img src="/images/doc_block_2.png" alt=""/>
                                </div>
                                <div class="category-info">
                                    <p>Residue Years</p>
                                </div>
                            </li>
                            <li>
                                <div class="category-img">
                                    <img src="/images/doc_block_3.png" alt=""/>
                                </div>
                                <div class="category-info">
                                    <p>Shakir Stewart Crime Files</p>
                                </div>
                            </li>
                            <li>
                                <div class="category-img">
                                    <img src="/images/doc_block_4.png" alt=""/>
                                </div>
                                <div class="category-info">
                                    <p>The Battle</p>
                                </div>
                            </li>
                        </ul>
                    </div>
                    <div role="tabpanel" class="tab-pane" id="live">
                        <ul class="categories">
                            <li>
                                <div class="category-img">
                                    <img src="/images/live_block_1.png" alt=""/>
                                </div>
                                <div class="category-info">
                                    <p>EPMD - What Cha Sayin</p>
                                </div>
                            </li>
                            <li>
                                <div class="category-img">
                                    <img src="/images/live_block_2.png" alt=""/>
                                </div>
                                <div class="category-info">
                                    <p>Too Short Fightin the Feeling</p>
                                </div>
                            </li>
                            <li>
                                <div class="category-img">
                                    <img src="/images/live_block_3.png" alt=""/>
                                </div>
                                <div class="category-info">
                                    <p>Keith Murray - Candi Bar</p>
                                </div>
                            </li>
                            <li>
                                <div class="category-img">
                                    <img src="/images/live_block_4.png" alt=""/>
                                </div>
                                <div class="category-info">
                                    <p>Keith Murray - Get lifted</p>
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>

                <a href="" class="btn btn-red" data-toggle="modal" data-target="#signup-popup">Sign up now</a>
            </div>
        </div>
    </div>
    </div>
</section>

<section id="apps">
    <div class="container">
        <div class="row">
            <div class="col-lg-4 col-md-5 col-xs-4">
                <img src="/images/iphone.png" class="iphone"/>
            </div>
            <div class="col-lg-8 col-md-7 col-xs-8">
                <div class="apps-info">
                    <p>HipHopTV Will be available</p>

                    <p class="big-text">on all devices</p>
                </div>
                <div class="apps-btn">
                    <a href="https://itunes.apple.com/us/app/hiphoptv/id948701818?mt=8" target="_blank" class="app-store" title="App Store"></a>
                    <a href="https://play.google.com/store/apps/details?id=com.hiphoptv.android" target="_blank" class="google-play" title="Google Play"></a>
                </div>
            </div>
        </div>
    </div>
</section>

<section id="cta">
    <div class="container">
        <div class="row">
            <div class="block-title"></div>
            <h4>Sign up now for early and exclusive
                access before HipHopTV launches!</h4></div>
        <a href="" class="btn btn-red" data-toggle="modal" data-target="#signup-popup">Sign up now</a>
    </div>
    </div>
</section>

<div class="modal black fade" id="thanks-popup" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><img
                        src="/images/close.png"/>
                </button>
                <h4 class="modal-title">Thank you</h4>
            </div>
            <div class="modal-body">
                <p class="text-center">Thank you for signing up. We will email you the password to get exclusive
                    preview, one week before the official launch.</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-red" data-dismiss="modal">ok</button>
            </div>
        </div>
    </div>
</div>
<div class="modal black video fade" id="video-popup" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><img
                        src="/images/close.png"/></button>
            </div>
            <div class="modal-body">
                <video width="878" height="495" controls>
                    <source src="files/hiphop.mp4" type="video/mp4">
                </video>
                <div class="share-block">
                    <ul class="share-links">
                        <li><a href="https://www.facebook.com/sharer/sharer.php?u=http://hiphoptv.com/" class="fb" title="Share on Facebook"
                               target="_blank"></a></li>
                        <li><a href="https://plus.google.com/share?url=http://hiphoptv.com/" class="gp" title="Google+"
                               target="_blank"></a></li>
                        <li><a href="https://twitter.com/home?status=http://hiphoptv.com/" class="tw" title="Twitter" target="_blank"></a>
                        </li>
                        <li><a href="https://pinterest.com/pin/create/button/?url=http://hiphoptv.com/" class="pin" title="Pinterest"
                               target="_blank"></a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal black fade" id="signup-popup" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><img src="/images/close.png"/>
                </button>
                <h4 class="modal-title">Sign Up</h4>
            </div>
            <!-- Begin MailChimp Signup Form -->
            <div id="mc_embed_signup" class="signup">
                <form action="//hiphoptv.us11.list-manage.com/subscribe/post?u=4aa112fafa6221bd0b656c981&amp;id=03cd463d70" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validate" target="_blank" novalidate>
                    <div id="mc_embed_signup_scroll">
                        <div class="modal-body">
                            <div class="mc-field-group">
                                <input type="text" value="" name="FNAME" class="form-control" id="mce-FNAME" placeholder="Full name">
                            </div>
                            <div class="mc-field-group size1of2">
                                <div class="datefield">
                                    <span class="subfield monthfield"><input class="datepart form-control" type="text" pattern="[0-9]*" value="" placeholder="MM" size="2" maxlength="2" name="MMERGE3[month]" id="mce-MMERGE3-month" /></span>
                                    <span class="subfield dayfield"><input class="datepart form-control" type="text" pattern="[0-9]*" value="" placeholder="DD" size="2" maxlength="2" name="MMERGE3[day]" id="mce-MMERGE3-day" /></span>
                                    <span class="subfield yearfield"><input class="datepart form-control" type="text" pattern="[0-9]*" value="" placeholder="YYYY" size="4" maxlength="4" name="MMERGE3[year]" id="mce-MMERGE3-year" /></span>
                                </div>
                            </div>
                            <div class="mc-field-group">
                                <input type="email" value="" name="EMAIL" class="form-control required email" id="mce-EMAIL" placeholder="Email address">
                            </div>
                            <div id="mce-responses" class="clear">
                                <div class="response" id="mce-error-response" style="display:none"></div>
                            </div>
                            <div style="position: absolute; left: -5000px;"><input type="text" name="b_4aa112fafa6221bd0b656c981_03cd463d70" tabindex="-1" value=""></div>
                        </div>
                        <div class="modal-footer clear"><input type="submit" value="Sign Up Now" name="subscribe" id="mc-embedded-subscribe" class="btn btn-block btn-signup btn-red"></div>
                    </div>
                </form>


                <?php /*$form = ActiveForm::begin(); */?><!--
                <fieldset class="registration-form">
                    <div class="row">
                        <div class="col-xs-6 ">
                            <?/*= $form->field($profile, 'name')->textInput(
                                ['placeholder' => $profile->getAttributeLabel('name')]
                            )->label(false) */?>
                        </div>
                        <div class="col-xs-6 ">
                            <?/*= $form->field($profile, 'surname')->textInput(
                                ['placeholder' => $profile->getAttributeLabel('surname')]
                            )->label(false) */?>
                        </div>
                    </div>

                    <?/*= $form->field($profile, 'date_of_birth')->textInput(
                        ['placeholder' => 'MM/DD/YYYY', 'id' => 'datepicker']
                    )->label(false) */?>

                    <?/*= $form->field($user, 'username')->textInput(
                        ['placeholder' => $user->getAttributeLabel('username')]
                    )->label(false)

                    */?>
                    <?/*= $form->field($user, 'email')->textInput(
                        ['placeholder' => $user->getAttributeLabel('email')]
                    )->label(false) */?>
                    <?/*= $form->field($user, 'password')->passwordInput(
                        ['placeholder' => $user->getAttributeLabel('password')]
                    )->label(false) */?>
                    <?/*= $form->field($user, 'repassword')->passwordInput(
                        ['placeholder' => $user->getAttributeLabel('repassword')]
                    )->label(false) */?>

                    <?/*= $form->field($profile, 'avatar_url')->widget(
                        Widget::className(),
                        [
                            'settings' => [
                                'url' => ['fileapi-upload']
                            ],
                            'crop' => true,
                            'cropResizeWidth' => 100,
                            'cropResizeHeight' => 100
                        ]
                    )->label(false) */?>
                    <?/*= Html::submitButton(
                        'SIGN UP NOW',
                        [
                            'class' => 'btn btn-block btn-signup btn-red'
                        ]
                    ) */?>
                </fieldset>
                <?php
/*                    if( $isPost && (count(ActiveForm::validate($user)) || count(ActiveForm::validate($profile)))) {
                        print '<div id="validate-failed"></div>';
                    }
                */?>
                <?php /*ActiveForm::end(); */?>


                <?php /*$authAuthChoice = AuthChoice::begin([
                    'baseAuthUrl' => ['/security/auth/auth']
                ]); */?>
                <ul>
                    <?php /*foreach ($authAuthChoice->getClients() as $client): */?>
                        <li><?php /*$authAuthChoice->clientLink($client, 'CONNECT WITH FACEBOOK') */?></li>
                    <?php /*endforeach; */?>
                </ul>
                --><?php /*AuthChoice::end(); */?>

            </div>
            <script type='text/javascript' src='//s3.amazonaws.com/downloads.mailchimp.com/js/mc-validate.js'></script><script type='text/javascript'>(function($) {window.fnames = new Array(); window.ftypes = new Array();fnames[0]='EMAIL';ftypes[0]='email';fnames[1]='FNAME';ftypes[1]='text';fnames[3]='MMERGE3';ftypes[3]='date';}(jQuery));var $mcj = jQuery.noConflict(true);</script>
            <!--End mc_embed_signup-->
        </div>
    </div>
</div>

<div class="modal black video fade" id="sign-in-blocked" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><img
                        src="/images/close.png"/></button>
            </div>
            <div class="modal-body">
                <p>
                    HIPHOPTV is under maintenance. You will receive an email within 1-2 days when it is back up.
                </p>
            </div>
        </div>
    </div>
</div>