<?php

?>

<div class="container-fluid about-us-wrapper">
    <div class="aboutus-container container">

        <div class="row">
            <div class="col-lg-12">
                <h1 class="about-us-header">about us</h1>
            </div>
        </div>

        <div class="row">
            <div class="col-lg-12 about-us-info-wrapper">
                <div class="about-us-info-container">

                    <div class="au-text-head">
                        <span class="au-red">Hiphoptv</span> is the world's leading all-premium music video and Hip Hop content Over the Top network in the world.
                    </div>

                    <div class="au-text-main">
                        Hiphoptv.com brings a library of HD music videos, exclusive original programming and live concert
                        performances to everyone on the planet who loves Hip Hop. Viewers can watch on-demand through
                        hiphoptv.com, the mobile web and apps for mobile/tablets and TVs, or through Hiphoptv.com,
                        the always-on broadcast-style linear Hiphoptv.com channel built by expert team of Hip Hop enthusiast.
                        Hiphoptv.com is designed for those who would rather watch Hip Hop and are the true urban
                        flavored tastemakers of the world.
                    </div>

                </div>
            </div>
        </div>

    </div>
</div>