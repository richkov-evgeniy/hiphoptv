<?php

namespace app\modules\info\controllers;

use yii\web\Controller;
use Yii;
use vova07\users\models\frontend\User;
use vova07\users\models\Profile;
use yii\widgets\ActiveForm;
use yii\web\Response;
use yii\helpers\Url;

class DefaultController extends Controller
{
    public function actionIndex()
    {
        if(!Yii::$app->user->getId()) {
            $this->redirect('/home-guest');
        }

        return $this->render('index');
    }


    public function actionAboutus()
    {
        if(!Yii::$app->user->getId()) {
            $this->redirect('/home-guest');
        }

        return $this->render('aboutus');
    }


    public function actionOurteam()
    {
        if(!Yii::$app->user->getId()) {
            $this->redirect('/home-guest');
        }

        return $this->render('ourteam');
    }


    public function actionPrivacypolicy()
    {
        return $this->render('privacy-policy');
    }


    public function actionHomeguest()
    {
        /*$user = new User(['scenario' => 'signup']);
        $profile = new Profile();

        if ($user->load(Yii::$app->request->post()) && $profile->load(Yii::$app->request->post())) {
            if ($user->validate() && $profile->validate()) {
                $user->populateRelation('profile', $profile);
                if ($user->save(false)) {

                    Yii::$app->user->login($user);
                    Yii::$app->session->setFlash(
                        'success',
                        'Your account has been created.'
                    );

                    $params = Yii::$app->request->getBodyParams();
                    $full_name = $params['Profile']['name'] . ' ' . $params['Profile']['surname'];
                    $date_array = explode('/', $params['Profile']['date_of_birth']);
                    $month = $date_array[0];
                    $day = $date_array[1];
                    $year = $date_array[2];
                    $email = $params['User']['email'];

                    $MailChimp = new \Drewm\MailChimp('399ac3bc19480b247eae9586588e63f5-us11');
                    $result = $MailChimp->call('lists/subscribe', array(
                        'id'                => '03cd463d70',
                        'email'             => array('email'=> $email),
                        'merge_vars'        => array('FNAME'=> $full_name, 'MMERGE3[month]' => $month, 'MMERGE3[day]' => $day, 'MMERGE3[year]' => $year),
                        'double_optin'      => false,
                        'update_existing'   => true,
                        'replace_interests' => false,
                        'send_welcome'      => true,
                    ));

                    return $this->goHome();
                } else {
                    Yii::$app->session->setFlash('danger', 'User sign up failed. Please try again!');
                    return $this->refresh();
                }
            } elseif (Yii::$app->request->isAjax) {
                Yii::$app->response->format = Response::FORMAT_JSON;
                return ActiveForm::validate($user);
            }
        }

        $isPost = Yii::$app->request->isPost;*/

        return $this->render('home-guest', [
            /*'user' => $user,
            'profile' => $profile,
            'isPost' => $isPost*/
        ]);
    }


    public function actionInvestors()
    {
        $access = false;

        $post_data = Yii::$app->request->post();
        if(!empty($post_data)) {
            if($post_data['password'] == 'HHTV2015') {
                $access = true;
            }
        }

        return $this->render('investors',
            [
                'access' => $access
            ]
        );
    }

}
