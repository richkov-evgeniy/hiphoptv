<?php
use yii\helpers\Url;

$this->title = $video_name;

$this->registerMetaTag(['property' =>'og:title', 'content' => 'HipHopTv']);
$this->registerMetaTag(['property' =>'og:type', 'content' => 'website']);
$this->registerMetaTag(['property' =>'og:url', 'content' => $video_url]);
$this->registerMetaTag(['property' =>'og:image', 'content' =>$video_image . "?height=200&amp;amp;crop=auto"]);

//$this->registerMetaTag(['property' =>'og:image:width', 'content' => '200']);

//$this->registerMetaTag(['property' =>'og:image:height', 'content' => '200']);


$this->registerMetaTag(['property' =>'og:description', 'content' => $video_name]);


?>

<!--<div class="fb-share-button" data-href="http://hiphoptvdev.com/video/watch/DMX---Don%27t-Call-Me" data-layout="button_count"></div>


<div class="fb-share-button" data-href="<?//= $video_url ?>" data-layout="button"></div>
-->
<div class="site-index">

    <div class="body-content">

        <div class="main-block">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="player">
                            <div class="video-content">
                                <div class="vod-container">
                                    <script src='//player.ooyala.com/v3/8716a37dac0c4bcebb4edaed06b01a36'></script><div id='ooyalaplayer' style='width:auto;height:517px'></div><script>OO.ready(function() { OO.Player.create('ooyalaplayer', '<?= $video_id ?>', {"autoplay":true}); });</script><noscript><div>Please enable Javascript to watch this video</div></noscript>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="container-player-icons-bar">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="player-icons-bar">
                            <ul>
                                <!--<li><a href="/#"><img src="<?//= $this->assetManager->publish('@vova07/themes/site/images/icon-share.png')[1] ?>"></a></li>-->
                                <li>
                                    <?php
                                        if(!$guest) :
                                    ?>
                                    <a id="add_to_playlist" class="registered" href="/#">
                                        <div class="atp-icon">

                                        </div>
                                        <!--<img src="<?/*//= $this->assetManager->publish('@vova07/themes/site/images/icon-plus.png')[1] */?>">-->
                                    </a>
                                    <div class="available-playlists">
                                        <span id="plus-current-video" data-id="<?= $video_id ?>"></span>
                                            <a href="/playlists/playlists/index/" target="_blank">+ Create new playlist</a>
                                        <?php foreach($playlists as $playlist) : ?>
                                            <a href="#" class="a-playlist" data-id="<?= $playlist['id'] ?>"><?= $playlist['playlist_name'] ?></a>
                                        <?php endforeach; ?>
                                    </div>

                                    <?php
                                        else :
                                    ?>
                                        <a id="add_to_playlist" class="unregistered" href="/#"><img src="<?= $this->assetManager->publish('@vova07/themes/site/images/icon-plus.png')[1] ?>"></a>
                                        <div class="available-playlists">
                                            <div id="plus-current-video" data-id="<?= $video_id ?>"></div>
                                            <div class="go_to_signin"><a href="/login/">SIGN IN</a> <span>TO ADD VIDEOS</span></div>
                                        </div>

                                    <?php
                                        endif;
                                    ?>

                                </li>
                                <li>
                                   <div class="player-current-info-single"> <?= $video_name ?></div>
                                </li>
                                <!--<li><a href="/#"><img src="<?//= $this->assetManager->publish('@vova07/themes/site/images/icon-i.png')[1] ?>"></a></li>-->
                                <div class="c-clearfix"></div>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>




</div>

