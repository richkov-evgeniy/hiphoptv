<?php

namespace app\modules\watch;

class Module extends \yii\base\Module
{
    public $controllerNamespace = 'app\modules\watch\controllers';

    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
