<?php

namespace app\modules\watch\controllers;

use yii\web\Controller;
use app\modules\watch\models\Videos;
use app\modules\playlists\models\Playlists;
use Yii;

class WatchController extends Controller
{
    public $defaultAction = 'index';

    public function actionWatch($id)
    {
       /* if(!Yii::$app->user->getId()) {
            $this->redirect('/home-guest');
        }*/

        $model_playlists = new Playlists();
        $playlists = $model_playlists->find()
            ->where(['user_id' => Yii::$app->user->getId()])
            ->asArray()
            ->all();

        $guest = Yii::$app->user->isGuest;

        $model_videos = new Videos();
        $video = $model_videos->find()
            ->where(['url_alias' => $id])
            ->one();

        $video_id = $video->embed_code;

        return $this->render('index', [
            'video_id' => $video_id,
            'video_image' => $video->preview_image_url,
            'video_url' =>$video->url,
            'video_name' => $video->name,
            'playlists' => $playlists,
            'guest' => $guest
        ]);
    }

}


