<?php

namespace app\modules\security\controllers;

use yii\web\Controller;

use yii\helpers\Url;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\authclient\ClientInterface;
use api\modules\v1\models\User;
use api\modules\v1\models\Profile;
use Yii;



class AuthController extends Controller
{
    /**
     * @inheritdoc
     */
   /* public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => ['login', 'auth'],
                        'roles' => ['?']
                    ],
                    [
                        'allow' => true,
                        'actions' => ['logout'],
                        'roles' => ['@']
                    ],
                ]
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post']
                ]
            ]
        ];
    }*/

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'auth' => [
                'class' => 'yii\authclient\AuthAction',
                'successCallback' => [$this, 'signinfb'],
            ]
        ];
    }

    /**
     * Displays the login page.
     *
     * @return string|\yii\web\Response
     */
    public function actionLogin()
    {
        $model = $this->module->manager->createLoginForm();

        if ($model->load(\Yii::$app->getRequest()->post()) && $model->login()) {
            return $this->goBack();
        }

        return $this->render('login', [
            'model' => $model
        ]);
    }

    /**
     * Logs the user out and then redirects to the homepage.
     *
     * @return \yii\web\Response
     */
    public function actionLogout()
    {
        \Yii::$app->getUser()->logout();

        return $this->goHome();
    }

    /**
     * Logs the user in if this social account has been already used. Otherwise shows registration form.
     *
     * @param  ClientInterface $client
     * @return \yii\web\Response
     */
    public function authenticate(ClientInterface $client)
    {
        /*$attributes = $client->getUserAttributes();
        $provider   = $client->getId();
        print '<pre>';
        print_r($attributes);
        print_r($client);
        print_r($provider);
        exit();*/



        /*$clientId   = $attributes['id'];

        if (null === ($account = $this->module->manager->findAccount($provider, $clientId))) {
            $account = $this->module->manager->createAccount([
                'provider'   => $provider,
                'client_id'  => $clientId,
                'data'       => json_encode($attributes)
            ]);
            $account->save(false);
        }

        if (null === ($user = $account->user)) {
            $this->action->successUrl = Url::to(['/user/registration/connect', 'account_id' => $account->id]);
        } else {
            \Yii::$app->user->login($user, $this->module->rememberFor);
        }*/
    }


    public function signinfb(ClientInterface $client)
    {
        $attributes = $client->getUserAttributes();
        $provider   = $client->getId();

        $service = $provider;
        $service_id = $attributes['id'];

        $user = User::findIdentifyBySocialId($service, $service_id);

        if($user) {
            Yii::$app->user->login($user);
        }   else {
                $user = User::findByEmail($attributes['email']);
                if($user) {
                    $user->service = $service;
                    $user->service_id = $service_id;
                    $user->save(false);
                    Yii::$app->user->login($user);
                }   else {
                    $user = new User(['scenario' => 'fb']);
                    $profile = new Profile();

                    if (isset($attributes['email']) && isset($service) && isset($service_id) &&
                        isset($attributes['first_name']) && isset($attributes['last_name'])) {

                        $user->email = $attributes['email'];
                        $user->service = $service;
                        $user->service_id = $service_id;

                        $profile->name = $attributes['first_name'];
                        $profile->surname = $attributes['last_name'];
                        if(isset($attributes['birthday'])){
                            $profile->date_of_birth = $attributes['birthday'];
                        }

                        if ($user->validate() && $profile->validate()) {
                            $user->populateRelation('profile', $profile);

                            if ($user->save(false)) {
                                Yii::$app->user->login($user);

                                $full_name = $attributes['first_name'] . ' ' . $attributes['last_name'];
                                $date_array = explode('/', $attributes['birthday']);
                                $month = $date_array[0];
                                $day = $date_array[1];
                                $year = $date_array[2];
                                $email = $attributes['email'];

                                $MailChimp = new \Drewm\MailChimp('399ac3bc19480b247eae9586588e63f5-us11');
                                $result = $MailChimp->call('lists/subscribe', array(
                                    'id'                => '03cd463d70',
                                    'email'             => array('email'=> $email),
                                    'merge_vars'        => array('FNAME'=> $full_name, 'MMERGE3[month]' => $month, 'MMERGE3[day]' => $day, 'MMERGE3[year]' => $year),
                                    'double_optin'      => false,
                                    'update_existing'   => true,
                                    'replace_interests' => false,
                                    'send_welcome'      => true,
                                ));

                            } else {
                                $response_data['status'] = false;
                                $response_data['body'] = null;
                                $response_data['message'] = "User sign up failed. Please try again";
                                return ($response_data);
                            }
                        }   else {
                            $response_data['status'] = false;
                            $response_data['body'] = null;
                            $response_data['message'] = "User sign up failed. Please try again. Not validate";
                            return ($response_data);
                        }
                    }   else {
                        $response_data['status'] = false;
                        $response_data['body'] = null;
                        $response_data['message'] = "User sign up failed. Please try again. Not validate";
                        return ($response_data);
                    }
                }
            }
    }

}
