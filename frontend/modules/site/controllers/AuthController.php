<?php

namespace app\modules\site\controllers;

use yii\web\Controller;

class AuthController extends Controller
{
    public function actionIndex()
    {
        return 'check';
    }



    public function actions()
    {
        return [
            'auth' => [
                'class' => 'yii\authclient\AuthAction',
                'successCallback' => [$this, 'successCallback'],
            ],
        ];
    }

    public function successCallback($client)
    {
        $attributes = $client->getUserAttributes();
        // user login or signup comes here
    }
}