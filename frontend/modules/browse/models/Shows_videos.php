<?php
namespace app\modules\browse\models;


use yii\db\ActiveRecord;
use Yii;


class Shows_videos extends ActiveRecord
{

    public function getVideos()
    {
        return $this->hasOne(Videos::className(), ['embed_code' => 'video_embed_code']);
    }

    public function getShows()
    {
        return $this->hasOne(Shows::className(), ['embed_code' => 'show_embed_code']);
    }

}