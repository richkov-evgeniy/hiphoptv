<?php
namespace app\modules\browse\models;


use yii\db\ActiveRecord;
use Yii;


class Labels_videos extends ActiveRecord
{
    public function getVideos()
    {
       return $this->hasMany(Videos::className(), ['embed_code' => 'video_embed_code']);
    }


    public function getLabels()
    {
        return $this->hasOne(Labels::className(), ['label_id' => 'label_id'])->where(['parent_id' => 'ec7508d8c2694b17b47eda1e416327f0']);
    }

}