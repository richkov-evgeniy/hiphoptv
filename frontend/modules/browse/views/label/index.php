<?php
use yii\helpers\Url;
?>


<div class="container-fluid show-page">

    <div class="show-banner">
        <!--<img src="">-->
        <div class="show-banner-picture container">
            <img src="<?= $this->assetManager->publish('@vova07/themes/site/images/kidink1.png')[1] ?>">
        </div>
    </div>

    <div class="container-fluid main-text-wrapper">
        <div class="main-text-container container">
            <div class="main-text">
                Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old.Contrary to popular belief,
                Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old.Contrary to popular belief, Lorem Ipsum is not
                simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old.
            </div>
        </div>
    </div>


    <div class="container-fluid show-list-wrapper">
        <div class="video-list-container container">
            <div class="video-list">
                <?php
                //print "<pre>";
                //   print_r($videos);
                // print "</pre>";
                foreach ($videos as $video) :
                    ?>

                    <div class='video-item-wrapper col-lg-4 col-md-4 col-sm-4 col-xs-12'>
                        <div class="video-item-container">
                            <a href="<?= Url::to(['/watch/watch/watch', 'id' => $video->videos[0]->embed_code ]); ?>"><img src="<?= $video->videos[0]->preview_image_url ?>"></a>
                            <div class="video-info">
                                <a href="<?= Url::to(['/watch/watch/watch', 'id' => $video->videos[0]->embed_code]); ?>" class="video-link"><?= $video->videos[0]->name ?></a>
                                <div class="show-description">
                                    <!--<?//= $video->videos[0]->description ?> -->
                                    1 video
                                </div>
                            </div>
                        </div>
                    </div>

                <?php
                endforeach;
                ?>
            </div>
        </div>
    </div>



</div>
