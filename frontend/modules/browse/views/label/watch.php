<?php
use yii\helpers\Url;
?>

<div class="container-fluid label-page">


    <!--
    <div class="show-banner">
        <div class="show-banner-picture container">
            <img src="<?//= $this->assetManager->publish('@vova07/themes/site/images/kidink1.png')[1] ?>">
        </div>
    </div>
    -->
        <div class="container-fluid label-head-wrapper">
            <div class=" container">
                <div class="row">
                    <div class="label-name col-lg-12">
                        <h1><?= $label[0]['name']  ?></h1>
                    </div>
                </div>
            </div>
        </div>


        <div class="label-wrapper">
            <div class="label-container container">
                <div class="row">
                    <?php
                    //$limit_video = 6;
                    ?>
                    <?php foreach($videos as $video) : ?>
                        <?php //$limit_video--; ?>
                        <div class='label-video-wrapper col-lg-2 col-md-2 col-sm-2 col-xs-6'>
                            <div class="label-video-container">
                                <a href="<?= Url::to(['/watch/watch/watch', 'id' => $video->videos[0]->url_alias]); ?>">
                                    <img class="play_button" src="<?= $this->assetManager->publish('@vova07/themes/site/images/play_button.png')[1]?>" >
                                    <img src="<?= $video->videos[0]->preview_image_url ?>" class="label-video-img"></a>
                                <div class="label-video-info">
                                    <a href="<?= Url::to(['/watch/watch/watch', 'id' => $video->videos[0]->url_alias]); ?>" class="label-video-link"><?= $video->videos[0]->name ?></a>
                                    <!--<div class="label-video-views">
                                        41,503 views
                                    </div>-->
                                </div>
                            </div>
                        </div>
                        <?php //if($limit_video == 0) break; ?>
                    <?php endforeach; ?>
                    <div class="c-clearfix"></div>
                </div>
            </div>
        </div>


</div>
