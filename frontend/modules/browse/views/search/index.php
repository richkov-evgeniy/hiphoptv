<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
?>

<div class="container-fluid browse-wrapper">
    <div class="browse-container container">
        <div class="row">
            <div class="col-lg-12">
                <?php
                    echo Html::input('text', 'search', null, ['class'=>'input-search', 'placeholder'=>'Search for artists and videos']);
                ?>
            </div>
        </div>
        <div class="search-results">
            <!--<div class="artists-results">
                <div class="row">
                    <div class="section-heading col-lg-12">
                        <h2 class="h1-style"><a href="#" class="head-style">Artists Results</a></h2>
                        <div class="c-clearfix"></div>
                    </div>
                </div>
                <div class="artists-results-container row">
                </div>
                <div class="c-clearfix"></div>
            </div>-->

            <div class="video-results">
                <div class="row">
                    <div class="section-heading col-lg-12">
                        <h2 class="h1-style"><a href="#" class="head-style">Video Results</a></h2>
                        <div class="c-clearfix"></div>
                    </div>
                </div>
                <div class="video-results-container row">
                </div>
                <div class="c-clearfix"></div>
            </div>

        </div>




    </div>
</div>
