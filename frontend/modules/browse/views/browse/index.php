<?php

use app\modules\vod\Module;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\jui\AutoComplete;
?>

<div class="container-fluid browse-wrapper">
    <div class="browse-container container">



        <!--<div class="original-shows-wrapper row">
            <div class="section-heading col-lg-12">
                <h1><a class="head-style" >Original Shows</a></h1>
                <a class="see-more" href="/shows">See more</a>
                <div class="c-clearfix"></div>
            </div>
            <?php
/*
            foreach ($shows as $show):
                */?>
                <div class='shows-item-wrapper col-lg-4 col-md-4 col-sm-4 col-xs-12'>
                    <div class="shows-item-container">

                        <a href="<?/*= Url::to(['/browse/shows/watch', 'id' => $show->url_alias]); */?>">
                            <img class="play_button" src="<?/*= $this->assetManager->publish('@vova07/themes/site/images/play_button.png')[1]*/?>" >
                            <img class="show-thumb" src="<?/*= $show->preview_image_url */?>">
                        </a>
                        <div class="shows-info">
                            <a href="<?/*= Url::to(['/browse/shows/watch', 'id' => $show->url_alias]); */?>" class="show-link"><?/*= $show->name */?></a>
                            <div class="show-description">
                                <?/*= $show->description */?>
                            </div>
                        </div>
                    </div>
                </div>
            <?php
/*            endforeach;
            */?>
            <div class="c-clearfix"></div>
        </div>-->

        <?php foreach($array_labels_videos as $key=> $label_relation) : ?>
        <div class="label-wrapper row">
                <div class="section-heading col-lg-12">
                    <h2 class="h1-style"><a class="head-style"><?= $label_relation['name'] ?></a></h2>
                    <a class="see-more" href="<?= Url::to(['/browse/label/watch', 'id' => $label_relation['name'] ]); ?>">See more </a>
                    <div class="c-clearfix"></div>
                </div>
                <?php
                $limit_video = 6;
                ?>
                <?php foreach($label_relation['videos'] as $video) : ?>
                    <?php $limit_video--; ?>
                    <div class='label-video-wrapper col-lg-2 col-md-2 col-sm-2 col-xs-6'>
                        <div class="label-video-container">
                            <a href="<?= Url::to(['/watch/watch/watch', 'id' => $video['object']->url_alias]); ?>">
                                <img class="play_button" src="<?= $this->assetManager->publish('@vova07/themes/site/images/play_button.png')[1]?>" >
                                <img src="<?= $video['object']->preview_image_url ?>" class="label-video-img">
                            </a>
                            <div class="label-video-info">
                                <a href="<?= Url::to(['/watch/watch/watch', 'id' => $video['object']->url_alias]); ?>" class="label-video-link"><?= $video['object']->name ?></a>
                                <?php if(isset($video['link_to_artist'])) : ?>
                                    <a href="<?= Url::to(['/browse/label/watch', 'id' => $video['link_to_artist']->name]); ?>" class="link_to_artist"><?= $video['link_to_artist']->name ?></a>
                                <?php endif; ?>
                                <!--<div class="label-video-views">
                                    41,503 views
                                </div>-->
                            </div>
                        </div>
                    </div>
                    <?php if($limit_video == 0) break; ?>
                <?php endforeach; ?>
                <div class="c-clearfix"></div>
        </div>
        <?php endforeach;

        ?>


    </div>
</div>
