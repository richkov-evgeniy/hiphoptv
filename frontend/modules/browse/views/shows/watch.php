<?php
use yii\helpers\Url;
?>


<div class="container-fluid show-page">
        <!--
            <div class="show-banner">
                <div class="show-banner-picture container">
                    <img src="<?//= $this->assetManager->publish('@vova07/themes/site/images/kidink1.png')[1] ?>">
                </div>
            </div>
        -->
    <div class="container-fluid main-text-wrapper">
        <div class="main-text-container container">
            <div class="row">
                <div class="show-name col-lg-12">
                    <h1><?= $show['name']  ?></h1>
                </div>
                <div class="main-text col-lg-12">
                    <?= $show['description']  ?>
                </div>
            </div>
        </div>
    </div>


    <div class="container-fluid show-list-wrapper">
        <div class="video-list-container container">
            <div class="video-list row">
                <?php
                foreach ($videos as $video) :
                    if(!empty($video->videos)) :
                ?>

                        <div class='video-item-wrapper col-lg-4 col-md-4 col-sm-4 col-xs-12'>
                            <div class="video-item-container">
                                <a href="<?= Url::to(['/watch/watch/watch', 'id' => $video->videos->url_alias ]); ?>">
                                    <img class="play_button" src="<?= $this->assetManager->publish('@vova07/themes/site/images/play_button.png')[1]?>" >
                                    <img class="video-thumb" src="<?= $video->videos->preview_image_url ?>">
                                </a>
                                <div class="video-info">
                                    <a href="<?= Url::to(['/watch/watch/watch', 'id' => $video->videos->url_alias]); ?>" class="video-link"><?= $video->videos->name ?></a>
                                    <!--<div class="show-description">
                                        1 video
                                    </div>-->
                                </div>
                            </div>
                        </div>

                <?php
                    endif;
                endforeach;
                ?>
            </div>
        </div>
    </div>



</div>
