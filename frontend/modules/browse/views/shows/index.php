<?php

use app\modules\vod\Module;
use yii\helpers\Html;
use yii\helpers\Url;

?>

<div class="container-fluid shows-catalog-wrapper">
    <div class="shows-catalog container">
        <div class="row">

            <div class="section-heading col-lg-12">
                <h1><a class="head-style" href="#">Original Shows</a></h1>
                <div class="c-clearfix"></div>
            </div>

            <?php
            foreach ($shows as $show):
                ?>
                <div class='shows-item-wrapper col-lg-4 col-md-4 col-sm-4 col-xs-12'>
                    <div class="shows-item-container">
                        <a href="<?= Url::to(['/browse/shows/watch', 'id' => $show->url_alias]); ?>">
                            <img class="play_button" src="<?= $this->assetManager->publish('@vova07/themes/site/images/play_button.png')[1]?>" >
                            <img class="show-thumb" src="<?= $show->preview_image_url ?>">
                        </a>
                        <div class="shows-info">
                            <a href="<?= Url::to(['/browse/shows/watch', 'id' => $show->embed_code]); ?>" class="show-link"><?= $show->name ?></a>
                            <div class="show-description">
                                <?= $show->description ?>
                            </div>
                        </div>
                    </div>
                </div>
            <?php
            endforeach;
            ?>
            <div class="c-clearfix"></div>
        </div>
    </div>
</div>
