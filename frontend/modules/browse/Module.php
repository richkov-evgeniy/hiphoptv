<?php

namespace app\modules\browse;

class Module extends \yii\base\Module
{
    public $controllerNamespace = 'app\modules\browse\controllers';

    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
