<?php

namespace app\modules\browse\controllers;

use yii\web\Controller;
use app\modules\browse\models\Labels_videos;
use app\modules\browse\models\Labels;
use Yii;

class LabelController extends Controller
{

    public function actionWatch($id)
    {
        if(!Yii::$app->user->getId()) {
            $this->redirect('/home-guest');
        }


        $model_labels = new Labels();
        $label = $model_labels->find()
            ->where(['name' => $id])
            ->asArray()
            ->all();


        $model_labels_videos = new Labels_videos();
        $videos = $model_labels_videos->find()
            ->joinWith('videos')
            ->where(['label_id' => $label[0]['label_id']])
            ->orderBy('id')
            ->all();



        return $this->render('watch', [
            'videos' => $videos,
            'label' => $label
        ]);
    }

}


