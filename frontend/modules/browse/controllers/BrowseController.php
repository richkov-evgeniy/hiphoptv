<?php

namespace app\modules\browse\controllers;

use yii\web\Controller;
use app\modules\browse\models\Shows;
use app\modules\browse\models\Labels_videos;
use app\modules\browse\models\Videos;
use app\modules\browse\models\Labels;
use Yii;

class BrowseController extends Controller
{

    public function actionIndex()
    {

        if(!Yii::$app->user->getId()) {
            $this->redirect('/home-guest');
        }

        $model_labels_videos = new Labels_videos();

        $relation_videos = $model_labels_videos->find()
            ->joinWith('videos')
            ->orderBy('id')
            ->all();

        $array_labels_videos = array();

        foreach ($relation_videos as $relation_video) {
            $array_labels_videos[$relation_video->label_id]['videos'][]['object'] = $relation_video->videos[0];
        }

        $model_labels = new Labels();


        $labels = $model_labels->find()->where(['name' => ['Music Videos', 'Original Content', 'Documentaries', 'Live Performances']])->all();



        $filterd_array_labels_videos = array();

        foreach ($labels as $label) {
            if(array_key_exists($label->label_id, $array_labels_videos)) {
                $array_labels_videos[$label->label_id]['name'] = $label->name;
                $filterd_array_labels_videos[$label->label_id] = $array_labels_videos[$label->label_id];
            }
        }


        foreach ($filterd_array_labels_videos as $key_falv => $value) {
            foreach ($value['videos'] as $key_v => $video) {
                $link_to_artist = $model_labels_videos->find()
                    ->andWhere(['video_embed_code' => $video['object']['embed_code']])
                    ->joinWith('labels')
                    ->all();
                if($link_to_artist) {
                    $filterd_array_labels_videos[$key_falv]['videos'][$key_v]['link_to_artist'] = $link_to_artist[0]['labels'];
                }

            }
        }

        /*$model = new Shows();
        $shows = $model->find()
            ->where(['status' => 'live'])
            ->orderBy('id')
            ->limit(3)
            ->all();*/


        return $this->render('index', [
            'array_labels_videos' => $filterd_array_labels_videos
            //'shows' => $shows
        ]);
    }
}


