<?php

namespace app\modules\browse\controllers;

use yii\web\Controller;
use app\modules\browse\models\Shows;
use app\modules\browse\models\Labels_videos;
use app\modules\browse\models\Videos;
use app\modules\browse\models\Labels;
use Yii;

class SearchController extends Controller
{

    public function actionIndex()
    {
        return $this->render('index', [

        ]);
    }


    public function actionSearch() {

        if(!Yii::$app->user->getId()) {
            //$this->redirect('/login');
            $search_results = array();
            $search_results = false;
            echo json_encode($search_results);

        }   else {
            $search_results = array();

            if(!empty($_GET['text'])) {
                /*$labels_model = new Labels();
                $artists = $labels_model->find()->limit(6)
                    ->andWhere(['parent_id' => 'ec7508d8c2694b17b47eda1e416327f0'])
                    ->andWhere(['LIKE', 'name', '%' . $_GET['text'] . '%', false])
                    ->joinWith('lv')
                    ->asArray()
                    ->all();

                $search_results['artists'] = $artists;*/

                $search_results['artists'] = array();

                $videos_model = new Videos();
                $videos = $videos_model->find()
                    ->orWhere(['LIKE', 'name', '%' . $_GET['text'] . '%', false])
                    ->asArray()
                    ->all();

                $search_results['videos'] = $videos;

            } else {
                $search_results['artists'] = array();
                $search_results['videos'] = array();
            }

            echo json_encode($search_results);
        }


    }
}


