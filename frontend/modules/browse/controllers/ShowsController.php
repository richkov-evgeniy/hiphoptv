<?php

namespace app\modules\browse\controllers;

use yii\web\Controller;
use app\modules\browse\models\Shows;
use app\modules\browse\models\Shows_videos;
use app\modules\browse\models\Videos;
use Yii;

class ShowsController extends Controller
{

    public function actionIndex()
    {
        if(!Yii::$app->user->getId()) {
            $this->redirect('/home-guest');
        }

        $model = new Shows();
        $shows = $model->find()
            ->where(['status' => 'live'])
            ->orderBy('id')
            ->all();

        return $this->render('index',
            [
                'shows' => $shows
            ]
        );
    }


    public function actionWatch($id)
    {
        if(!Yii::$app->user->getId()) {
            $this->redirect('/home-guest');
        }


        $show_model = new Shows();
        $show = $show_model->find()
            ->asArray()
            ->where(['url_alias' => $id])
            ->one();


        $model_shows_videos = new Shows_videos();
        $videos = $model_shows_videos->find()
            ->joinWith('videos')
            ->where(['show_embed_code' => $show['embed_code']])
            ->orderBy('id')
            ->all();
/*

        print "<pre>";
        print_r($videos);
        exit();*/

        return $this->render('watch', [
            'show' => $show,
            'videos' => $videos
        ]);
    }

}


