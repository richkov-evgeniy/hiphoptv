/**
 * Created by root on 18.11.14.
 */
$(document).ready(function() {


    $('.show-hide-schedule').click(function(e){
        e.preventDefault();

        if($('.sh-arrow i').hasClass('Chevron-top')) {
            $('.sh-arrow i').removeClass('Chevron-top');
            $('.sh-arrow i').addClass('Chevron-bottom');
            $('.sh-label').text('hide schedule');
        }   else {
            $('.sh-arrow i').removeClass('Chevron-bottom');
            $('.sh-arrow i').addClass('Chevron-top');
            $('.sh-label').text('show schedule');
        }
        $('.schedule-content').slideToggle(500);
    });


    $('.glyphicon-search a').click(function(e){
        e.preventDefault();
        $('.search-modal').css('display', 'block');
        $('body').css('overflow-y', 'hidden');
        $('.wrap').css('margin-left', '-15px');
    });


    $('.close-search-modal').click(function(e){
        e.preventDefault();
        $('.search-modal').css('display', 'none');
        $('body').css('overflow-y', 'auto');
        $('.wrap').css('margin-left', '0px');
    });

    $('input[name="search"]').keyup(function(e){
        e.preventDefault();
        Search($('input[name="search"]').val());
    });


    function Search(input_text) {
        jQuery.ajax({
            url: '/browse/search/search/?text=' + input_text,
            type: 'GET',
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function(data){
                console.log(data);
                if(data.artists.length + data.videos.length) {
                    $('.search-results').css('display', 'block');
                }   else {
                    $('.search-results').css('display', 'none');
                }
               $('.artists-results-container').text('');
                $('.video-results-container').text('');
                     /*
                    $.each(data.artists, function (k, v) {
                        $('.artists-results-container').append(
                            '<div  id="artist-' + v.id + '" class="col-lg-2 col-md-2 col-sm-2 col-xs-6 artist-item-wrapper">' +
                                '<div class="artist-item-container">' +
                                    '<a href="/browse/label/watch/?id=' + v.label_id + '"><img class="play_button" src="/assets/be4c9c0e/play_button.png"><img class="results-artist-thumb" src="http://ak.c.ooyala.com/00aHZ0cTopuAQY4b_5YVU6pKJgyo1xLk/Ut_HKthATH4eww8X4xMDoxOjBmO230Ws"></a>' +
                                    '<div class="artist-info">' +
                                        '<a href="/browse/label/watch/?id=' + v.label_id + '" class="artist-name">' + v.name + '</a>' +
                                        '<div class="video-count">' + v.lv.length + ' videos</div>' +
                                    '<div>' +
                                '</div>' +
                            '</div>'
                        );
                    });
                    $('.artists-results-container').append('<div class="c-clearfix"></div>');*/


                    $.each(data.videos, function (k, v) {
                        $('.video-results-container').append(
                            '<div  id="video-' + v.id + '" class="col-lg-2 col-md-2 col-sm-2 col-xs-6 artist-item-wrapper">' +
                                '<div class="video-item-container ">' +
                                    '<a href="/watch/watch/watch/?id=' + v.embed_code + '"><img class="play_button" src="/assets/be4c9c0e/play_button.png"><img class="results-video-thumb" src="' + v.preview_image_url + '">' +
                                    '<div class="search-video-info">' +
                                        '<a href="/watch/watch/watch/?id=' + v.embed_code + '" class="video-name">' + v.name + '</a>' +
                                    '<div>' +
                                '</div>' +
                            '</div>'
                        );
                    });
                    $('.artists-results-container').append('<div class="c-clearfix"></div>');
            }
        });
    }


    //playlist event create
    $('#create-playlist-submit').click(function(e){
        e.preventDefault();
        CreatePlaylist($('#playlist-name').val());
    });



    function CreatePlaylist(playlist_name) {
        jQuery.ajax({
            type: "POST",
            url:  '/playlists/playlists/create/',
            dataType: "json",
            data:
            {
                _csrf : $('meta[name="csrf-token"]').attr('content'),
                playlist_name: playlist_name
            },
            success: function(data){
                window.location.href = "/playlists/playlists/index/";
            }
        });
    }


    //add video to playlist event
    $('.available-playlists .a-playlist').click(function(e){
        e.preventDefault();
        if($(this).hasClass('not-available')) {
            return;
        }
        AddToPlaylist($(this).data('id'), $('#plus-current-video').data('id'));
    });


    function AddToPlaylist(playlist_id, video_id) {
        jQuery.ajax({
            type: "POST",
            url:  '/playlists/playlists/addvideo/',
            dataType: "json",
            data:
            {
                _csrf : $('meta[name="csrf-token"]').attr('content'),
                playlist_id : playlist_id,
                video_id : video_id
            },
            success: function(data){
                console.log(data);
                $('*[data-id="' + playlist_id + '"]').addClass('not-available');
                if( $('.available-playlists ').css('display') == 'none') {
                    $('.available-playlists ').css('display', 'block');
                }   else {
                    $('.available-playlists ').css('display', 'none');
                }
            }
        });
    }


    //event show playlist added box
    $('#add_to_playlist.registered').click(function(e){
        e.preventDefault();
        if( $('.available-playlists ').css('display') == 'none') {
            $('.available-playlists ').css('display', 'block');
        }   else {
            $('.available-playlists ').css('display', 'none');
        }
    });


    //event show playlist added box if user is guest
    $('#add_to_playlist.unregistered').click(function(e){
        e.preventDefault();
        if( $('.available-playlists ').css('display') == 'none') {
            $('.available-playlists ').css('display', 'block');
        }   else {
            $('.available-playlists ').css('display', 'none');
        }
    });


    //playlist event delete
    $('.pl-delete').click(function(e){
        e.preventDefault();
        DeletePlaylist($(this).data('id'));
    });

    $('#playlist-delete').click(function(e){
        e.preventDefault();
        DeletePlaylist($('.playlist-edit-page-container').data('id'));
    });


    function DeletePlaylist(playlist_id) {
        jQuery.ajax({
            type: "POST",
            url:  '/playlists/playlists/delete/',
            dataType: "json",
            data:
            {
                _csrf : $('meta[name="csrf-token"]').attr('content'),
                playlist_id: playlist_id
            },
            success: function(data){
                window.location.href = "/playlists/playlists/index/";
            }
        });
    }


    //draggable & sortable videos in playlist
    $("#sortable").sortable({
        revert: true,
        stop: function(event, ui) {
            if(!ui.item.data('tag') && !ui.item.data('handle')) {
                ui.item.data('tag', true);
                ui.item.fadeTo(400);
            }
        }
    });
    $("#draggable").draggable({
        connectToSortable: '#sortable',
        helper: 'clone',
        revert: 'invalid'
    });
    $("ul, li").disableSelection();


    $( ".playlist-videos li" ).hover(
        function() {
            $('a', $(this)).css('color', 'rgb(211, 174, 65)');
            $('.position-pl', $(this)).css('color', 'rgb(211, 174, 65)');
            $('.pl-video-container', $(this)).css('border', 'solid 1px rgb(211, 174, 65)');
        },
        function() {
            $('a', $(this)).css('color', 'rgb(231, 231, 231)');
            $('.position-pl', $(this)).css('color', 'rgb(231, 231, 231)');
            $('.pl-video-container', $(this)).css('border', 'solid 1px rgb(54, 54, 54)');
        }
    );


    //playlist update event
    $('#playlist-save').click(function(e){
        e.preventDefault();
        var videos_sort = [];
        var counter = 0
        $('.playlist-videos li.video-available').each(function(){
            counter++;
            videos_sort[counter] = $(this).data('id');
        });
        UpdatePlaylist($('.playlist-edit-page-container').data('id'), $('#edit-playlist-name').val(), videos_sort, $('.playlist-edit-page-container').data('name'));
    });


    function UpdatePlaylist(playlist_id, playlist_name, videos_sort, old_playlist_name) {
        jQuery.ajax({
            type: "POST",
            url:  '/playlists/playlists/update/',
            dataType: "json",
            data:
            {
                _csrf : $('meta[name="csrf-token"]').attr('content'),
                playlist_id: playlist_id,
                playlist_name: playlist_name,
                videos_sort: videos_sort,
                old_playlist_name: old_playlist_name
            },
            success: function(data){
                window.location.href = "/playlists/playlists/index/";
            }
        });
    }


    //playlist delete video event
    $('.pl-video-delete-link').click(function(e){
        e.preventDefault();
        var parent = $(this).closest('li');
        if(parent.hasClass('video-available')) {
            parent.removeClass('video-available');
            parent.addClass('video-unavailable');
            $(this).text('+');
        }   else {
            parent.removeClass('video-unavailable');
            parent.addClass('video-available');
            $(this).text('x');
        }

    });

    /*//playlist event create
    $('#create-playlist-submit').click(function(e){
        e.preventDefault();
        CreatePlaylist($('#playlist-name').val());
    });



    function CreatePlaylist(playlist_name) {
        jQuery.ajax({
             type: "POST",
             url:  '/playlist/playlist/create/',
             dataType: "json",
             data:
             {
                _csrf : $('meta[name="csrf-token"]').attr('content'),
                playlist_name: playlist_name
             },
            success: function(data){
                //console.log(data);
                window.location.href = "/playlist/playlist/index/";
            }
        });
    }


    //add video to playlist event
    $('.available-playlists .a-playlist').click(function(e){
        e.preventDefault();
        if($(this).hasClass('not-available')) {
            return;
        }
        AddToPlaylist($(this).data('id'), $('#plus-current-video').data('id'));
    });


    function AddToPlaylist(playlist_id, video_id) {
        jQuery.ajax({
            type: "POST",
            url:  '/playlist/playlist/addvideo/',
            dataType: "json",
            data:
            {
                _csrf : $('meta[name="csrf-token"]').attr('content'),
                playlist_id : playlist_id,
                video_id : video_id
            },
            success: function(data){
                console.log(data);
                $('*[data-id="' + playlist_id + '"]').addClass('not-available');
                if( $('.available-playlists ').css('display') == 'none') {
                    $('.available-playlists ').css('display', 'block');
                }   else {
                    $('.available-playlists ').css('display', 'none');
                }
            }
        });
    }


    //event show playlist added box
    $('#add_to_playlist.registered').click(function(e){
        e.preventDefault();
        if( $('.available-playlists ').css('display') == 'none') {
            $('.available-playlists ').css('display', 'block');
        }   else {
            $('.available-playlists ').css('display', 'none');
        }
    });


    //event show playlist added box if user is guest
    $('#add_to_playlist.unregistered').click(function(e){
        e.preventDefault();
        if( $('.available-playlists ').css('display') == 'none') {
            $('.available-playlists ').css('display', 'block');
        }   else {
            $('.available-playlists ').css('display', 'none');
        }
    });



    //playlist event delete
    $('.pl-delete').click(function(e){
        e.preventDefault();
        DeletePlaylist($(this).data('id'));
    });

    $('#playlist-delete').click(function(e){
        e.preventDefault();
        DeletePlaylist($('.playlist-edit-page-container').data('id'));
    });


    function DeletePlaylist(playlist_id) {
        jQuery.ajax({
            type: "POST",
            url:  '/playlist/playlist/delete/',
            dataType: "json",
            data:
            {
                _csrf : $('meta[name="csrf-token"]').attr('content'),
                playlist_id: playlist_id
            },
            success: function(data){
                window.location.href = "/playlist/playlist/index/";
            }
        });
    }





    //draggable & sortable videos in playlist
    $("#sortable").sortable({
        revert: true,
        stop: function(event, ui) {
            if(!ui.item.data('tag') && !ui.item.data('handle')) {
                ui.item.data('tag', true);
                ui.item.fadeTo(400);
            }
        }
    });
    $("#draggable").draggable({
        connectToSortable: '#sortable',
        helper: 'clone',
        revert: 'invalid'
    });
    $("ul, li").disableSelection();


    $( ".playlist-videos li" ).hover(
        function() {
            $('a', $(this)).css('color', 'rgb(211, 174, 65)');
            $('.position-pl', $(this)).css('color', 'rgb(211, 174, 65)');
            $('.pl-video-container', $(this)).css('border', 'solid 1px rgb(211, 174, 65)');
        },
        function() {
            $('a', $(this)).css('color', 'rgb(231, 231, 231)');
            $('.position-pl', $(this)).css('color', 'rgb(231, 231, 231)');
            $('.pl-video-container', $(this)).css('border', 'solid 1px rgb(54, 54, 54)');
        }
    );


    //playlist update event
    $('#playlist-save').click(function(e){
        e.preventDefault();
        var videos_sort = [];
        var counter = 0
        $('.playlist-videos li.video-available').each(function(){
            counter++;
            videos_sort[counter] = $(this).data('id');
        });
        UpdatePlaylist($('.playlist-edit-page-container').data('id'), $('#edit-playlist-name').val(), videos_sort, $('.playlist-edit-page-container').data('name'));
    });


    function UpdatePlaylist(playlist_id, playlist_name, videos_sort, old_playlist_name) {
        jQuery.ajax({
            type: "POST",
            url:  '/playlist/playlist/update/',
            dataType: "json",
            data:
            {
                _csrf : $('meta[name="csrf-token"]').attr('content'),
                playlist_id: playlist_id,
                playlist_name: playlist_name,
                videos_sort: videos_sort,
                old_playlist_name: old_playlist_name
            },
            success: function(data){
                window.location.href = "/playlist/playlist/index/";
            }
        });
    }


    //playlist delete video event
    $('.pl-video-delete-link').click(function(e){
        e.preventDefault();
        var parent = $(this).closest('li');
        if(parent.hasClass('video-available')) {
            parent.removeClass('video-available');
            parent.addClass('video-unavailable');
            $(this).text('+');
        }   else {
            parent.removeClass('video-unavailable');
            parent.addClass('video-available');
            $(this).text('x');
        }

    });*/


   /* OO.ready(function() {
        var player = OO.Player.create('ooyalaplayer', 'c5NHZ0cTofjJ50WA-XBqvCIyfz0jLsFA', {
            height:'100%',
            width:'100%',
            onCreate: function(player) { window.messageBus = player.mb;}
        });

        $('#p-pause').click(function(e){
            e.preventDefault();
            //player.destroy();
            //OO.Player.pause('ooyalaplayer', 'c5NHZ0cTofjJ50WA-XBqvCIyfz0jLsFA');

        });


        window.messageBus.subscribe(OO.EVENTS.PLAYING, 'example', function(eventName) {
            alert("Player is now playing!!!");
        });*/

        //window.messageBus.publish(OO.EVENTS.PLAY);


       /* window.messageBus.intercept(OO.EVENTS.PAUSED, 'example', function(eventName) {
            if (confirm("Pause Video?") == true) { window.messageBus.publish('user_allowed_pause'); }
        });*/


   //});


    if($('#watch-playlist').size()) {
        OO.ready(function() {

            var embedcodes = [];
            var counter = 0;
            $('.playlist-videos-list .pl-video-item').each(function(){
                embedcodes[counter] = $(this).data('id');
                counter++;
            });

            var i = 0;
            var player = OO.Player.create('ooyalaplayer', embedcodes[i], {
                'wmode': 'opaque',
                onCreate: function (player) {
                    window.messageBus = player.mb;
                }

            });

            window.messageBus.subscribe(OO.EVENTS.PLAYED, 'LancerMain',
                function (eventName) {

                    i = i + 1;
                    player.setEmbedCode(embedcodes[i]);

                });

            window.messageBus.subscribe(OO.EVENTS.PLAYBACK_READY, 'LancerMain',
                function (eventName) {
                    player.play();
                });

            $('#p-pause').click(function (e) {
                e.preventDefault();
                i = i + 1;
                player.setEmbedCode(embedcodes[i]);

            });


            $('.playlist-videos-list a').click(function(e){
                e.preventDefault();
                console.log('click');
                i = parseInt($(this).data('order'));
                //console.log(i);
                player.setEmbedCode(embedcodes[i]);
            });

            $('.pl-video-bottom.available').on('click',function(){
                $('.pl-video-bottom').removeClass('available');
                $('.playlist-videos-list li:first').each(function(){
                    count_elements = parseInt($('.playlist-videos-list li:last a').data('order'));
                    max_margin = (count_elements-2)*(-100);
                    margin = parseInt($(this).css('margin-top'));
                    console.log(margin);
                    console.log(max_margin);
                    if(margin < max_margin) return;
                    margin = margin - 100;
                    $(this).animate({ 'marginTop': margin+'px'}, 300);
                });
                setTimeout(function() {
                    $('.pl-video-bottom').addClass('available');
                }, 350);

            })

            $('.pl-video-top.available').on('click',function(){
                $('.pl-video-top').removeClass('available');
                $('.playlist-videos-list li:first').each(function(){
                    margin = parseInt($(this).css('margin-top'));
                    if(margin >= 0) {
                        margin = 0;
                    }   else {
                        margin = margin + 100;
                    }
                    $(this).animate({ 'marginTop': margin+'px'}, 300);
                });
                setTimeout(function() {
                    $('.pl-video-top').addClass('available');
                }, 350);
            })



        });

    }


    $(function() {
        $( "#datepicker" ).datepicker();
    });

});


