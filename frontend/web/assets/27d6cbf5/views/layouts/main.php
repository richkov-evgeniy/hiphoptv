<?php

/**
 * Theme main layout.
 *
 * @var \yii\web\View $this View
 * @var string $content Content
 */

use vova07\themes\site\widgets\Alert;
use yii\widgets\Breadcrumbs;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\helpers\Url;
use yii\helpers\Html;

?>
<?php $this->beginPage(); ?>
    <!DOCTYPE html>
    <html lang="<?= Yii::$app->language ?>">
    <head>
        <?= $this->render('//layouts/head') ?>
        <?php
            echo HTML::csrfMetaTags();
        ?>
    </head>
    <body>
    <?php $this->beginBody(); ?>

        <div class="wrap">

            <div class="container-fluid">
                <?= Breadcrumbs::widget([
                    'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
                ]) ?>

                <div class="container">
                    <div class="row">
                       <!-- <div class="col-lg-12">-->
                            <?php
                            NavBar::begin([
                                'brandLabel' => '<img src="'.$this->assetManager->publish('@vova07/themes/site/images/logo.png')[1].'">',
                                'brandUrl' => Yii::$app->homeUrl,
                                'options' => [
                                    'class' => 'navbar-inverse',
                                ],
                            ]);
                            echo Nav::widget([
                                'options' => ['class' => 'navbar-nav navbar-right'],
                                'items' => [
                                    ['label' => 'Search', 'url' => [Url::to('/browse/search')], 'options'=> ['class'=>'glyphicon glyphicon-search']],
                                    ['label' => 'Browse', 'url' => [Url::to('/browse/browse')]],
                                    ['label' => 'Shows', 'url' => [Url::to('/browse/shows')]],
                                    Yii::$app->user->isGuest ?
                                        ['label' => 'Sign in >', 'url' => ['/users/guest/login'], 'linkOptions'=> ['class'=>'signin-signout'], 'options'=> ['class'=>'li-signin-signout']] :
                                        [
                                        'label' => Yii::t('vova07/themes/site', 'Settings'),
                                        'url' => '#',
                                        'template' => '<a href="{url}" class="dropdown-toggle" data-toggle="dropdown">{label} <i class="icon-angle-down"></i></a>',
                                        'visible' => !Yii::$app->user->isGuest,
                                        'items' => [
                                            [
                                                'label' => 'Playlists',
                                                'url' => ['/playlists/playlists/index/']
                                            ],
                                            [
                                                'label' => Yii::t('vova07/themes/site', 'Edit profile'),
                                                'url' => ['/users/user/update']
                                            ],
                                            [
                                                'label' => Yii::t('vova07/themes/site', 'Change email'),
                                                'url' => ['/users/user/email']
                                            ],
                                            [
                                                'label' => Yii::t('vova07/themes/site', 'Change password'),
                                                'url' => ['/users/user/password']
                                            ],
                                            [   'label' => 'Logout',
                                                'url' => ['/users/user/logout'],
                                                'linkOptions' => ['data-method' => 'post', 'class'=>'signin-signout'], 'options'=> ['class'=>'li-signin-signout']
                                            ],
                                        ]
                                    ],
                                ],
                            ]);
                            NavBar::end();
                            //'Logout (' . Yii::$app->user->identity->username . ')',
                            ?>
                            <!-- <?= $this->render('//layouts/top-menu') ?> -->
                        <!--</div>-->
                    </div>
                </div>
                    <?= $content ?>
                </div>

            </div>


            <footer class="footer">
                <div class="container-fluid">
                    <!--<p class="pull-left">&copy; My Company <?/*= date('Y') */?></p>
                    <p class="pull-right"><?/*= Yii::powered() */?></p>-->
                    <!--<div class="row">-->
                        <div class="container">
                            <div class="footer-menu">
                                <ul>
                                    <li><a href="/#">about us</a></li>
                                    <li><a href="/#">apps</a></li>
                                    <li><a href="/#">our team</a></li>
                                </ul>
                            </div>

                            <div class="social-icons">
                                <div class="sc-label">Connect with us!</div>
                                <div class="c-clearfix"></div>
                                <ul class="s-icons-list">
                                    <li><a href="/#"><img src="<?= $this->assetManager->publish('@vova07/themes/site/images/icon-instagram.png')[1] ?>"></a></li>
                                    <li><a href="/#"><img src="<?= $this->assetManager->publish('@vova07/themes/site/images/icon-twitter.png')[1] ?>"></a></li>
                                    <li><a href="/#"><img src="<?= $this->assetManager->publish('@vova07/themes/site/images/icon-googleplus.png')[1] ?>"></a></li>
                                    <li><a href="/#"><img src="<?= $this->assetManager->publish('@vova07/themes/site/images/icon-facebook.png')[1] ?>"></a></li>
                                    <li><a href="/#"><img src="<?= $this->assetManager->publish('@vova07/themes/site/images/icon-rss.png')[1] ?>"></a></li>
                                    <div class="c-clearfix"></div>
                                </ul>
                            </div>

                            <div class="c-clearfix"></div>

                            <p class="copyrights">&copy; 2014 HipHopTV. All Rights Reserved</p>

                        </div>
                    <!--</div>-->
                </div>
            </footer>
    </div>

    <div class="search-modal">
        <div class="browse-container container">
            <div class="row">
                <div class="col-lg-12">
                    <?php
                    echo Html::input('text', 'search', null, ['class'=>'input-search', 'placeholder'=>'Search for artists and videos']);
                    ?>
                    <a href="#" class="close-search-modal">X</a>
                </div>
            </div>
            <div class="search-results">
                <!--<div class="artists-results">
                    <div class="row">
                        <div class="section-heading col-lg-12">
                            <h2 class="h1-style"><a href="#" class="head-style">Artists Results</a></h2>
                            <div class="c-clearfix"></div>
                        </div>
                    </div>
                    <div class="artists-results-container row">
                    </div>
                    <div class="c-clearfix"></div>
                </div>-->

                <div class="video-results">
                    <div class="row">
                        <div class="section-heading col-lg-12">
                            <h2 class="h1-style"><a href="#" class="head-style">Video Results</a></h2>
                            <div class="c-clearfix"></div>
                        </div>
                    </div>
                    <div class="video-results-container row">
                    </div>
                    <div class="c-clearfix"></div>
                </div>
            </div>
        </div>
    </div>

    <?php $this->endBody(); ?>
    </body>
    </html>
<?php $this->endPage(); ?>