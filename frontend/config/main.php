<?php

return [
    'id' => 'app-frontend',
    'name' => 'HipHopTV',
    'basePath' => dirname(__DIR__),
    'defaultRoute' => 'site/default/index',
    'modules' => [
        'site' => [
            'class' => 'vova07\site\Module'
        ],
        'blogs' => [
            'controllerNamespace' => 'vova07\blogs\controllers\frontend'
        ],
        /*'vod' => [
            'class' => 'app\modules\vod\Module',
        ],*/
        /*'playlist' => [
            'class' => 'app\modules\playlist\Module',
        ],*/
        'browse' => [
            'class' => 'app\modules\browse\Module',
        ],
        'watch' => [
            'class' => 'app\modules\watch\Module',
        ],
        'playlists' => [
            'class' => 'app\modules\playlists\Module',
        ],
        'security' => [
            'class' => 'app\modules\security\Module',
        ],
        'info' => [
            'class' => 'app\modules\info\Module',
        ],
    ],
    'components' => [
        'request' => [
            'cookieValidationKey' => 'sdi8s#fnj98jwiqiw;qfh!fjgh0d8f',
            'baseUrl' => ''
        ],
        'urlManager' => [
            'rules' => [
                '' => 'site/default/index',
                '<_a:(about|contacts|captcha)>' => 'site/default/<_a>',
                'shows/watch/<id>'=>'browse/shows/watch',

                'video/watch/<id>' => 'watch/watch/watch',

                'label/watch/<id>' => 'browse/label/watch',

                'shows' => 'browse/shows',
                'browse' => 'browse/browse',
                'about-us' => 'info/default/aboutus',
                'our-team' => 'info/default/ourteam',
                'privacy-policy' => 'info/default/privacypolicy',
                'home-guest' => 'info/default/homeguest',
                'investors' => 'info/default/investors',
            ],
            //'suffix' => '/',

        ],
        'view' => [
            'theme' => 'vova07\themes\site\Theme'
        ],
        'errorHandler' => [
            'errorAction' => 'site/default/error'
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning']
                ]
            ]
        ],
        'authClientCollection' => [
            'class' => 'yii\authclient\Collection',
            'clients' => [
                /*'google' => [
                    'class' => 'yii\authclient\clients\GoogleOpenId'
                ],*/
                'facebook' => [
                    'class' => 'yii\authclient\clients\Facebook',
                    'clientId' => '1734625276763298',
                    'clientSecret' => '90131fb63612b48e77c809a8b95b8725',
                    'authUrl' => 'https://www.facebook.com/dialog/oauth?display=popup',
                    'scope'=> ['user_birthday', 'email']
                ],
            ],
        ]
    ],


    'params' => require(__DIR__ . '/params.php')
];
