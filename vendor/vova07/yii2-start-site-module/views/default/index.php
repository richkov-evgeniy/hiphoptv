<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;
/* @var $this yii\web\View */
$this->title = 'HIPHOPTV';
?>
<div class="container-fluid">
    <div class="main-block">
        <div class="container">
            <div class="player">
                <div class="video-content">
                    <?php
                        if(\Yii::$app->mobileDetect->isMobile() or \Yii::$app->mobileDetect->isTablet()) :
                    ?>
                        <video width="100%" height="auto" controls>
                            <source src="http://104.236.105.82:1935/live/LiveTV/playlist.m3u8" type="application/x-mpegURL">
                        </video>
                    <?php
                        else:
                    ?>
                        <div id="watch-live" class="video-content">
                            <script src='//player.ooyala.com/v3/eaab03347ec4d1d8fdcfecb243bb5e9'></script>
                            <div id='ooyalaplayer'></div>
                        </div>
                    <?php
                        endif;
                    ?>
                </div>
            </div>
        </div>
    </div>

    <div class="container-player-icons-bar">
        <div class="container">
            <div class="player-icons-bar">
                <div class="player-current-info">
                </div>
                <!--
                <ul>
                    <li><a href="/#"><img src="<?//= $this->assetManager->publish('@vova07/themes/site/images/icon-share.png')[1] ?>"></a></li>
                    <li><a href="/#"><img src="<?//= $this->assetManager->publish('@vova07/themes/site/images/icon-plus.png')[1] ?>"></a></li>
                    <li><a href="/#"><img src="<?//= $this->assetManager->publish('@vova07/themes/site/images/icon-i.png')[1] ?>"></a></li>
                    <div class="c-clearfix"></div>
                </ul>
                -->
            </div>
        </div>
    </div>

    <div class="schedule">
        <div class="container">
            <a href="/#" class="show-hide-schedule">
                <div class="sh-label">hide schedule</div>
                <div class="sh-arrow"><i class="Chevron-bottom"></i></div>
                <div class="c-clearfix"></div>
            </a>

            <div class="schedule-content">
                <?php if($schedule['body']['live_video']) : ?>
                <div class="current-video">

                    <?php

                       // print_r($schedule['body']['live_video']);exit;

                    ?>

                    <div class="video-info">
                        <img class="live-video-banner" src="<?= $schedule['body']['live_video']['image'] ?>">
                        <div class="live-video-info">
                            <div class="video-time">
                                <?= $schedule['body']['live_video']['start_time_pst'] ?> (PT) /
                                <?= $schedule['body']['live_video']['start_time_est'] ?> (ET)
                            </div>
                            <h2 class="video-title"><?= $schedule['body']['live_video']['title'] ?></h2>
                            <p class="video-description"><?= $schedule['body']['live_video']['text'] ?>
                            </p>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                </div>
                <?php endif; ?>

                <div class="video-set row">
                    <?php if(count($schedule['body']['schedule'])) : ?>
                        <?php foreach($schedule['body']['schedule'] as $event) : ?>
                            <div class="col-lg-4 col-md-6 col-sm-12 col-xs-12 video-event">
                                <div class="video-event-sub-wrapper">
                                    <div class="event-preview" style="background-image: url('<?= $event['image'] ?>')">
                                    </div>
                                    <div class="event-short-info">
                                        <div class="video-time">
                                            <?= $event['start_time_pst'] ?> (PT)
                                            <?= $event['start_time_est'] ?> (ET)
                                        </div>
                                        <h2 class="video-title"><?= $event['title'] ?></h2>
                                        <p class="video-description"><?= $event['text'] ?>
                                        </p>
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                            </div>
                        <?php endforeach; ?>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </div>


</div>