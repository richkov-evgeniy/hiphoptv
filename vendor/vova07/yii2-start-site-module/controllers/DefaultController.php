<?php

namespace vova07\site\controllers;

use vova07\site\components\Controller;
use vova07\site\models\ContactForm;
use vova07\site\Module;
use yii\captcha\CaptchaAction;
use yii\web\ErrorAction;
use yii\web\ViewAction;
use Yii;
use DateTimeZone;
use DateTime;

use app\modules\playlists\controllers;

use app\modules\playlists\models\Playlists;
use app\modules\browse\models\Videos;

use vova07\site\models\Events;
/**
 * Frontend main controller.
 */
class DefaultController extends Controller
{
    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => ErrorAction::className()
            ],
            'about' => [
                'class' => ViewAction::className(),
                'defaultView' => 'about'
            ],
            'captcha' => [
                'class' => CaptchaAction::className(),
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
                'backColor' => 0XF5F5F5,
                'height' => 34
            ]
        ];
    }

    /**
     * Site "Home" page.
     */
    public function actionIndex()
    {
        if(!Yii::$app->user->getId()) {
            $this->redirect('/home-guest');
        }

        $events = Events::find()->asArray()->orderBy('start_time')->all();


        $future_videos = [];
        $schedule = [];
        $live_video = FALSE;

        if($events) {
            $total_duration = 0;
            foreach($events as $event) {
                $total_duration += $event['duration'];
            }

            //date_default_timezone_set('UTC');
            date_default_timezone_set('EST5EDT');
            $time = time();
            $h = date('H', $time);
            $i = date('i', $time);
            $s = date('s', $time);

            $d = date('d', $time);
            $m = date('m', $time);
            $y = date('Y', $time);


            $day_begin_time = mktime(0, 0, 0, $m, $d, $y);

            $left_second_from_day = $h*60*60 + $i*60 + $s;

            $repeat_videos = [];

            $live_video_search_seconds = 0;
            if($left_second_from_day <  $total_duration) {
                foreach($events as $event) {
                    if(!$live_video) {
                        $live_video_search_seconds += $event['duration'];
                        if($live_video_search_seconds > $left_second_from_day) {
                            $reload = $event['duration'] - ($time - ($event['start_time'] + $day_begin_time));
                            $event['start_time'] = date("Y-m-d g:i a", $event['start_time'] + $day_begin_time);
                            $event['end_time'] =  date("Y-m-d g:i a", $event['end_time'] + $day_begin_time);
                            $live_video = $event;
                        }   else {
                            $event['start_time'] = date("Y-m-d g:i a", $event['start_time'] + $day_begin_time + $total_duration);
                            $event['end_time'] =  date("Y-m-d g:i a", $event['end_time'] + $day_begin_time + $total_duration);
                            $repeat_videos[] = $event;
                        }
                    }   else {
                        $event['start_time'] = date("Y-m-d g:i a", $event['start_time'] + $day_begin_time);
                        $event['end_time'] =  date("Y-m-d g:i a", $event['end_time'] + $day_begin_time);
                        $future_videos[] = $event;
                    }
                }
            }   else {
                $current_time_cycle = $this->getTimeForCycle($left_second_from_day, $total_duration);
                $left_second_from_day = $current_time_cycle['left_second_from_day'];
                foreach ($events as $event) {
                    if(!$live_video) {
                        $live_video_search_seconds += $event['duration'];
                        if($live_video_search_seconds > $left_second_from_day) {
                            $reload = $event['duration'] - ($time - ($event['start_time'] + $total_duration*$current_time_cycle['cycle'] + $day_begin_time));
                            $event['start_time'] = date("Y-m-d g:i a", $event['start_time'] + $total_duration*$current_time_cycle['cycle'] + $day_begin_time);
                            $event['end_time'] =  date("Y-m-d g:i a", $event['end_time'] + $total_duration*$current_time_cycle['cycle'] + $day_begin_time);
                            $live_video = $event;
                        }   else {
                            $event['start_time'] = date("Y-m-d g:i a", $event['start_time'] + $total_duration*$current_time_cycle['cycle'] + $day_begin_time + $total_duration);
                            $event['end_time'] =  date("Y-m-d g:i a", $event['end_time'] + $total_duration*$current_time_cycle['cycle'] + $day_begin_time + $total_duration);
                            $repeat_videos[] = $event;
                        }
                    }   else {
                        $event['start_time'] = date("Y-m-d g:i a", $event['start_time'] + $total_duration*$current_time_cycle['cycle'] + $day_begin_time);
                        $event['end_time'] =  date("Y-m-d g:i a", $event['end_time'] + $total_duration*$current_time_cycle['cycle'] + $day_begin_time);
                        $future_videos[] = $event;
                    }
                }
            }


            foreach($repeat_videos as $repeat_video) {
                $future_videos[] = $repeat_video;
            }

            $schedule['reload'] = abs($reload);
            $schedule['live_video'] = $live_video;
            $schedule['schedule'] = $future_videos;

            $schedule['embed_code'] = 'k3M2M4cjoh9VcP6GU-OrHrJ6O1XgRAGk';
            $schedule['preview_image_url'] = 'http://hiphoptv.com/images/banner-live.jpg';


            /**/
            $utc = new DateTimeZone('UTC');
            $pst = new DateTimeZone('PST8PDT');
            $est = new DateTimeZone('EST5EDT');
            /**/

            $dateTime = new DateTime ($schedule['live_video']['start_time']);
            $dateTime->setTimezone($est);
            $schedule['live_video']['start_time_est'] = $dateTime->format('g:i a');
            $dateTime = new DateTime ($schedule['live_video']['start_time']);
            $dateTime->setTimezone($pst);
            $schedule['live_video']['start_time_pst'] = $dateTime->format('g:i a');
            $schedule['live_video']['start_time'] = date('g:i a', strtotime($schedule['live_video']['start_time']));

            foreach($schedule['schedule'] as &$event) {
                $dateTime = new DateTime ($event['start_time']);
                $dateTime->setTimezone($est);
                $event['start_time_est'] = $dateTime->format('g:i a');
                $dateTime = new DateTime ($event['start_time']);
                $dateTime->setTimezone($pst);
                $event['start_time_pst'] = $dateTime->format('g:i a');
                $event['start_time'] = date('g:i a', strtotime($event['start_time']));
            }

            $response_data['status'] = true;
            $response_data['body'] = $schedule;
            $response_data['message'] = 'null';
        }   else {
            $response_data['status'] = false;
            $response_data['body'] = null;
            $response_data['message'] = 'Schedule not exist';
        }

        return $this->render('index',[
            'schedule' => $response_data
        ]);
    }

    public function getTimeForCycle($left_second_from_day, $total_duration, $cycle = 0) {
        if($left_second_from_day >  $total_duration) {
            $current_time_cycle = $left_second_from_day - $total_duration;
            $cycle++;
            return $this->getTimeForCycle($current_time_cycle, $total_duration, $cycle);
        }   else {
            return ['left_second_from_day' => $left_second_from_day, 'cycle' => $cycle];
        }
    }



    /**
     * Site "Contact" page.
     */
    public function actionContacts()
    {
        $model = new ContactForm;
        if ($model->load(Yii::$app->request->post()) && $model->contact(Yii::$app->params['adminEmail'])) {
            Yii::$app->session->setFlash(
                'success',
                Module::t('site', 'CONTACTS_FLASH_SUCCESS_MSG')
            );
            return $this->refresh();
        } else {
            return $this->render(
                'contacts',
                [
                    'model' => $model
                ]
            );
        }
    }


    public function actionShows()
    {
        return "videos";
    }
}
