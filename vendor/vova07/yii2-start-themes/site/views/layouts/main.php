<?php

/**
 * Theme main layout.
 *
 * @var \yii\web\View $this View
 * @var string $content Content
 */

use vova07\themes\site\widgets\Alert;
use yii\widgets\Breadcrumbs;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\helpers\Url;
use yii\helpers\Html;

?>
<?php $this->beginPage(); ?>
    <!DOCTYPE html>
    <html lang="<?= Yii::$app->language ?>">
    <head>
        <?= $this->render('//layouts/head') ?>
        <?php
        echo HTML::csrfMetaTags();
        ?>
        <link href="/css/style.css" rel="stylesheet">
    </head>
    <body>
    <?php $this->beginBody(); ?>


    <?= Breadcrumbs::widget([
        'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
    ]) ?>
    <header id="header">
        <div class="container">
            <div class="row">
                <!-- <div class="col-lg-12">-->
                <?php
                NavBar::begin([
                    'brandLabel' => '<img src="/images/logo.png"><span>BETA</span><div class="clearfix"></div>',
                    'brandUrl' => Yii::$app->homeUrl,
                    'options' => [
                        'class' => 'navbar-inverse main-menu-top',
                    ],
                ]);
                echo Nav::widget([
                    'options' => ['class' => 'navbar-nav navbar-right'],
                    'items' => [
                        !Yii::$app->user->isGuest ? ['label' => 'LIVE', 'url' => [Url::to('/')]]  : '',
                        !Yii::$app->user->isGuest ? ['label' => 'Search', 'url' => [Url::to('/browse/search')]] : '',
                        !Yii::$app->user->isGuest ? ['label' => 'Browse', 'url' => [Url::to('/browse/browse')]] : '',
                        !Yii::$app->user->isGuest ? ['label' => 'featured', 'url' => [Url::to(['/browse/shows/watch', 'id' => 'featured'])]] : '',
                        //['label' => 'investors', 'url' => "/investors"],
                        !Yii::$app->user->isGuest ? ['label' => 'Playlists', 'url' => ['/playlists/playlists/index/']] : '',

                        Yii::$app->user->isGuest ?
                            ['label' => 'Sign In', 'url' => ['/users/guest/login'], 'linkOptions' => ['class' => 'signin-signout'], 'options' => ['class' => 'li-signin-signout']] : '',
                        Yii::$app->user->isGuest ?
                           // ['label' => 'Sign Up', 'url' => '/signup', 'linkOptions' => ['data-toggle' => 'modal','data-target' => '#signup-popup']]:
                            ['label' => 'Sign Up', 'url' => '/signup']:
                            [
                                'label' => Yii::t('vova07/themes/site', 'Settings'),
                                'url' => '#',
                                'template' => '<a href="{url}" class="dropdown-toggle" data-toggle="dropdown">{label} <i class="icon-angle-down"></i></a>',
                                'visible' => !Yii::$app->user->isGuest,
                                'items' => [
                                    [
                                        'label' => 'Playlists',
                                        'url' => ['/playlists/playlists/index/']
                                    ],
                                    [
                                        'label' => Yii::t('vova07/themes/site', 'Edit profile'),
                                        'url' => ['/users/user/update']
                                    ],
                                    [
                                        'label' => Yii::t('vova07/themes/site', 'Change email'),
                                        'url' => ['/users/user/email']
                                    ],
                                    [
                                        'label' => Yii::t('vova07/themes/site', 'Change password'),
                                        'url' => ['/users/user/password']
                                    ],
                                    ['label' => 'Logout',
                                        'url' => ['/users/user/logout'],
                                        'linkOptions' => ['data-method' => 'post', 'class' => 'signin-signout'], 'options' => ['class' => 'li-signin-signout']
                                    ],
                                ]
                            ],
                    ],
                ]);
                NavBar::end();
                //'Logout (' . Yii::$app->user->identity->username . ')',
                ?>
                <!-- <?= $this->render('//layouts/top-menu') ?> -->
                <!--</div>-->
            </div>
        </div>
    </header>
    <?= $content ?>

    <footer id="footer">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <ul class="footer-menu">
                        <li><a href="/about-us">About us</a></li>
                        <li><a href="/#">Apps</a></li>
                        <li><a href="/privacy-policy">Privacy policy</a></li>
                        <li><a href="/investors">For investors</a></li>
                    </ul>
                    <ul class="social-links">
                        <li><a href="https://www.facebook.com/HipHopTVLive" class="fb"
                               title="Facebook" target="_blank"></a></li>
                        <li><a href="https://twitter.com/HipHopTVlive" class="tw" title="Twitter" target="_blank"></a>
                        </li>
                        <li><a href="https://instagram.com/hiphoptvlive/" class="insta" title="Instagram"
                               target="_blank"></a></li>
<!--                        <li><a href="http://www.pinterest.com/hiphoptvcom/" class="pin" title="Pinterest" target="_blank"></a></li>-->
                    </ul>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <p class="copyright">HipHopTV © 2015. All Rights Reserved</p>

                    <p class="develop">Development by <a href="http://absolutewebservices.com" target="_blank"
                                                         title="Absolute Web Services">Absolute Web Services</a></p>
                </div>
            </div>
        </div>
    </footer>

    <div class="search-modal">
        <div class="browse-container container">
            <div class="row">
                <div class="col-lg-12">
                    <?php
                    echo Html::input('text', 'search', null, ['class' => 'input-search', 'placeholder' => 'Search for artists and videos']);
                    ?>
                    <a href="#" class="close-search-modal">X</a>
                </div>
            </div>
            <div class="search-results">
                <!--<div class="artists-results">
                    <div class="row">
                        <div class="section-heading col-lg-12">
                            <h2 class="h1-style"><a href="#" class="head-style">Artists Results</a></h2>
                            <div class="c-clearfix"></div>
                        </div>
                    </div>
                    <div class="artists-results-container row">
                    </div>
                    <div class="c-clearfix"></div>
                </div>-->

                <div class="video-results">
                    <div class="row">
                        <div class="section-heading col-lg-12">
                            <h2 class="h1-style"><a href="#" class="head-style">Video Results</a></h2>

                            <div class="c-clearfix"></div>
                        </div>
                    </div>
                    <div class="video-results-container row">
                    </div>
                    <div class="c-clearfix"></div>
                </div>
            </div>
        </div>
    </div>

    <?php $this->endBody(); ?>

    <!-- Google Analytics -->
    <script>
        (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
            (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
            m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
        })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

        ga('create', 'UA-70235309-1', 'auto');
        ga('send', 'pageview');

    </script>
    <!-- End Google Analytics -->

    </body>


    <div id="fb-root"></div>
    <script>(function (d, s, id) {
            var js, fjs = d.getElementsByTagName(s)[0];
            if (d.getElementById(id)) return;
            js = d.createElement(s);
            js.id = id;
            js.src = "//connect.facebook.net/ru_RU/sdk.js#xfbml=1&appId=1734625276763298&version=v2.0";
            fjs.parentNode.insertBefore(js, fjs);
        }(document, 'script', 'facebook-jssdk'));</script>
    </html>
<?php $this->endPage(); ?>