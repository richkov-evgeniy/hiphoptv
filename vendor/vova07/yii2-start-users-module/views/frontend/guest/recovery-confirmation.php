<?php

/**
 * Recovery confirmation page view.
 *
 * @var \yii\web\View $this View
 * @var \yii\widgets\ActiveForm $form Form
 * @var \vova07\users\models\frontend\RecoveryConfirmationForm $model Model
 */

use vova07\users\Module;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

$this->title = Module::t('users', 'FRONTEND_RECOVERY_CONFIRMATION_TITLE');
$this->params['breadcrumbs'] = [
    $this->title
];
$this->params['contentId'] = 'error'; ?>

<div class="container-fluid signin-signout-container">
    <div class="container">
        <div class="row">
            <div class="col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-2 in-out-form signin">
                <?php $form = ActiveForm::begin(); ?>
                    <fieldset class="registration-form">
                        <?= $form->field($model, 'password')->passwordInput(['placeholder' => $model->getAttributeLabel('password')])->label(false) ?>
                        <?= $form->field($model, 'repassword')->passwordInput(['placeholder' => $model->getAttributeLabel('repassword')])->label(false) ?>
                        <?= $form->field($model, 'token', ['template' => "{input}\n{error}"])->hiddenInput() ?>
                        <?= Html::submitButton(
                            Module::t('users', 'FRONTEND_RECOVERY_CONFIRMATION_SUBMIT'),
                            [
                                'class' => 'btn btn-success pull-right'
                            ]
                        ) ?>
                    </fieldset>
                <?php ActiveForm::end(); ?>
            </div>
        </div>
    </div>
</div>