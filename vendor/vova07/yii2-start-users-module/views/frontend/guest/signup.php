<?php

/**
 * Signup page view.
 *
 * @var \yii\web\View $this View
 * @var \yii\widgets\ActiveForm $form Form
 * @var \vova07\users\models\frontend\User $user Model
 * @var \vova07\users\models\Profile $profile Profile
 */

use vova07\fileapi\Widget;
use vova07\users\Module;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\authclient\widgets\AuthChoice;

$this->title = Module::t('users', 'FRONTEND_SIGNUP_TITLE');
$this->params['breadcrumbs'] = [
    $this->title
]; ?>
<div class="container-fluid signin-signout-container">
    <div class="container">
        <div class="row">
            <div class="col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-2 in-out-form signup">
                <?php $form = ActiveForm::begin(); ?>
                    <fieldset class="registration-form">
                        <div class="row">
                            <div class="col-xs-6 ">
                                <?= $form->field($profile, 'name')->textInput(
                                    ['placeholder' => $profile->getAttributeLabel('name')]
                                )->label(false) ?>
                            </div>
                            <div class="col-xs-6 ">
                                <?= $form->field($profile, 'surname')->textInput(
                                    ['placeholder' => $profile->getAttributeLabel('surname')]
                                )->label(false) ?>
                            </div>
                        </div>

                        <?= $form->field($profile, 'date_of_birth')->textInput(
                            ['placeholder' => 'MM/DD/YYYY', 'id' => 'datepicker']
                        )->label(false) ?>

                        <?= $form->field($user, 'username')->textInput(
                            ['placeholder' => $user->getAttributeLabel('username')]
                        )->label(false)

                        ?>
                        <?= $form->field($user, 'email')->textInput(
                            ['placeholder' => $user->getAttributeLabel('email')]
                        )->label(false) ?>

                        <div class="pass-label">Create Password</div>

                        <?= $form->field($user, 'password')->passwordInput(
                            ['placeholder' => $user->getAttributeLabel('password')]
                        )->label(false) ?>
                        <div class="pass-label">Confirm Password</div>
                        <?= $form->field($user, 'repassword')->passwordInput(
                            ['placeholder' => $user->getAttributeLabel('repassword')]
                        )->label(false) ?>

                        <?= $form->field($profile, 'avatar_url')->widget(
                            Widget::className(),
                            [
                                'settings' => [
                                    'url' => ['fileapi-upload']
                                ],
                                'crop' => true,
                                'cropResizeWidth' => 100,
                                'cropResizeHeight' => 100
                            ]
                        )->label(false) ?>
                        <?= Html::submitButton(
                            Module::t('users', 'FRONTEND_SIGNUP_SUBMIT'),
                            [
                                'class' => 'btn btn-success btn-large pull-right'
                            ]
                        ) ?>
                        <?//= Html::a(Module::t('users', 'FRONTEND_SIGNUP_RESEND'), ['resend']); ?>
                    </fieldset>
                <?php ActiveForm::end(); ?>


                <?php $authAuthChoice = AuthChoice::begin([
                    'baseAuthUrl' => ['/security/auth/auth']
                ]); ?>
                <ul>
                    <?php foreach ($authAuthChoice->getClients() as $client): ?>
                        <li><?php $authAuthChoice->clientLink($client, 'CONNECT WITH FACEBOOK') ?></li>
                    <?php endforeach; ?>
                </ul>
                <?php AuthChoice::end(); ?>

            </div>
        </div>
    </div>
</div>