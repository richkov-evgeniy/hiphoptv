<?php

/**
 * Recovery password page view.
 *
 * @var \yii\web\View $this View
 * @var \yii\widgets\ActiveForm $form Form
 * @var \vova07\users\models\frontend\RecoveryForm $model Model
 */

use vova07\users\Module;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

$this->title = Module::t('users', 'FRONTEND_RECOVERY_TITLE');
$this->params['breadcrumbs'] = [
    $this->title
];
$this->params['contentId'] = 'error'; ?>
<div class="container-fluid signin-signout-container">
    <div class="container">
        <div class="row">
            <div class="col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-2 in-out-form signin">
                <?php $form = ActiveForm::begin(
                    [
                        'options' => [
                            'class' => 'center'
                        ]
                    ]
                ); ?>
                    <fieldset class="registration-form">
                        <?= $form->field($model, 'email')->textInput(['placeholder' => $model->getAttributeLabel('email')])->label(false) ?>
                        <?= Html::submitButton(
                            Module::t('users', 'FRONTEND_RECOVERY_SUBMIT'),
                            [
                                'class' => 'btn btn-success pull-right'
                            ]
                        ) ?>
                    </fieldset>
                <?php ActiveForm::end(); ?>
            </div>
        </div>
    </div>
</div>