<?php

/**
 * Sign In page view.
 *
 * @var \yii\web\View $this View
 * @var \yii\widgets\ActiveForm $form Form
 * @var \vova07\users\models\LoginForm $model Model
 */

use vova07\users\Module;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\authclient\widgets\AuthChoice;

$this->title = Module::t('users', 'FRONTEND_LOGIN_TITLE');
$this->params['breadcrumbs'] = [
    $this->title
]; ?>

<div class="container-fluid signin-signout-container">
    <div class="container">
        <div class="row">
            <div class="col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-2 in-out-form signin">
                <?php $form = ActiveForm::begin(); ?>
                <div class="signin-signout-name">SIGN IN</div>
                <fieldset class="registration-form">
                    <?= $form->field($model, 'username')->textInput(['placeholder' => $model->getAttributeLabel('username')])->label(false) ?>
                    <?= $form->field($model, 'password')->passwordInput(['placeholder' => $model->getAttributeLabel('password')])->label(false) ?>
                    <?= $form->field($model, 'rememberMe')->checkbox(['checked'=>true]) ?>
                    <?= Html::submitButton(Module::t('users', 'FRONTEND_LOGIN_SUBMIT'), ['class' => 'btn btn-primary']) ?>


                    <div class="forgot-pass-link">
                        <?= Module::t('users', 'FRONTEND_LOGIN_OR') ?>
                        &nbsp;
                        <?= Html::a(Module::t('users', 'FRONTEND_LOGIN_RECOVERY'), ['recovery']) ?>
                    </div>

                    <div class="not-registered-link">
                        Don't have an account?
			&nbsp;
                        <?= Html::a('Not registered yet?', ['/users/guest/signup']) ?>
                    </div>

                </fieldset>
                <?php ActiveForm::end(); ?>

                <?php $authAuthChoice = AuthChoice::begin([
                    'baseAuthUrl' => ['/security/auth/auth']
                ]); ?>
                <ul>
                    <?php foreach ($authAuthChoice->getClients() as $client): ?>
                        <li><?php $authAuthChoice->clientLink($client, 'CONNECT WITH FACEBOOK') ?></li>
                    <?php endforeach; ?>
                </ul>
                <?php AuthChoice::end(); ?>



                <!--<?//= Connect::widget([ 'baseAuthUrl' => ['/user/security/auth']]) ?>-->

            </div>
        </div>

    </div>
</div>
