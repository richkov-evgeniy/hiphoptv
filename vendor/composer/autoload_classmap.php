<?php

// autoload_classmap.php @generated by Composer

$vendorDir = dirname(dirname(__FILE__));
$baseDir = dirname($vendorDir);

return array(
    'LightOpenID' => $vendorDir . '/nodge/lightopenid/openid.php',
    'LightOpenIDProvider' => $vendorDir . '/nodge/lightopenid/provider/provider.php',
    'Mobile_Detect' => $vendorDir . '/mobiledetect/mobiledetectlib/Mobile_Detect.php',
);
