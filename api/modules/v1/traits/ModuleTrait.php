<?php

//namespace vova07\users\traits;
namespace api\modules\v1\traits;

use api\modules\v1\Module;
use Yii;

/**
 * Class ModuleTrait
 * @package vova07\users\traits
 * Implements `getModule` method, to receive current module instance.
 */
trait ModuleTrait
{
    /**
     * @var \vova07\users\Module|null Module instance
     */
    private $_module;

    /**
     * @return \vova07\users\Module|null Module instance
     */
    public function getModule()
    {
        if ($this->_module === null) {
            $module = Module::getInstance();
            if ($module instanceof Module) {
                $this->_module = $module;
            } else {
                $this->_module = Yii::$app->getModule('v1');
            }
        }
        return $this->_module;
    }
}
