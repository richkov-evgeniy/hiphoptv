<?php
namespace api\modules\v1\models;
use \yii\db\ActiveRecord;


class Labels extends ActiveRecord
{
    public function rules()
    {
        return [
            [['name', 'full_name', 'label_id', 'parent_id'], 'required']
        ];
    }


    public function fields()
    {
        $fields = parent::fields();

        unset($fields['url_alias']);

        return $fields;
    }

    public function extraFields()
    {

    }


}
