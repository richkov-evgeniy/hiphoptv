<?php
namespace api\modules\v1\models;
use \yii\db\ActiveRecord;


class Shows extends ActiveRecord
{
    /**
     * Define rules for validation
     */
    public function rules()
    {
        return [
            [['created_at', 'description', 'duration', 'embed_code', 'name', 'preview_image_url', 'status', 'update_at', 'player_id'], 'required']
        ];
    }

    public function fields()
    {
        $fields = parent::fields();

        unset($fields['asset_type'], $fields['url_alias'], $fields['main_photo'], $fields['main_text'], $fields['player_id']);

        return $fields;
    }




}
