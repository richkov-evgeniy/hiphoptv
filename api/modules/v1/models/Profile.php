<?php

namespace api\modules\v1\models;

use api\modules\v1\traits\ModuleTrait;
use yii\db\ActiveRecord;
use Yii;
use api\modules\v1\Module;

class Profile extends ActiveRecord
{
    use ModuleTrait;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%profiles}}';
    }


    public function fields()
    {
        $fields = parent::fields();

        unset($fields['avatar_url']);

        return $fields;
    }


    /**
     * @inheritdoc
     */
    public static function findByUserId($id)
    {
        return static::findOne(['user_id' => $id]);
    }

    /**
     * @return string User full name
     */
    public function getFullName()
    {
        return $this->name . ' ' . $this->surname;
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            // Name
            ['name', 'match', 'pattern' => '/^[a-zа-яё]+$/iu'],
            // Surname
            ['surname', 'match', 'pattern' => '/^[a-zа-яё]+(-[a-zа-яё]+)?$/iu'],
            [['surname', 'name'], 'required'],
            [['date_of_birth'], 'date', 'format' => 'MM/dd/yyyy']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'name' => Module::t('users', 'ATTR_NAME'),
            'surname' => Module::t('users', 'ATTR_SURNAME'),
            'date_of_birth' => 'Date of birth'
        ];
    }

    /**
     * @return Profile|null Profile user
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id'])->inverseOf('profile');
    }
}
