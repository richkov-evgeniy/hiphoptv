<?php
namespace api\modules\v1\models;
use \yii\db\ActiveRecord;


class Labels_videos extends ActiveRecord
{
    public function rules()
    {
        return [
            [['label_id', 'video_embed_code'], 'required']
        ];
    }


    public function fields()
    {
        $fields = parent::fields();
        return $fields;
    }

    public function extraFields()
    {

    }


    public function getVideos()
    {
        return $this->hasOne(Videos::className(), ['embed_code' => 'video_embed_code']);
    }

    public function getLabels()
    {
        return $this->hasOne(Labels::className(), ['label_id' => 'label_id']);
    }


}
