<?php
namespace api\modules\v1\models;
use \yii\db\ActiveRecord;


class Videos extends ActiveRecord
{
    /**
     * Define rules for validation
     */
    public function rules()
    {
        return [
            [['asset_type', 'created_at', 'description', 'duration', 'embed_code', 'name', 'preview_image_url', 'status', 'updated_at', 'player_id, url'], 'required']
        ];
    }

    public function fields()
    {
        $fields = parent::fields();

        unset($fields['asset_type'], $fields['player_id'], $fields['url_alias']);

        return $fields;
    }

}
