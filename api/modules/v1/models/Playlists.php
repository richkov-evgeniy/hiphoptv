<?php
namespace api\modules\v1\models;
use \yii\db\ActiveRecord;


class Playlists extends ActiveRecord
{
    /**
     * Define rules for validation
     */
    public function rules()
    {
        return [
            [['user_id', 'playlist_name'], 'required']
        ];
    }

    public function fields()
    {
        $fields = parent::fields();

        return $fields;
    }

}
