<?php
namespace api\modules\v1\models;
use \yii\db\ActiveRecord;
use api\modules\v1\models\Videos;


class Shows_videos extends ActiveRecord
{
    /**
     * Define rules for validation
     */
    public function rules()
    {
        return [
            [['show_embed_code', 'video_embed_code'], 'required']
        ];
    }

    public function fields()
    {
        $fields = parent::fields();

        return $fields;
    }


    public function extraFields()
    {
        return ['videos'];
    }


    public function getVideos()
    {
        return $this->hasOne(Videos::className(), ['embed_code' => 'video_embed_code'])->where(['status' => 'live']);
    }






}
