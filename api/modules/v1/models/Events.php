<?php
namespace api\modules\v1\models;
use \yii\db\ActiveRecord;


class Events extends ActiveRecord
{
    public function rules()
    {
        return [
            [['title', 'time', 'text'], 'required']
        ];
    }


    public function fields()
    {
        $fields = parent::fields();
        return $fields;
    }

    public function extraFields()
    {

    }

}
