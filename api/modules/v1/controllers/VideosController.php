<?php

namespace api\modules\v1\controllers;

use api\modules\v1\models\Videos;
use yii\rest\ActiveController;
use yii\web\Response;
use yii\filters\auth\HttpBasicAuth;
use yii\data\ActiveDataProvider;
use yii\db\Query;
use yii\filters\auth\QueryParamAuth;
use api\modules\v1\UniversalHttpBasicAuth;

class VideosController extends ActiveController
{
    public $modelClass = 'api\modules\v1\models\Videos';

    public function behaviors()
    {
        $behaviors = parent::behaviors();
        $behaviors['contentNegotiator']['formats']['text/html'] = Response::FORMAT_JSON;
        $behaviors['authenticator'] = [
            'class' =>  UniversalHttpBasicAuth::className(),
        ];
        return $behaviors;
    }

    public $serializer = [
        'class' => 'yii\rest\Serializer',
        'collectionEnvelope' => 'items',
    ];

    public function actionAll($limit = false, $page = false)
    {
        $query = new Query();
        return new ActiveDataProvider([
            'query' => $query->select('id, created_at, description, duration, embed_code, name, preview_image_url, status, updated_at, url')->from('{{%videos}}'),
            'pagination' => [
                'pageSize' => $limit,
                'page' => $page
            ],
        ]);
    }

    public function actionSearch($condition, $limit = false, $page = false)
    {
        $condition = str_replace("'", "", $condition);
        $query = new Query();
        return new ActiveDataProvider([
            'query' => $query->select('*')->from('{{%videos}}')->where($condition),
            'pagination' => [
                'pageSize' => $limit,
                'page' => $page
            ],
        ]);
    }

    public function actionGetresults($string, $page=false, $limit=false)
    {
        $string = str_replace("'", "", $string);
        $query = new Query();
        return new ActiveDataProvider([
            'query' => $query->select('*')->from('{{%videos}}')->where(['LIKE', 'name','%' . $string . '%', false]),
            'pagination' => [
                'pageSize' => $limit,
                'page' => $page
            ],
        ]);
    }
}

