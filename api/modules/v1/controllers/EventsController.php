<?php

namespace api\modules\v1\controllers;

use api\modules\v1\models\Events;
use yii\rest\ActiveController;
use yii\web\Response;
use yii\db\Query;
use yii\data\ActiveDataProvider;
use yii\filters\auth\HttpBasicAuth;
use api\modules\v1\SSHttpBasicAuth;
use yii\filters\auth\CompositeAuth;

use DateTimeZone;
use DateTime;

class EventsController extends ActiveController
{
    public $modelClass = 'api\modules\v1\models\Events';


    public $serializer = [
        'class' => 'yii\rest\Serializer',
        'collectionEnvelope' => 'items',
    ];


    public function behaviors()
    {
        $behaviors = parent::behaviors();
        $behaviors['contentNegotiator']['formats']['text/html'] = Response::FORMAT_JSON;

        /*$behaviors['authenticator'] = [
            'class' =>  SSHttpBasicAuth::className()
        ];*/

        /*$behaviors['authenticator'] = [
            'class' => CompositeAuth::className(),
            'authMethods' => [
                SSHttpBasicAuth::className(),
                HttpBasicAuth::className()
            ],
        ];*/

        return $behaviors;
    }


    public function actionAll($limit = false, $page = false)
    {
        $query = new Query();
        return new ActiveDataProvider([
            'query' => $query->select('id, title, time, text')->from('{{%events}}')->orderBy('time'),
            'pagination' => [
                'pageSize' => $limit,
                'page' => $page
            ],
        ]);
    }


    public function actionLive($limit = false, $page = false)
    {
        $events = Events::find()->asArray()->orderBy('start_time')->all();
        $future_videos = [];
        $schedule = [];
        $live_video = FALSE;

        if($events) {

            $total_duration = 0;
            foreach($events as $event) {
                $total_duration += $event['duration'];
            }
            date_default_timezone_set('EST5EDT');
            $time = time();
            $h = date('H', $time);
            $i = date('i', $time);
            $s = date('s', $time);

            $d = date('d', $time);
            $m = date('m', $time);
            $y = date('Y', $time);


            $day_begin_time = mktime(0, 0, 0, $d, $m, $y);

            $left_second_from_day = $h*60*60 + $i*60 + $s;

            $repeat_videos = [];

            $live_video_search_seconds = 0;
            if($left_second_from_day <  $total_duration) {
                foreach($events as $event) {
                    if(!$live_video) {
                        $live_video_search_seconds += $event['duration'];
                        if($live_video_search_seconds > $left_second_from_day) {
                            $reload = $event['duration'] - ($time - ($event['start_time'] + $day_begin_time));
                            $event['start_time'] = date("g:i a", $event['start_time'] + $day_begin_time);
                            $event['end_time'] =  date("g:i a", $event['end_time'] + $day_begin_time);
                            $live_video = $event;
                        }   else {
                            $event['start_time'] = date("g:i a", $event['start_time'] + $day_begin_time + $total_duration);
                            $event['end_time'] =  date("g:i a", $event['end_time'] + $day_begin_time + $total_duration);
                            $repeat_videos[] = $event;
                        }
                    }   else {
                        $event['start_time'] = date("g:i a", $event['start_time'] + $day_begin_time);
                        $event['end_time'] =  date("g:i a", $event['end_time'] + $day_begin_time);
                        $future_videos[] = $event;
                    }
                }
            }   else {
                $current_time_cycle = $this->getTimeForCycle($left_second_from_day, $total_duration);
                $left_second_from_day = $current_time_cycle['left_second_from_day'];
                foreach ($events as $event) {
                    if(!$live_video) {
                        $live_video_search_seconds += $event['duration'];
                        if($live_video_search_seconds > $left_second_from_day) {
                            $reload = $event['duration'] - ($time - ($event['start_time'] + $total_duration*$current_time_cycle['cycle'] + $day_begin_time));
                            $event['start_time'] = date("g:i a", $event['start_time'] + $total_duration*$current_time_cycle['cycle'] + $day_begin_time);
                            $event['end_time'] =  date("g:i a", $event['end_time'] + $total_duration*$current_time_cycle['cycle'] + $day_begin_time);
                            $live_video = $event;
                        }   else {
                            $event['start_time'] = date("g:i a", $event['start_time'] + $total_duration*$current_time_cycle['cycle'] + $day_begin_time + $total_duration);
                            $event['end_time'] =  date("g:i a", $event['end_time'] + $total_duration*$current_time_cycle['cycle'] + $day_begin_time + $total_duration);
                            $repeat_videos[] = $event;
                        }
                    }   else {
                        $event['start_time'] = date("g:i a", $event['start_time'] + $total_duration*$current_time_cycle['cycle'] + $day_begin_time);
                        $event['end_time'] =  date("g:i a", $event['end_time'] + $total_duration*$current_time_cycle['cycle'] + $day_begin_time);
                        $future_videos[] = $event;
                    }
                }
            }


            foreach($repeat_videos as $repeat_video) {
                $future_videos[] = $repeat_video;
            }

            $schedule['reload'] = abs($reload);

            $schedule['live_video'] = $live_video;
            $schedule['schedule'] = $future_videos;

            /**/

            $utc = new DateTimeZone('UTC');
            /*$pst = new DateTimeZone('PST8PDT');
            $est = new DateTimeZone('EST5EDT');*/

            //live video start time
            $dateTime = new DateTime ($schedule['live_video']['start_time']);
            $dateTime->setTimezone($utc);
            $schedule['live_video']['start_time'] = $dateTime->format('g:i a');
            //live video end time
            $dateTime = new DateTime ($schedule['live_video']['end_time']);
            $dateTime->setTimezone($utc);
            $schedule['live_video']['end_time'] = $dateTime->format('g:i a');

            //schedule
            foreach($schedule['schedule'] as &$event) {
                $dateTime = new DateTime ($event['start_time']);
                $dateTime->setTimezone($utc);
                $event['start_time'] = $dateTime->format('g:i a');

                $dateTime = new DateTime ($event['end_time']);
                $dateTime->setTimezone($utc);
                $event['end_time'] = $dateTime->format('g:i a');
            }

            /**/


            $schedule['embed_code'] = 'ZpcHYydjqzpEvI1kcvFp5j0kqfV8njGS';
            $schedule['preview_image_url'] = 'http://hiphoptv.com/images/banner-live.jpg';
            $schedule['stream_ip'] = '104.236.105.82:1935';

            $response_data['status'] = true;
            $response_data['body'] = $schedule;
            $response_data['message'] = 'null';

            return $response_data;
        }   else {
            $response_data['status'] = false;
            $response_data['body'] = null;
            $response_data['message'] = 'Schedule not exist';
            return $response_data;
        }

    }


    public function getTimeForCycle($left_second_from_day, $total_duration, $cycle = 0) {
        if($left_second_from_day >  $total_duration) {
            $current_time_cycle = $left_second_from_day - $total_duration;
            $cycle++;
            return $this->getTimeForCycle($current_time_cycle, $total_duration, $cycle);
        }   else {
            return ['left_second_from_day' => $left_second_from_day, 'cycle' => $cycle];
        }
    }


}


