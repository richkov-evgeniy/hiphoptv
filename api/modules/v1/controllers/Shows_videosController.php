<?php

namespace api\modules\v1\controllers;

use api\modules\v1\models\Shows;
use api\modules\v1\models\Videos;
use api\modules\v1\models\Shows_videos;
use yii\rest\ActiveController;
use yii\web\Response;
use yii\filters\auth\HttpBasicAuth;
use yii\db\Query;
use api\modules\v1\UniversalHttpBasicAuth;


class Shows_videosController extends ActiveController
{
    public $modelClass = 'api\modules\v1\models\Shows_videos';


    public function behaviors()
    {
        $behaviors = parent::behaviors();

        $behaviors['authenticator'] = [
            'class' =>  UniversalHttpBasicAuth::className(),
        ];

        return $behaviors;
    }


    public function actionIndex()
    {
        return new ActiveDataProvider([
            'query' => Post::find(),
        ]);
    }


    public function actionList($id)
    {
        $model_shows_videos = new Shows_videos();
        $videos = $model_shows_videos->find()
            ->joinWith('videos')
            ->where(['show_embed_code' => $id])
            ->orderBy('id')
            ->all();

        return $videos;

    }


}


