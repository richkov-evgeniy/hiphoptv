<?php

namespace api\modules\v1\controllers;

use api\modules\v1\models\Labels_videos;
use yii\rest\ActiveController;
use yii\web\Response;
use api\modules\v1\models\Labels;
use yii\data\ActiveDataProvider;
use yii\db\Query;
use yii\filters\auth\QueryParamAuth;
use yii\filters\auth\HttpBasicAuth;
use api\modules\v1\UniversalHttpBasicAuth;


class LabelsController extends ActiveController
{

    public $modelClass = 'api\modules\v1\models\Labels';

    public function behaviors()
    {
        $behaviors = parent::behaviors();
        $behaviors['contentNegotiator']['formats']['text/html'] = Response::FORMAT_JSON;

        $behaviors['authenticator'] = [
            'class' =>  UniversalHttpBasicAuth::className(),
        ];

        return $behaviors;
    }


    public $serializer = [
        'class' => 'yii\rest\Serializer',
        'collectionEnvelope' => 'items',
    ];


    public function actionAll($limit = false, $page = false)
    {
        $query = new Query();
        return new ActiveDataProvider([
            'query' => $query->select('id, name, label_id, parent_id')->from('{{%labels}}'),
            'pagination' => [
                'pageSize' => $limit,
                'page' => $page
            ],
        ]);

    }


    public function actionSearch($condition)
    {
        $condition = str_replace("'", "", $condition);

        $query = new Query();

        return new ActiveDataProvider([
            'query' => $query->select('id, name, full_name, label_id, parent_id')->from('{{%labels}}')->where($condition)
        ]);

    }


    public function actionVideos($id, $limit = false, $page = false)
    {
        $query = new Query();

        return new ActiveDataProvider([
            'query' => $query->select('*')->from('{{%labels_videos}}')->where(['label_id' => $id])->leftJoin('{{%videos}}','{{%videos}}.embed_code = {{%labels_videos}}.video_embed_code'),
            'pagination' => [
                'pageSize' => $limit,
                'page' => $page
            ]
        ]);
    }


    public function actionGroupvideos($limit = 6)
    {
        $label_videos = array();

        $query = new Query();

        $labels_resource = $query->select('id, name, label_id')->from('{{%labels}}')->where(['name' => ['Music Videos', 'Original Content', 'Documentaries', 'Live Performances']])->all();

        $labels_id = array();

        if($labels_resource) {

            foreach ($labels_resource as $item) {
                $labels_id[] = $item['label_id'];
                $label_videos[$item['label_id']]['name'] = $item['name'];
            }

            $model_labels_videos = new Labels_videos();

            $resource_videos = $model_labels_videos->find()
                ->joinWith('videos')
                ->where(['label_id' => $labels_id])
                ->orderBy('id')
                ->all();

            foreach ($resource_videos as $item) {
                if (isset($label_videos[$item->label_id])) {
                    if (count($label_videos[$item->label_id]) > $limit) {
                        continue;
                    } else {
                        $label_videos[$item->label_id]['videos'][] = $item->videos;
                    }
                } else {
                    $label_videos[$item->label_id]['videos'][] = $item->videos;
                }
            }

            return $label_videos;
        }   else {
            $response_data['status'] = false;
            $response_data['body'] = null;
            $response_data['message'] = "Categories not found";
            return ($response_data);
        }
    }


    public function actionBrowse($limit = false, $page = false)
    {
        $query = new Query();

        return new ActiveDataProvider([
            'query' => $query->select('id, name, full_name, label_id, parent_id')->from('{{%labels}}')->where(['name' => ['Music Videos', 'Original Content', 'Documentaries', 'Live Performances']]),
            'pagination' => [
                'pageSize' => $limit,
                'page' => $page
            ],
        ]);

    }



}


