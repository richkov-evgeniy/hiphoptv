<?php

namespace api\modules\v1\controllers;

use yii\web\Controller;
use yii\rest\ActiveController;
use yii\web\Response;

class VersionController extends ActiveController
{
    public $modelClass = 'api\modules\v1\models\Shows_videos';

    public function actions()
    {
        $actions = array();
        return $actions;
    }

    public function behaviors()
    {
        $behaviors = parent::behaviors();
        $behaviors['contentNegotiator']['formats']['text/html'] = Response::FORMAT_JSON;
        return $behaviors;
    }

    public $serializer = [
        'class' => 'yii\rest\Serializer',
        'collectionEnvelope' => 'items',
    ];

    public function actionFeatured()
    {
        $response_data = array();
        $response_data['status'] = true;
        $response_data['body'] = false;
        $response_data['message'] = 'featured';
        return ($response_data);
    }
}


