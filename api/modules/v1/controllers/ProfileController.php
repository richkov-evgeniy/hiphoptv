<?php

namespace api\modules\v1\controllers;

use api\modules\v1\models\Videos;
use yii\rest\ActiveController;
use yii\web\Response;
use yii\filters\auth\HttpBasicAuth;
use yii\data\ActiveDataProvider;
use yii\db\Query;
use yii\filters\auth\QueryParamAuth;
use api\modules\v1\models\Users;
class ProfileController extends ActiveController
{
    public $modelClass = 'api\modules\v1\models\Profile';

    public function behaviors()
    {
        $behaviors = parent::behaviors();
        $behaviors['contentNegotiator']['formats']['text/html'] = Response::FORMAT_JSON;

        $behaviors['authenticator'] = [
            'class' =>  HttpBasicAuth::className(),
        ];

        return $behaviors;
    }

    public $serializer = [
        'class' => 'yii\rest\Serializer',
        //'collectionEnvelope' => 'items',
    ];


    public function actions()
    {
        $actions = parent::actions();

        return $actions;
    }




}

