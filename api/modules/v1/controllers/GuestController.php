<?php

namespace api\modules\v1\controllers;

use api\modules\v1\models\Users;
use yii\rest\ActiveController;
use yii\web\Response;
//use yii\filters\auth\HttpBasicAuth;
//use yii\data\ActiveDataProvider;
//use yii\db\Query;
//use yii\filters\auth\QueryParamAuth;
//use yii\web\Controller;
//use api\modules\v1\models\User;
//use api\modules\v1\models\Profile;
use Yii;
use api\modules\v1\models\User;
use api\modules\v1\models\Profile;
use api\modules\v1\Module;
use yii\filters\auth\HttpBasicAuth;
use api\modules\v1\SSHttpBasicAuth;

class GuestController extends ActiveController
{
    public $modelClass = 'api\modules\v1\models\User';

    public function behaviors()
    {
        $behaviors = parent::behaviors();
        $behaviors['contentNegotiator']['formats']['text/html'] = Response::FORMAT_JSON;

        $behaviors['authenticator'] = [
            'class' =>  SSHttpBasicAuth::className()
        ];

        return $behaviors;
    }

    public $serializer = [
        'class' => 'yii\rest\Serializer',
        'collectionEnvelope' => 'items',
    ];


    public function actions()
    {
        $actions = parent::actions();

        unset($actions['update'], $actions['delete'], $actions['view'], $actions['create'], $actions['index'], $actions['options']);

        return $actions;
    }


    public function actionSignup()
    {

        $user = new User(['scenario' => 'signup']);
        $profile = new Profile();

        $response_data = array();

        if(User::findByEmail(Yii::$app->request->post('email'))) {
            $response_data['status'] = false;
            $response_data['body'] = null;
            $response_data['message'] = "Your email already exists. Please try logging in or use another email";
            return ($response_data);
        }

        if ($user->load(Yii::$app->request->post()) && $profile->load(Yii::$app->request->post())) {
            if ($user->validate() && $profile->validate()) {
                $user->populateRelation('profile', $profile);
                if ($user->save(false)) {
                    if ($this->module->requireEmailConfirmation === true) {
                        $response_data['status'] = true;
                        $response_data['body'] = null;
                        $response_data['message'] = "User sign up success, need confirm by e-mail";
                        return ($response_data);
                    } else {
                        $response_data['status'] = true;
                        $response_data['body']['access_token'] = $user->access_token;
                        $response_data['message'] = null;
                        return ($response_data);
                    }
                } else {
                    $response_data['status'] = false;
                    $response_data['body'] = null;
                    $response_data['message'] = "User sign up failed. Please try again";
                    return ($response_data);
                }
            }   else {
                $response_data['status'] = false;
                $response_data['body'] = null;

                $response_data['message'] = '';
                if($user->hasErrors()) {
                    $errors_attr = $user->getErrors();
                    foreach($errors_attr as $error_attr) {
                        foreach($error_attr as $error) {
                            $response_data['message'] .= $error . ' ';
                        }
                    }
                }
                if($profile->hasErrors()) {
                    $errors_attr = $profile->getErrors();
                    foreach($errors_attr as $error_attr) {
                        foreach($error_attr as $error) {
                            $response_data['message'] .= $error . ' ';
                        }
                    }
                }

                return ($response_data);
            }
        }   else {
            $response_data['status'] = false;
            $response_data['body'] = null;
            $response_data['message'] = "User sign up failed. Please try again. Required parameters are missing.";
            return ($response_data);
        }

    }


    public function actionSignin()
    {
        $response_data = array();
        $post = Yii::$app->request->post();
        $email = $post['email'];
        $password = $post['password'];

        $user = User::findByEmail($email);
        if($user) {
            $validate_password = $user->validatePassword($password);
            if ($validate_password) {
                $response_data['status'] = true;
                $response_data['body']['access_token'] = $user->access_token;
                $response_data['body']['id'] = $user->id;
                $response_data['message'] = null;
                return ($response_data);
            } else {
                $response_data['status'] = false;
                $response_data['body'] = null;
                $response_data['message'] = "Not correct password or email";
                return ($response_data);
            }
        }   else {
            $response_data['status'] = false;
            $response_data['body'] = null;
            $response_data['message'] = "Not correct password or email";
            return ($response_data);
        }
    }


    public function actionSigninfb()
    {
        $post = Yii::$app->request->post();

        $service = $post['User']['service'];
        $service_id = $post['User']['service_id'];

        $user = User::findIdentifyBySocialId($service, $service_id);

        if($user) {
            $response_data['status'] = true;
            $response_data['body']['access_token'] = $user->access_token;
            $response_data['message'] = 'success signin';
            return ($response_data);
        }   else {
            $user = User::findByEmail($post['User']['email']);
            if($user) {
                $user->service = $post['User']['service'];
                $user->service_id = $post['User']['service_id'];
                $user->save(false);

                $response_data['status'] = true;
                $response_data['body']['access_token'] = $user->access_token;
                $response_data['message'] = 'success signin';
                return ($response_data);
            }   else {
                $user = new User(['scenario' => 'fb']);
                $profile = new Profile();

                $response_data = array();

                if ($user->load(Yii::$app->request->post()) && $profile->load(Yii::$app->request->post())) {
                    if ($user->validate() && $profile->validate()) {
                        $user->populateRelation('profile', $profile);

                        if ($user->save(false)) {
                            $response_data['status'] = true;
                            $response_data['body']['access_token'] = $user->access_token;
                            $response_data['message'] = 'success signup';
                            return ($response_data);
                        }   else {
                            $response_data['status'] = false;
                            $response_data['body'] = null;
                            $response_data['message'] = "User sign up failed. Please try again";
                            return ($response_data);
                        }
                    }   else {
                        $response_data['status'] = false;
                        $response_data['body'] = null;

                        $response_data['message'] = '';
                        if($user->hasErrors()) {
                            $errors_attr = $user->getErrors();
                            foreach($errors_attr as $error_attr) {
                                foreach($error_attr as $error) {
                                    $response_data['message'] .= $error . ' ';
                                }
                            }
                        }
                        if($profile->hasErrors()) {
                            $errors_attr = $profile->getErrors();
                            foreach($errors_attr as $error_attr) {
                                foreach($error_attr as $error) {
                                    $response_data['message'] .= $error . ' ';
                                }
                            }
                        }

                        return ($response_data);
                    }
                }   else {
                    $response_data['status'] = false;
                    $response_data['body'] = null;
                    $response_data['message'] = "User sign up failed. Please try again. Required parameters are missing.";
                    return ($response_data);
                }
            }
        }
    }


    public function actionSignupfb()
    {
        $user = new User(['scenario' => 'fb']);
        $profile = new Profile();

        $response_data = array();

        if ($user->load(Yii::$app->request->post()) && $profile->load(Yii::$app->request->post())) {
            if ($user->validate() && $profile->validate()) {
                $user->populateRelation('profile', $profile);


                if ($user->save(false)) {
                    $response_data['status'] = true;
                    $response_data['body']['access_token'] = $user->access_token;
                    $response_data['message'] = null;
                    return ($response_data);
                } else {
                    $response_data['status'] = false;
                    $response_data['body'] = null;
                    $response_data['message'] = "User sign up failed. Please try again";
                    return ($response_data);
                }
            }   else {
                $response_data['status'] = false;
                $response_data['body'] = null;
                $response_data['message'] = "User sign up failed. Please try again. Not validate";
                return ($response_data);
            }
        }   else {
            $response_data['status'] = false;
            $response_data['body'] = null;
            $response_data['message'] = "User sign up failed. Please try again. Not validate";
            return ($response_data);
        }
    }


    public function actionGetUniversalToken()
    {
        //$access_token = Yii::$app->security->generatePasswordHash('awshiphoptv2015');
        $access_token = '$2y$13$KFsvxHYARdOqenUq9Fis3e9SVp5uryaPNW778ANzeuAbdxUsYjqwi';
        $response_data['access_token'] = $access_token;
        return ($response_data);
    }


}

