<?php

namespace api\modules\v1\controllers;

use api\modules\v1\models\Playlists;
use api\modules\v1\models\Videos;
use yii\rest\ActiveController;
use yii\web\Response;
use yii\filters\auth\HttpBasicAuth;
use yii\data\ActiveDataProvider;
use yii\db\Query;
use yii\filters\auth\QueryParamAuth;
use yii\filters\ContentNegotiator;

class PlaylistsController extends ActiveController
{
    public $modelClass = 'api\modules\v1\models\Playlists';

    public function behaviors()
    {
        $behaviors = parent::behaviors();
        $behaviors['contentNegotiator'] = [
            'class' => ContentNegotiator::className(),
            'formats' => [
                'application/json' => Response::FORMAT_JSON
            ]

        ];
        $behaviors['authenticator'] = [
            'class' =>  HttpBasicAuth::className(),
        ];
        return $behaviors;
    }

    public $serializer = [
        'class' => 'yii\rest\Serializer',
        'collectionEnvelope' => 'items',
    ];


    public function actions()
    {
        $actions = parent::actions();
        unset($actions['create']);
        return $actions;
    }


    public function actionAll() {

        $model_playlists = new Playlists();

        $playlists = $model_playlists->find()
            ->where(['user_id' => \Yii::$app->user->getId()])
            ->asArray()
            ->all();

        if($playlists) {

            foreach($playlists as &$playlist) {
                $playlist['url'] = 'http://hiphoptv.com/playlists/playlists/watch?playlist_id=' . $playlist['id'];
                $playlist['preview'] = 'http://hiphoptv.com/images/music_block_2.png';
            }

            $response_data['status'] = true;
            $response_data['body'] = $playlists;
            $response_data['message'] = null;
            return ($response_data);
        }   else {
            $response_data['status'] = false;
            $response_data['body'] = null;
            $response_data['message'] = 'Playlists not exists';
            return ($response_data);
        }

    }


    public function actionCreate() {

        $data = \Yii::$app->request->getBodyParams();

        $model_playlists = new Playlists();

        $model_playlists->user_id = \Yii::$app->user->getId();

        $model_playlists->playlist_name = $data['playlist_name'];

        if($model_playlists->save()){
            $response_data['status'] = true;
            $response_data['body'] = $model_playlists;
            $response_data['message'] = null;
            return ($response_data);
        }   else {
            $response_data['status'] = false;
            $response_data['body'] = null;
            $response_data['message'] = 'Something going wrong, try again.';
            return ($response_data);
        }

    }


    public function actionAddvideo()
    {
        $data = \Yii::$app->request->getBodyParams();

        $model_playlists = new Playlists();
        $playlist = $model_playlists->find()
            ->andWhere(['id' => $data['playlist_id']])
            ->andWhere(['user_id' => \Yii::$app->user->getId()])
            ->one();

        if($playlist) {
            $old_videos = json_decode($playlist->videos, true);

            if (count($old_videos)) {
                if (in_array($data['embed_code'], $old_videos)) {
                    $response_data['status'] = false;
                    $response_data['body'] = $playlist;
                    $response_data['message'] = 'This video already added in playlist';
                    return ($response_data);
                }
            }

            $old_videos[] = $data['embed_code'];
            $playlist->videos = json_encode($old_videos);
            if($playlist->save()) {
                $response_data['status'] = true;
                $response_data['body'] = $playlist;
                $response_data['message'] = "Videos successfully added";
                return ($response_data);
            }   else {
                $response_data['status'] = false;
                $response_data['body'] = null;
                $response_data['message'] = 'Something going wrong, try again.';
                return ($response_data);
            }
        }   else {
            $response_data['status'] = false;
            $response_data['body'] = null;
            $response_data['message'] = 'Playlist not found';
            return ($response_data);
        }

    }


    public function actionGetplaylist($playlist_id)
    {

        $model_playlists = new Playlists();
        $playlist = $model_playlists->find()
            ->andWhere(['id' => $playlist_id ])
            ->andWhere(['user_id' => \Yii::$app->user->getId()])
            ->one();

        if($playlist) {
            $playlist_videos = json_decode($playlist->videos, true);

            $model_videos = new Videos();
            $videos = $model_videos->find()
                ->select('id, description, duration, embed_code, name, preview_image_url, url')
                ->where(['embed_code' => $playlist_videos])
                ->asArray()
                ->all();

            if($videos) {
                $videos_sorted = array();

                foreach ($videos as $video) {
                    $order_number = array_search($video['embed_code'], $playlist_videos);
                    $videos_sorted[$order_number] = $video;
                }

                ksort($videos_sorted);

                $videos_result = [];
                foreach($videos_sorted as $video) {
                    $videos_result[$video['id']] = $video;
                }

               /*
                print "---";
                print "<pre>";
                print_r($videos_sorted);
                exit;*/

                $response_data['status'] = true;
                $response_data['body']['playlist_name'] = $playlist->playlist_name;
                $response_data['body']['id'] = $playlist->id;
                $response_data['body']['url'] = 'http://hiphoptv.com/playlists/playlists/watch?playlist_id=' . $playlist->id;
                $response_data['body']['preview'] = 'http://hiphoptv.com/images/music_block_2.png';
                $response_data['body']['videos'] = (object)$videos_result;
                $response_data['message'] = null;

                return $response_data;
            }   else {
                $response_data['status'] = true;
                $response_data['body']['playlist_name'] = $playlist->playlist_name;
                $response_data['body']['id'] = $playlist->id;
                $response_data['body']['url'] = 'http://hiphoptv.com' . '/playlists/playlists/watch?playlist_id=' . $playlist->id;
                $response_data['body']['preview'] = 'http://hiphoptv.com/images/music_block_2.png';
                $response_data['body']['videos'] = null;
                $response_data['message'] = 'Not found videos for playlist';

                return $response_data;
            }


        }   else {
            $response_data['status'] = false;
            $response_data['body'] = null;
            $response_data['message'] = 'Playlist not found';
            return ($response_data);
        }
    }


    public function actionUpdateplaylist() {

        $data = \Yii::$app->request->getBodyParams();

        if(isset($data['playlist_id'])) {

            $model_playlists = new Playlists();

            $playlist = $model_playlists->find()
                ->andWhere(['id' => $data['playlist_id']])
                ->andWhere(['user_id' => \Yii::$app->user->getId()])
                ->one();

            if($playlist) {

                if (isset($data['videos_sort'])) {

                    $data_videos_sort = json_decode($data['videos_sort'], true);

                    $videos_sort = $data_videos_sort;
                    $videos = array();
                    foreach ($videos_sort as $video) {
                        $videos[] = $video;
                    }

                    $playlist->videos = json_encode($videos);
                } else {
                    $videos = array();
                    $playlist->videos = json_encode($videos);
                }

                if(isset($data['playlist_name'])) {
                    $playlist->playlist_name = $data['playlist_name'];
                }

                if($playlist->save()) {
                    $response_data['status'] = true;
                    $response_data['body'] = $playlist;
                    $response_data['message'] = null;
                    return $response_data;
                }   else {
                    $response_data['status'] = false;
                    $response_data['body'] = null;
                    $response_data['message'] = 'Something going wrong, try again.';
                    return ($response_data);
                }

            }   else {
                $response_data['status'] = false;
                $response_data['body'] = null;
                $response_data['message'] = 'Playlist not found';

                return $response_data;
            }

        }   else {
            $response_data['status'] = false;
            $response_data['body'] = null;
            $response_data['message'] = 'Missing requirement argument playlist_id';

            return $response_data;
        }
    }


    public function actionDeletevideo() {

        $data = \Yii::$app->request->getBodyParams();

        if(isset($data['playlist_id'])) {

            $model_playlists = new Playlists();

            $playlist = $model_playlists->find()
                ->andWhere(['id' => $data['playlist_id']])
                ->andWhere(['user_id' => \Yii::$app->user->getId()])
                ->one();

            if($playlist) {

                if (isset($data['embed_code'])) {

                    $playlist_videos = json_decode($playlist->videos, true);

                    if(($key = array_search($data['embed_code'], $playlist_videos)) !== false) {
                        unset($playlist_videos[$key]);
                    }

                    $playlist->videos = json_encode($playlist_videos);
                }


                if($playlist->save()) {
                    $response_data['status'] = true;
                    $response_data['body'] = $playlist;
                    $response_data['message'] = null;
                    return $response_data;
                }   else {
                    $response_data['status'] = false;
                    $response_data['body'] = null;
                    $response_data['message'] = 'Something going wrong, try again.';
                    return ($response_data);
                }

            }   else {
                $response_data['status'] = false;
                $response_data['body'] = null;
                $response_data['message'] = 'Playlist not found';

                return $response_data;
            }

        }   else {
            $response_data['status'] = false;
            $response_data['body'] = null;
            $response_data['message'] = 'Missing requirement argument playlist_id';

            return $response_data;
        }
    }


}
