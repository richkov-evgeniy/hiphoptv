<?php

namespace api\modules\v1\controllers;

use api\modules\v1\models\Shows;
use api\modules\v1\models\Videos;
use api\modules\v1\models\Shows_videos;
use yii\rest\ActiveController;
use yii\web\Response;
use yii\filters\auth\HttpBasicAuth;
use yii\db\Query;
use yii\data\ActiveDataProvider;
use yii\data\ArrayDataProvider;
use yii\filters\auth\QueryParamAuth;
use api\modules\v1\UniversalHttpBasicAuth;


class ShowsController extends ActiveController
{
    public $modelClass = 'api\modules\v1\models\Shows_videos';

    public function behaviors()
    {
        $behaviors = parent::behaviors();
        $behaviors['contentNegotiator']['formats']['text/html'] = Response::FORMAT_JSON;
        $behaviors['authenticator'] = [
            'class' =>  UniversalHttpBasicAuth::className(),
        ];
        return $behaviors;
    }

    public $serializer = [
        'class' => 'yii\rest\Serializer',
        'collectionEnvelope' => 'items',
    ];


    public function actionAll($limit = false, $page = false)
    {
        $query = new Query();
        return new ActiveDataProvider([
            'query' => $query->select('id, embed_code, created_at, description, duration, name, preview_image_url, status, updated_at')->from('{{%shows}}'),
            'pagination' => [
                'pageSize' => $limit,
                'page' => $page
            ],
        ]);
    }


    public function actionSearch($condition, $limit = false, $page = false)
    {
        $condition = str_replace("'", "", $condition);
        $query = new Query();
        return new ActiveDataProvider([
            'query' => $query->select('*')->from('{{%shows}}')->where($condition),
            'pagination' => [
                'pageSize' => $limit,
                'page' => $page
            ],
        ]);
    }


    public function actionVideos($embed_code, $limit = false, $page = false)
    {
        $query = new Query();
        return new ActiveDataProvider([
            'query' => $query->select('*')->from('{{%shows_videos}}')->where(['show_embed_code' => $embed_code])->leftJoin('{{%videos}}','{{%videos}}.embed_code = {{%shows_videos}}.video_embed_code'),
            'pagination' => [
                'pageSize' => $limit,
                'page' => $page
            ]
        ]);
    }

    public function actionInfo($embed_code)
    {
        $model_shows = new Shows();
        $show_info = $model_shows->find()
            ->where(['embed_code' => $embed_code])
            ->one();

        if($show_info) {
            $response_data['status'] = true;
            $response_data['body'] = $show_info;
            $response_data['message'] = '';
            return ($response_data);
        }   else {
            $response_data['status'] = false;
            $response_data['body'] = '';
            $response_data['message'] = 'show not exist';
            return ($response_data);
        }
    }
}


