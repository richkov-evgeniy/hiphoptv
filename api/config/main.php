<?php
use Yii;
$params = array_merge(
    require(__DIR__ . '/../../common/config/params.php'),
    require(__DIR__ . '/../../common/config/params-local.php'),
    require(__DIR__ . '/params.php'),
    require(__DIR__ . '/params-local.php')
);

return [
    'id' => 'app-api',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log'],
    'modules' => [
        'v1' => [
           // 'basePath' => '@app/modules/v1',
            'class' => 'api\modules\v1\Module'
        ],

    ],
    'components' => [
        /*'request' => [
            'baseUrl' => '/api'
        ],*/
        'user' => [
            'identityClass' => 'api\modules\v1\models\Users',
            'enableAutoLogin' => false,
            'enableSession' => false,

        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'urlManager' => [
            'enablePrettyUrl' => true,
            //'enableStrictParsing' => true,
            'showScriptName' => false,
            //'suffix' => '/',
            'rules' =>
                [
                    [
                        'class' => 'yii\rest\UrlRule',
                        'controller' => 'v1/events',
                    ],

                    [
                        'class' => 'yii\rest\UrlRule',
                        'controller' => 'v1/playlists',
                        'extraPatterns' => [
                            'POST create' => 'create',
                            'PUT addvideo' => 'addvideo',
                            'GET getplaylist' => 'getplaylist',
                            'PUT updateplaylist' => 'updategetplaylist',
                        ]
                    ],

                    [
                        'class' => 'yii\rest\UrlRule',
                        'controller' => 'v1/videos',
                        'extraPatterns' => [
                            'GET search/{condition}' => 'search',
                        ]
                    ],

                    [
                        'class' => 'yii\rest\UrlRule',
                        'controller' => 'v1/labels',
                        'extraPatterns' => [
                            'GET search/{condition}' => 'search',
                        ]
                    ],

                    [
                        'class' => 'yii\rest\UrlRule',
                        'controller' => 'v1/shows',
                        'extraPatterns' => [
                            'GET search/{condition}' => 'search',
                            'GET videos/{embed_code}' => 'videos'
                        ]
                    ],

                    [
                        'class' => 'yii\rest\UrlRule',
                        'controller' => 'v1/shows_videos',
                        'extraPatterns' => [
                            'GET list/{id}' => 'list'
                        ]
                    ],

                    [
                        'class' => 'yii\rest\UrlRule',
                        'controller' => 'v1/users',
                    ],

                    [
                        'class' => 'yii\rest\UrlRule',
                        'controller' => 'v1/guest',
                        'extraPatterns' => [
                            'POST signup' => 'signup',
                            'POST signin' => 'signin'
                        ]
                    ],

                    [
                        'class' => 'yii\rest\UrlRule',
                        'controller' => 'v1/version',
                        'extraPatterns' => [
                            'GET featured' => 'featured',
                        ]
                    ],


                ],
        ],
        'request' => [
            'parsers' => [
                'application/json' => 'yii\web\JsonParser',
            ]
        ],
        'response' => [
            'class' => 'yii\web\Response',
            'on beforeSend' => function ($event) {
                $response = $event->sender;
                if(isset($response->data['status'])) {
                    if ($response->data['status'] == true) {
                        $response->data = [
                            //'success' => $response->isSuccessful,
                            'data' => $response->data,
                        ];
                        $response->statusCode = 200;
                    } else {
                        $response->data = [
                            //'success' => $response->isSuccessful,
                            'data' => $response->data,
                        ];
                        $response->statusCode = 200;
                    }
                }
            },
        ],
    ],
    'params' => $params,
];



