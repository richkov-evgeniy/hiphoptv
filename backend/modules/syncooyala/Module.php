<?php

namespace app\modules\syncooyala;

class Module extends \yii\base\Module
{
    public $controllerNamespace = 'app\modules\syncooyala\controllers';

    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
