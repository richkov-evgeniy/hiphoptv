<?php

namespace app\modules\syncooyala\controllers;

use yii\web\Controller;
use app\modules\syncooyala\models\Labels_videos;
use app\modules\syncooyala\models\OoyalaApi;
use app\modules\syncooyala\models\Labels;

class Labels_videosController extends Controller
{
    public $defaultAction = 'index';

    public $ooyala;

    public function actionIndex()
    {

    }


    public function actionUp()
    {
        $this->ooyala = $this->ooyalaInit();

        $model_labels = new Labels();
        $labels = $model_labels->find()
            ->orderBy('id')
            ->all();

        foreach ($labels as $label) {
            $label_assets = $this->ooyala->get('/v2/labels/'. $label->label_id .'/assets');

            if(count($label_assets->items)) {
                foreach ($label_assets->items as $label_asset) {
                    $model_labels_videos = new Labels_videos();
                    $model_labels_videos->label_id = $label->label_id;
                    $model_labels_videos->video_embed_code = $label_asset->embed_code;
                    $model_labels_videos->save();
                }
            }
        }

        $status = 'success';

        return $this->render('index',
            [
                'status' => $status
            ]
        );

    }


    public function ooyalaInit() {
        $ooyala = new OoyalaApi('pyNzkyOp8Z5TRaIuevSYK36nA_xq.n6z6U', 'kOuVERF2R9Lpw9nXywjDFcDzmF02QTA-srRHYIKa');
        return $ooyala;
    }


}
