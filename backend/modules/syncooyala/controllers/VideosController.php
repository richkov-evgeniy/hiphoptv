<?php

namespace app\modules\syncooyala\controllers;

use yii\web\Controller;
use app\modules\syncooyala\models\Videos;
use app\modules\syncooyala\models\OoyalaApi;

class VideosController extends Controller
{
    public $defaultAction = 'index';

    public $ooyala;

    public function actionIndex()
    {

    }


    public function actionUp()
    {
        $this->ooyala = $this->ooyalaInit();
        $shows = $this->ooyala->get('/v2/assets/', array('where' => "asset_type='video'"));
        foreach ($shows->items as $show) {
            $model = new Videos();
            $model->asset_type = $show->asset_type;
            $model->created_at = $show->created_at;
            $model->description = $show->description;
            $model->duration = $show->duration;
            $model->embed_code = $show->embed_code;
            $model->name = $show->name;
            $model->preview_image_url = $show->preview_image_url;
            $model->status = $show->status;
            $model->updated_at = $show->updated_at;
            $model->player_id = $show->player_id;
            $model->save();
        }

        $status = 'success';

        return $this->render('index',
            [
                'status' => $status
            ]
        );

    }





    public function ooyalaInit() {
        $ooyala = new OoyalaApi('pyNzkyOp8Z5TRaIuevSYK36nA_xq.n6z6U', 'kOuVERF2R9Lpw9nXywjDFcDzmF02QTA-srRHYIKa');
        return $ooyala;
    }


}
