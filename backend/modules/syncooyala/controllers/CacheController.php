<?php

namespace app\modules\syncooyala\controllers;

use app\modules\syncooyala\models\Labels;
use app\modules\syncooyala\models\Videos;
use app\modules\syncooyala\models\Shows;
use app\modules\syncooyala\models\Labels_videos;
use app\modules\syncooyala\models\Shows_videos;
use app\modules\syncooyala\models\OoyalaApi;

use yii\web\Controller;

class CacheController extends Controller
{

    public $ooyala;

    public function actionUp()
    {
        $this->ooyala = $this->ooyalaInit();


        //get video
        $today = date("Y-m-d H:i:s", time()-360000000);
        $today_array = explode(' ', $today);
        $current_time = $today_array[0] . 'T' . $today_array[1] . 'Z';

        $updated_videos = $this->ooyala->get('/v2/assets/', array('where' => "asset_type='video'+AND+updated_at>='$current_time'", 'limit'=>'500'));


        $lv_videos_updated = array();
        $lv_videos_deleted = array();
        $lv_videos_created = array();

        if(count($updated_videos->items)) {

            foreach ($updated_videos->items as $video) {

                $model_video = new Videos();
                $resource_video = $model_video->find()
                    ->where(['embed_code' => $video->embed_code])
                    ->one();
                if ($resource_video) {
                    $resource_video->asset_type = $video->asset_type;
                    $resource_video->created_at = $video->created_at;
                    $resource_video->description = $video->description;
                    $resource_video->duration = $video->duration;
                    $resource_video->embed_code = $video->embed_code;
                    $resource_video->name = $video->name;
                    $resource_video->preview_image_url = $video->preview_image_url;
                    $resource_video->status = $video->status;
                    $resource_video->updated_at = $video->updated_at;
                    $resource_video->player_id = $video->player_id;

                    $alias_name = trim(preg_replace('| +|', ' ', $video->name));
                    $resource_video->url_alias = str_replace(' ', '-', $alias_name);

                    $resource_video->url = 'http://hiphoptv.com/video/watch/' . $resource_video->url_alias;

                    $resource_video->save();
                    $lv_videos_updated[] = $video->embed_code;
                } else {
                    $model_video->asset_type = $video->asset_type;
                    $model_video->created_at = $video->created_at;
                    $model_video->description = $video->description;
                    $model_video->duration = $video->duration;
                    $model_video->embed_code = $video->embed_code;
                    $model_video->name = $video->name;
                    $model_video->preview_image_url = $video->preview_image_url;
                    $model_video->status = $video->status;
                    $model_video->updated_at = $video->updated_at;
                    $model_video->player_id = $video->player_id;

                    $alias_name = trim(preg_replace('| +|', ' ', $video->name));
                    $model_video->url_alias = str_replace(' ', '-', $alias_name);

                    $model_video->url = 'http://hiphoptv.com/video/watch/' . $model_video->url_alias;

                    $model_video->save();
                    $lv_videos_created[] = $video->embed_code;
                }
            }
        }


        $model_video = new Videos();

        $all_videos_ooyala = $this->ooyala->get('/v2/assets/', array('where' => "asset_type='video'", 'limit'=>'500'));
        $db_videos = $model_video->find()
            ->asArray()
            ->all();


        if(count($all_videos_ooyala->items)) {
            foreach ($all_videos_ooyala->items as $ooyala_video) {
                $all_videos[] = $ooyala_video->embed_code;
            }
            if($db_videos) {
                foreach ($db_videos as $db_video) {
                    if (!in_array($db_video['embed_code'], $all_videos)) {
                        $model_video = new Videos();
                        $delete_video = $model_video->find()
                            ->where(['embed_code' => $db_video['embed_code']])
                            ->one();
                        $delete_video->delete();
                        $lv_videos_deleted[] = $db_video['embed_code'];
                    }
                }
            }
        }


        //get labels
        $labels = $this->ooyala->get('/v2/labels');
        $update_labels = array();

        if(count($labels->items))
        foreach ($labels->items as $label) {

            $model_label = new Labels();

            $resource_label = $model_label->find()
                ->Where(['label_id' => $label->id])
                ->one();

            if($resource_label) {
                $resource_label->name = $label->name;
                $resource_label->full_name = $label->full_name;
                $resource_label->parent_id = $label->parent_id;
                $resource_label->save();
            }   else {
                $model_label->label_id = $label->id;
                $model_label->name = $label->name;
                $model_label->full_name = $label->full_name;
                $model_label->parent_id = $label->parent_id;
                $model_label->save();
            }

            $update_labels[] = $label->id;
        }

        $model_label = new Labels();
        $db_labels = $model_label->find()
            ->select(['label_id'])
            ->asArray()
            ->all();

        if($db_labels) {
            foreach ($db_labels as $db_label) {
                if (!in_array($db_label['label_id'], $update_labels)) {
                    $model_label = new Labels();
                    $label = $model_label->find()
                        ->where(['label_id' => $db_label['label_id']])
                        ->one();
                    $label->delete();
                }
            }
        }


        //get labels videos

        //updated labels videos

        if(count($lv_videos_updated)) {
            foreach ($lv_videos_updated as $video_updated) {

                $model_labels_videos = new Labels_videos();

                $video_labels = $this->ooyala->get('/v2/assets/' . $video_updated . '/labels');

                $resource_labels_video = $model_labels_videos->find()
                    ->where(['video_embed_code' => $video_updated])
                    ->asArray()
                    ->all();

                $video_labels_array = array();

                foreach ($video_labels->items as $item) {
                    $video_labels_array[] = $item->id;

                    $model_labels_videos = new Labels_videos();

                    $resource_lv = $model_labels_videos->find()
                        ->andWhere(['video_embed_code' => $video_updated])
                        ->andWhere(['label_id' => $item->id])
                        ->one();
                    if (!$resource_lv) {
                        $model_labels_videos->label_id = $item->id;
                        $model_labels_videos->video_embed_code = $video_updated;
                        $model_labels_videos->save();
                    }
                }

                foreach ($resource_labels_video as $item) {
                    if (!in_array($item['label_id'], $video_labels_array)) {

                        $model_labels_videos = new Labels_videos();

                        $label_video = $model_labels_videos->find()
                            ->andWhere(['video_embed_code' => $video_updated])
                            ->andWhere(['label_id' => $item['label_id']])
                            ->one();
                        $label_video->delete();
                    }
                }
            }
        }


        //created labels videos
        if(count($lv_videos_created)) {
            foreach ($lv_videos_created as $video_created) {
                $video_labels = $this->ooyala->get('/v2/assets/' . $video_created . '/labels');

                if(count($video_labels->items)) {
                    foreach ($video_labels->items as $item) {

                        $model_labels_videos = new Labels_videos();

                        $model_labels_videos->video_embed_code = $video_created;
                        $model_labels_videos->label_id = $item->id;
                        $model_labels_videos->save();
                    }
                }
            }
        }


        //deleted labels videos
        if(count($lv_videos_deleted)) {
            foreach ($lv_videos_deleted as $video_deleted) {
                Labels_videos::deleteAll('video_embed_code = :code', [':code' => $video_deleted]);
            }
        }



        //get shows
        //updated show
        $updated_shows = $this->ooyala->get('/v2/assets/', array('where' => "asset_type='channel'+AND+updated_at>='$current_time'"));



        $sv_videos_updated = array();
        $sv_videos_deleted = array();
        $sv_videos_created = array();

        if(count($updated_shows->items)) {

            foreach ($updated_shows->items as $show) {

                $model_show = new Shows();
                $resource_show = $model_show->find()
                    ->where(['embed_code' => $show->embed_code])
                    ->one();
                if ($resource_show) {
                    $resource_show->asset_type = $show->asset_type;
                    $resource_show->created_at = $show->created_at;
                    $resource_show->description = $show->description;
                    $resource_show->duration = $show->duration;
                    $resource_show->embed_code = $show->embed_code;
                    $resource_show->name = $show->name;
                    $resource_show->preview_image_url = $show->preview_image_url;
                    $resource_show->status = $show->status;
                    $resource_show->updated_at = $show->updated_at;
                    $resource_show->player_id = $show->player_id;

                    $alias_name = trim(preg_replace('| +|', ' ', $show->name));
                    $resource_show->url_alias = str_replace(' ', '-', $alias_name);

                    $resource_show->save();
                    $sv_videos_updated[] = $show->embed_code;
                } else {
                    $model_show->asset_type = $show->asset_type;
                    $model_show->created_at = $show->created_at;
                    $model_show->description = $show->description;
                    $model_show->duration = $show->duration;
                    $model_show->embed_code = $show->embed_code;
                    $model_show->name = $show->name;
                    $model_show->preview_image_url = $show->preview_image_url;
                    $model_show->status = $show->status;
                    $model_show->updated_at = $show->updated_at;
                    $model_show->player_id = $show->player_id;

                    $alias_name = trim(preg_replace('| +|', ' ', $show->name));
                    $model_show->url_alias = str_replace(' ', '-', $alias_name);

                    $model_show->save();
                    $sv_videos_created[] = $show->embed_code;
                }
            }
        }

        //deleted_show
        $model_show = new Shows();

        $all_show_ooyala = $this->ooyala->get('/v2/assets/', array('where' => "asset_type='channel'"));
        $db_shows = $model_show->find()
            ->asArray()
            ->all();


        if(count($all_show_ooyala->items)) {
            foreach ($all_show_ooyala->items as $ooyala_show) {
                $all_shows[] = $ooyala_show->embed_code;
            }
            if($db_shows) {
                foreach ($db_shows as $db_show) {
                    if (!in_array($db_show['embed_code'], $all_shows)) {
                        $model_show = new Shows();
                        $delete_show = $model_show->find()
                            ->where(['embed_code' => $db_show['embed_code']])
                            ->one();
                        $delete_show->delete();
                        $sv_videos_deleted[] = $db_show['embed_code'];
                    }
                }
            }
        }


        ////////////////
        //updated labels videos

        if(count($sv_videos_updated)) {
            foreach ($sv_videos_updated as $show_updated) {

                $model_shows_videos = new Shows_videos();

                $show_lineup = $this->ooyala->get('/v2/assets/'. $show_updated .'/lineup');

                /*print "<pre>";
                print_r($show_lineup);exit();*/


                $resource_shows_video = $model_shows_videos->find()
                    ->where(['show_embed_code' => $show_updated])
                    ->asArray()
                    ->all();

                $show_videos_array = array();

                foreach ($show_lineup as $item) {
                    $show_videos_array[] = $item;

                    $model_shows_videos = new Shows_videos();

                    $resource_sv = $model_shows_videos->find()
                        ->andWhere(['video_embed_code' => $item])
                        ->andWhere(['show_embed_code' => $show_updated])
                        ->one();



                    if (!$resource_sv) {
                        $model_shows_videos->video_embed_code = $item;
                        $model_shows_videos->show_embed_code = $show_updated;
                        $model_shows_videos->save();
                    }
                }

               /* print "<pre>";
                print_r($resource_shows_video);
                print_r($show_videos_array);
                exit();*/

                foreach ($resource_shows_video as $item) {
                    if (!in_array($item['video_embed_code'], $show_videos_array)) {

                        $model_shows_videos = new Shows_videos();

                        $show_video = $model_shows_videos->find()
                            ->andWhere(['video_embed_code' => $item['video_embed_code']])
                            ->andWhere(['show_embed_code' => $show_updated])
                            ->one();
                        $show_video->delete();
                    }
                }
            }
        }


        /*print "<pre>";
        print_r($sv_videos_updated);exit();*/

        //created labels videos
        if(count($sv_videos_created)) {
            foreach ($sv_videos_created as $show_created) {

                $show_lineup = $this->ooyala->get('/v2/assets/'. $show_created .'/lineup');

                if(count($show_lineup)) {
                    foreach ($show_lineup as $item) {

                        $model_shows_videos = new Shows_videos();

                        $model_shows_videos->video_embed_code = $item;
                        $model_shows_videos->show_embed_code = $show_created;
                        $model_shows_videos->save();
                    }
                }
            }
        }


        //deleted labels videos
        if(count($sv_videos_deleted)) {
            foreach ($sv_videos_deleted as $show_deleted) {
                Shows_videos::deleteAll('show_embed_code = :code', [':code' => $show_deleted]);
            }
        }
    }



    public function ooyalaInit() {
        $ooyala = new OoyalaApi('pyNzkyOp8Z5TRaIuevSYK36nA_xq.n6z6U', 'kOuVERF2R9Lpw9nXywjDFcDzmF02QTA-srRHYIKa');
        return $ooyala;
    }


}
