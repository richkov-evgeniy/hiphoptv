<?php

namespace app\modules\syncooyala\controllers;

use yii\web\Controller;
use app\modules\syncooyala\models\Shows_videos;
use app\modules\syncooyala\models\OoyalaApi;
use app\modules\syncooyala\models\Shows;

class Shows_videosController extends Controller
{
    public $defaultAction = 'index';

    public $ooyala;

    public function actionIndex()
    {

    }


    public function actionUp()
    {
        $this->ooyala = $this->ooyalaInit();

        $model_shows = new Shows();
        $shows = $model_shows->find()
            ->orderBy('id')
            ->all();
     /*   print "<pre>";
        print_r($shows);
        print "</pre>";
        exit();*/

        foreach ($shows as $show) {

            $show_lineup = $this->ooyala->get('/v2/assets/'. $show->embed_code .'/lineup');

            foreach ($show_lineup as $video_embed_code) {
                $model_shows_videos = new Shows_videos();
                $model_shows_videos->show_embed_code = $show->embed_code;
                $model_shows_videos->video_embed_code = $video_embed_code;
                $model_shows_videos->save();
            }

        }

        $status = 'success';

        return $this->render('index',
            [
                'status' => $status
            ]
        );

    }





    public function ooyalaInit() {
        $ooyala = new OoyalaApi('pyNzkyOp8Z5TRaIuevSYK36nA_xq.n6z6U', 'kOuVERF2R9Lpw9nXywjDFcDzmF02QTA-srRHYIKa');
        return $ooyala;
    }


}
