<?php

namespace app\modules\syncooyala\controllers;

use yii\web\Controller;
use app\modules\syncooyala\models\Labels;
use app\modules\syncooyala\models\OoyalaApi;

class LabelsController extends Controller
{
    public $defaultAction = 'index';

    public $ooyala;

    public function actionIndex()
    {

    }


    public function actionUp()
    {
        $this->ooyala = $this->ooyalaInit();

        $labels = $this->ooyala->get('/v2/labels');

        foreach ($labels->items as $label) {
            $model = new Labels();
            $model->name = $label->name;
            $model->full_name = $label->full_name;
            $model->label_id = $label->id;
            $model->parent_id = $label->parent_id;
            $model->save();
        }

        $status = 'success';

        return $this->render('index',
            [
                'status' => $status
            ]
        );

    }


    public function ooyalaInit() {
        $ooyala = new OoyalaApi('pyNzkyOp8Z5TRaIuevSYK36nA_xq.n6z6U', 'kOuVERF2R9Lpw9nXywjDFcDzmF02QTA-srRHYIKa');
        return $ooyala;
    }


}
