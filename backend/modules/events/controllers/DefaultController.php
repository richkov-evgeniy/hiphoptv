<?php

namespace app\modules\events\controllers;

use app\modules\events\models\Events;
use yii\base\Request;
use yii\web\Controller;
use Yii;
use yii\helpers\Url;


class DefaultController extends Controller
{
    public function actionIndex()
    {
        $model_events = new Events();
        $events = $model_events->find()
            ->orderBy('start_time')
            ->asArray()
            ->all();

        return $this->render('index', [
            'events' => $events
        ]);
    }


    public function actionCreate()
    {
        $events = new Events();

        if (Yii::$app->request->getMethod() == 'POST') {

            $events->title = Yii::$app->request->getBodyParam('title');
            $events->text = Yii::$app->request->getBodyParam('text');

            $events->start_time = Yii::$app->request->getBodyParam('s-hh') * 60 * 60 + Yii::$app->request->getBodyParam('s-mm') * 60 + Yii::$app->request->getBodyParam('s-ss');
            $events->end_time = Yii::$app->request->getBodyParam('e-hh') * 60 * 60 + Yii::$app->request->getBodyParam('e-mm') * 60 + Yii::$app->request->getBodyParam('e-ss');
            if(Yii::$app->request->getBodyParam('e-hh')== 23 && Yii::$app->request->getBodyParam('e-mm') == 59 && Yii::$app->request->getBodyParam('e-ss') == 59) {
                $events->end_time = 24 * 60 * 60;
            }
            $events->duration = $events->end_time - $events->start_time;

            if ($events->save()) {
                $this->redirect(['/events/default/index']);
            }
        }

        return $this->render('create', [
            'events' => $events
        ]);
    }


    public function actionDelete($id)
    {
        $event = Events::findOne($id);
        $event->delete();
        $this->redirect(Url::to(['/events/default/index']));
    }


}
