<?php
    use yii\helpers\Url;
?>

<div class="events-default-index row">
    <a class=" col-lg-12 create-event-link" href="<?= Url::to(['/events/default/create']) ?>">+ Create event</a>

    <?php if(count($events)) : ?>
        <div class="col-lg-12">

            <ul class="events-list-header">
                <li class="row">
                    <div class="col-lg-12">
                        <div class="row">
                            <div class="col-lg-3"><div class="events-header-value">Title</div></div>
                            <div class="col-lg-1"><div class="events-header-value">Start time</div></div>
                            <div class="col-lg-1"><div class="events-header-value">End time</div></div>
                            <div class="col-lg-1"><div class="events-header-value">Duration</div></div>
                            <div class="col-lg-6"><div class="events-header-value">Text</div></div>
                        </div>
                    </div>
                </li>
            </ul>

            <ul class="events-list" id="sortable">
                <?php foreach($events as $event) : ?>
                    <li class="row event-available ui-state-default" id="event-id" data-id="<?= $event['id'] ?>">
                        <div class='event-item col-lg-12'>
                            <div class="event-item-container">
                                <div class="row">
                                    <div class="col-lg-3 b-event-link">
                                        <?= $event['title'] ?>
                                    </div>
                                    <div class="col-lg-1 b-event-time">
                                        <?= gmdate('H:i:s',$event['start_time']) ?>
                                    </div>

                                    <div class="col-lg-1 b-event-time">
                                        <?= gmdate('H:i:s',$event['end_time']) ?>
                                    </div>

                                    <div class="col-lg-1 b-event-duration">
                                        <?= gmdate('H:i:s',$event['duration']) ?>
                                    </div>

                                    <div class="col-lg-5 b-event-text">
                                        <?= $event['text'] ?>
                                    </div>
                                    <div class="col-lg-1 b-event-delete">
                                        <a href="<?= Url::to(['/events/default/delete', 'id' => $event['id']]); ?>">X</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </li>
                <?php endforeach; ?>
            </ul>
        </div>

        <div class="c-clearfix"></div>
    <?php else : ?>
        <div class="simple-text col-lg-12">
            Your events list is empty
        </div>
    <?php endif; ?>


</div>
