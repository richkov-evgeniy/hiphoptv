<?php

use vova07\fileapi\Widget;
use app\modules\events;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

?>

<?php

$array23 = [
    '0'=>'0',
    '1'=>'1',
    '2'=>'2',
    '3'=>'3',
    '4'=>'4',
    '5'=>'5',
    '6'=>'6',
    '7'=>'7',
    '8'=>'8',
    '9'=>'9',
    '10'=>'10',
    '11'=>'11',
    '12'=>'12',
    '13'=>'13',
    '14'=>'14',
    '15'=>'15',
    '16'=>'16',
    '17'=>'17',
    '18'=>'18',
    '19'=>'19',
    '20'=>'20',
    '21'=>'21',
    '22'=>'22',
    '23'=>'23'
];

$array59 = [
    '00'=>'00',
    '01'=>'01',
    '02'=>'02',
    '03'=>'03',
    '04'=>'04',
    '05'=>'05',
    '06'=>'06',
    '07'=>'07',
    '08'=>'08',
    '09'=>'09',
    '10'=>'10',
    '11'=>'11',
    '12'=>'12',
    '13'=>'13',
    '14'=>'14',
    '15'=>'15',
    '16'=>'16',
    '17'=>'17',
    '18'=>'18',
    '19'=>'19',
    '20'=>'20',
    '21'=>'21',
    '22'=>'22',
    '23'=>'23',
    '24'=>'24',
    '25'=>'25',
    '26'=>'26',
    '27'=>'27',
    '28'=>'28',
    '29'=>'29',
    '30'=>'30',
    '31'=>'31',
    '32'=>'32',
    '33'=>'33',
    '34'=>'34',
    '35'=>'35',
    '36'=>'36',
    '37'=>'37',
    '38'=>'38',
    '39'=>'39',
    '40'=>'40',
    '41'=>'41',
    '42'=>'42',
    '43'=>'43',
    '44'=>'44',
    '45'=>'45',
    '46'=>'46',
    '47'=>'47',
    '48'=>'48',
    '49'=>'49',
    '50'=>'50',
    '51'=>'51',
    '52'=>'52',
    '53'=>'53',
    '54'=>'54',
    '55'=>'55',
    '56'=>'56',
    '57'=>'57',
    '58'=>'58',
    '59'=>'59'
];

?>


<div class="row">
    <div class="col-sm-12">

        <?php $form = ActiveForm::begin(); ?>
        <fieldset class="create-events-form">

        <?= Html::input('text', 'title', null, ['placeholder' => 'title'])
        ?>

        <?php
            print "Start Time : ";

            print Html::dropDownList('s-hh','null', $array23);

            print Html::dropDownList('s-mm','null', $array59);

            print Html::dropDownList('s-ss','null', $array59);
        ?>

        <?php
        print "End Time : ";

        print Html::dropDownList('e-hh','null', $array23);

        print Html::dropDownList('e-mm','null', $array59);

        print Html::dropDownList('e-ss','null', $array59);
        ?>


        <?= Html::input('text', 'text', null, ['placeholder' => 'text'])
        ?>


        <?= Html::submitButton(
            'Create',
            [
                'class' => 'btn btn-success btn-large pull-right'
            ]
        ) ?>

        </fieldset>
        <?php ActiveForm::end(); ?>

    </div>
</div>