<?php

namespace app\modules\labels\controllers;

use yii\web\Controller;
use Yii;
use app\modules\labels\models\OoyalaApi;

class DefaultController extends Controller
{
    public $ooyala;

    public function actionIndex()
    {
        $this->ooyala = $this->ooyalaInit();

        $labels = $this->ooyala->get('/v2/labels');

        return $this->render('index', [
            'labels' => $labels->items
        ]);
    }


    public function actionCreate()
    {

        if(Yii::$app->request->post()) {

            $post = Yii::$app->request->post();
            $this->ooyala = $this->ooyalaInit();
            if($post['parent_id'] == 'null') {
                $labels = $this->ooyala->post('/v2/labels', ["name" => $post['label_name']]);
            }   else {
                $labels = $this->ooyala->post('/v2/labels', ["name" => $post['label_name'], "parent_id" => $post['parent_id'] ]);
            }


            $this->redirect('/labels/default/index/');
        }

        $this->ooyala = $this->ooyalaInit();

        $labels = $this->ooyala->get('/v2/labels');


        $parent_labels_id = array();
        $parent_labels_id['null'] = ' ';

        foreach($labels->items as $item) {
            $parent_labels_id[$item->id] = $item->name;
        }

        return $this->render('create', [
            'parent_labels_id' => $parent_labels_id
        ]);

    }


    public function actionDelete($id) {

        $this->ooyala = $this->ooyalaInit();

        $label = $this->ooyala->delete('/v2/labels/' . $id);

        $this->redirect('/labels/default/index/');
    }


    public function ooyalaInit() {
        $ooyala = new OoyalaApi('pyNzkyOp8Z5TRaIuevSYK36nA_xq.n6z6U', 'kOuVERF2R9Lpw9nXywjDFcDzmF02QTA-srRHYIKa');
        return $ooyala;
    }

}
