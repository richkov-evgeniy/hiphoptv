<?php
use yii\helpers\Url;
?>

<div class="labels-default-index row">
    <a class=" col-lg-12 create-label-link" href="<?= Url::to('/labels/default/create/') ?>">+ Create label</a>

    <div class="col-lg-12">
        <div class="row label-row header-section">
            <div class="col-lg-2">
                Name
            </div>
            <div class="col-lg-3">
                Full Name
            </div>
            <div class="col-lg-3">
                ID
            </div>
            <div class="col-lg-3">
                Parent ID
            </div>
            <div class="col-lg-1">

            </div>
        </div>
        <?php foreach($labels as  $label) : ?>
            <div class="row label-row">
                <div class="col-lg-2 col-label-name">
                    <?= $label->name ?>
                </div>
                <div class="col-lg-3">
                    <?= $label->full_name ?>
                </div>
                <div class="col-lg-3">
                    <?= $label->id ?>
                </div>
                <div class="col-lg-3">
                    <?= $label->parent_id ?>
                </div>
                <div class="col-lg-1">
                    <a class="link-delete-label" href="<?= Url::to(['/labels/default/delete/', 'id' => $label->id]) ?>">X</a>
                </div>
            </div>
        <?php endforeach; ?>
    </div>

</div>

