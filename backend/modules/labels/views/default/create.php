<?php


use app\modules\events;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

?>

<div class="row">
    <div class="col-lg-12">

            <?php $form = ActiveForm::begin(); ?>
            <fieldset class="create-labels-form">

                <div class="row">
                    <?= Html::input('text', 'label_name', null, ['placeholder' => 'name', 'class' => 'col-lg-2'])
                    ?>
                </div>

                <div class="row">
                    <?= Html::dropDownList('parent_id','null', $parent_labels_id, ['class' => 'col-lg-2'])
                    ?>
                </div>

                <div class="row">
                    <?= Html::submitButton(
                        'Create',
                        [
                            'class' => 'btn btn-success btn-large  col-lg-2'
                        ]
                    ) ?>
                </div>

            </fieldset>
            <?php ActiveForm::end(); ?>

    </div>
</div>