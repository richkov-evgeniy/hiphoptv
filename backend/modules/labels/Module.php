<?php

namespace app\modules\labels;

class Module extends \yii\base\Module
{
    public $controllerNamespace = 'app\modules\labels\controllers';

    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
